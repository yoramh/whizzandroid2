package com.whizzthrough.android.apps.tde.printer.api;

import java.io.IOException;

/**
 * Created by yoram on 25/01/16.
 */
public interface IPrinter {
    boolean isReal();

    void left() throws IOException;
    void right() throws IOException;
    void center() throws IOException;
    void setPrintFont(final EPrintFont printFont) throws IOException;
    void setUnderline(final boolean underline) throws IOException;
    void println(final String message) throws IOException;
    void println() throws IOException;
    void print(final String message) throws IOException;
    void setBold(boolean bold) throws IOException;
}
