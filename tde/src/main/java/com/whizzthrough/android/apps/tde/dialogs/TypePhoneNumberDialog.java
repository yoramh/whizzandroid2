package com.whizzthrough.android.apps.tde.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.whizzthrough.android.apps.tde.R;
import com.whizzthrough.android.commons.utils.FormUtils;

public class TypePhoneNumberDialog extends Dialog {
    public interface OnGetPhoneNumberListener {
        void onPhoneNumber(String phoneNumber) throws Exception;
    }

    public static void show2(
            final Context context,
            final Activity activity,
            final OnGetPhoneNumberListener onGetPhoneNumberListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.keypad2, null);
        builder.setView(layout);

        final Dialog dialog = builder.create();
        dialog.show();
        dialog.setCancelable(false);

        final EditText ed = (EditText)layout.findViewById(R.id.keypad2_phone_ed);

        Button btn = (Button) layout.findViewById(R.id.keypad2_ok_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onGetPhoneNumberListener.onPhoneNumber(ed.getText().toString());
                    dialog.dismiss();
                } catch (Throwable t) {
                    FormUtils.showErrorMessage(context, t.getMessage(), new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
            }
        });

        btn = (Button) layout.findViewById(R.id.keypad2_cancel_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public TypePhoneNumberDialog(Context context, int theme) {
        super(context, theme);
    }

    public TypePhoneNumberDialog(Context context) {
        super(context);
    }
}
