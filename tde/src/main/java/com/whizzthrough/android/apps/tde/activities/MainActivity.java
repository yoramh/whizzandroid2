package com.whizzthrough.android.apps.tde.activities;

import android.content.*;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.*;

import com.whizzthrough.android.apps.tde.R;
import com.whizzthrough.android.apps.tde.printer.api.EPrintFont;
import com.whizzthrough.android.apps.tde.printer.api.IPrinter;
import com.whizzthrough.android.apps.tde.receivers.PrinterBroadcastReceiver;
import com.whizzthrough.android.apps.tde.services.tde.TdeService;
import com.whizzthrough.android.apps.tde.services.printer.PrinterService;
import com.whizzthrough.android.apps.tde.utils.listeners.SmsOnClickListener;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.OnGetQueueInfoForTerminal;
import com.whizzthrough.android.commons.server.api.OnGrabTicket;
import com.whizzthrough.android.commons.server.json.*;
import com.whizzthrough.android.commons.utils.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends AppCompatActivity {
    private QueueDefinition queueDefinition;
    private PrinterBroadcastReceiver printerBroadcastReceiver;
    private Intent tdeServiceIntent;
    private BroadcastReceiver brGrabbedTicket = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            UserGrabTicketResponse request = (UserGrabTicketResponse)intent.getSerializableExtra(
                    TdeService.BUNDLE_JSON_DATA);

            if (Config.getUniqueCode(true).equals(request.getQrCodeUserToken())) {
                MainActivity.this.onBackPressed();
            }
        }
    };

    private void setPrinter(boolean connected) {
        ImageView img = (ImageView)findViewById(R.id.tde_main_activity_print);

        if (connected) {
            img.setAlpha((float) 1);
        } else {
            img.setAlpha((float) 0.4);
        }
    }

    private void startTicketDispenser() {
        setContentView(R.layout.activity_main);
        Config.setCurrentContext(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        final ViewGroup v = (ViewGroup)findViewById(R.id.tde_main_activity_root);
        SystemUtils.makeFullUi44(v);
        v.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    SystemUtils.makeFullUi44(v);
                }
            }
        });

        setPrinter(PrinterService.getPrinter().isReal());
        ImageView img = (ImageView)findViewById(R.id.tde_main_activity_print);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PrinterService.getPrinter().isReal()) {
                    return;
                }

                WhizzThroughServerFactory.getDefaultServer().grabTicket(
                        MainActivity.this,
                        new UserGrabTicketRequest(
                            "printed://null",
                            queueDefinition.getUniqueCode(),
                            "",
                            "en_EN"),
                        new OnGrabTicket(MainActivity.this) {
                            @Override
                            public void onResponse(UserGrabTicketResponse response, Object eventsData) {
                                MainActivity.this.print(response);

                                finish();
                            }
                        },
                        null);
            }
        });

        TextView animTV = (TextView)findViewById(R.id.tde_main_activity_lbl_ticket_number);
        animTV.setSelected(true);
        animTV.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        animTV.setSingleLine(true);

        //animTV.startAnimation(AnimationUtils.makeInAnimation(this,true));
        animTV.startAnimation(AnimationUtils.loadAnimation(this,R.anim.scrollad));


        img = (ImageView)findViewById(R.id.tde_back_button);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        img = (ImageView)findViewById(R.id.tde_main_activity_sms);
        img.setOnClickListener(new SmsOnClickListener(this, queueDefinition));


        TextView tv = (TextView)findViewById(R.id.tde_top_side_queue_name);
        tv.setText(queueDefinition.getName());
        tv = (TextView)findViewById(R.id.tde_queue_description);
        tv.setText(queueDefinition.getDescription());

        /*
        VideoView video = (VideoView)findViewById(R.id.tde_video);
        if (video != null) {
            try {
                Uri u = Uri.parse(Environment.getExternalStorageDirectory() + "/tde/movie.mp4");
                video.setVideoURI(u);
                video.setZOrderOnTop(true);
                video.start();
            } catch (Throwable t) {
                Log.e("WT", t.getMessage(), t);
            }
        }
        */

        if (queueDefinition.getBase64DefaultIcon() != null && !queueDefinition.getBase64DefaultIcon().isEmpty()) {
            ImageView iv = (ImageView)findViewById(R.id.tde_queue_logo) ;
            Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(Base64.decode(queueDefinition.getBase64DefaultIcon(), Base64.NO_WRAP)));
            Drawable bmd = new BitmapDrawable(getResources(), bm);
            bmd.setBounds(0, 0, bmd.getIntrinsicWidth() * 2, bmd.getIntrinsicHeight() *2);
            iv.setImageBitmap(bm);
        }

        WhizzThroughServerFactory.getDefaultServer().getQueueInfoForTerminal(
                this,
                new AppGetQueueInfoForTerminalRequest(this.queueDefinition.getId(), Config.getUniqueCode(true), 200, 200),
                new OnGetQueueInfoForTerminal(this) {
                    @Override
                    public void onResponse(final AppGetQueueInfoForTerminalResponse response, final Object eventsData) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final byte[] bImage = Base64.decode(response.getB64QrCode(), Base64.DEFAULT);
                                final Bitmap bmp = BitmapFactory.decodeByteArray(bImage, 0, bImage.length);


                                File f = new File("qr.png");
                                try (FileOutputStream out = new FileOutputStream(f)) {
                                    bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                                    // PNG is a lossless format, the compression factor (100) is ignored
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


                                ImageView img = (ImageView) MainActivity.this.findViewById(R.id.tde_main_activity_qrcode);
                                img.setImageBitmap(bmp);
                            }
                        });
                    }
                },
                null);
    }

    private void print(final UserGrabTicketResponse response) {
        try {
            final IPrinter printer = PrinterService.getPrinter();

            printer.center();

            printer.setPrintFont(EPrintFont.PF_DOUBLE_WIDTH);
            printer.println(response.getCompany());
            printer.println(response.getQueueName());

            printer.setPrintFont(EPrintFont.PF_NORMAL);
            printer.println(response.getDescription());
            printer.println();

            printer.setPrintFont(EPrintFont.PF_NORMAL);
            printer.println("**********");
            printer.println("Download the GetTicket app and type this code if you want to leave the premises and be notified when it is your turn");
            printer.setPrintFont(EPrintFont.PF_DOUBLE_WIDTH);
            printer.setBold(true);
            printer.println(response.getCode().toUpperCase());
            printer.setPrintFont(EPrintFont.PF_NORMAL);
            printer.println("**********");
            printer.println();

            printer.setPrintFont(EPrintFont.PF_DOUBLE_HEIGHT_AND_DOUBLE_WIDTH);
            printer.println("Ticket No " + response.getTicketNumber());
            printer.println();
            printer.println();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        printerBroadcastReceiver = new PrinterBroadcastReceiver(this);
        IntentFilter filter = printerBroadcastReceiver.getIntentFilter();
        registerReceiver(printerBroadcastReceiver, filter);

        filter = new IntentFilter();
        filter.addAction(TdeService.FILTER_MESSAGE_TICKET_GRABBED);
        registerReceiver(brGrabbedTicket, filter);

        if (savedInstanceState != null) {
            this.queueDefinition = (QueueDefinition) savedInstanceState.getSerializable("q");
        } else {
            this.queueDefinition = (QueueDefinition) getIntent().getSerializableExtra("q");
        }

        tdeServiceIntent = new Intent(this, TdeService.class);
        tdeServiceIntent.putExtra(TdeService.EXTRA_QUEUEID, queueDefinition.getId());
        startService(tdeServiceIntent);

        startTicketDispenser();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(printerBroadcastReceiver);
        unregisterReceiver(brGrabbedTicket);
        stopService(tdeServiceIntent);
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (queueDefinition != null) {
            outState.putSerializable("q", queueDefinition);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            final ViewGroup v = (ViewGroup)findViewById(R.id.tde_main_activity_root);
            SystemUtils.makeFullUi44(v);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
