package com.whizzthrough.android.apps.tde.utils.phones.api;

import com.whizzthrough.android.apps.tde.utils.phones.impl.IsraelPhoneNumberValidator;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yoram on 01/05/16.
 */
public class PhoneNumberValidatorRegistar {
    private static final Map<String, IPhoneNumberValidator> REGISTRAR = new HashMap<>();

    public static void register(String countryCode, IPhoneNumberValidator validator) {
        REGISTRAR.put(countryCode, validator);
    }

    public static IPhoneNumberValidator getValidator(String countryCode) {
        return new IsraelPhoneNumberValidator();
    }
}
