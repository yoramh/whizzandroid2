package com.whizzthrough.android.apps.tde.utils.listeners;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.EditText;

import com.whizzthrough.android.apps.tde.dialogs.TypePhoneNumberDialog;
import com.whizzthrough.android.apps.tde.utils.phones.api.IPhoneNumberValidator;
import com.whizzthrough.android.apps.tde.utils.phones.api.PhoneNumberValidatorRegistar;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.OnGrabTicket;
import com.whizzthrough.android.commons.server.json.QueueDefinition;
import com.whizzthrough.android.commons.server.json.UserGrabTicketRequest;
import com.whizzthrough.android.commons.server.json.UserGrabTicketResponse;
import com.whizzthrough.android.commons.utils.SystemUtils;

/**
 * Created by yoram on 01/05/16.
 */
public class SmsOnClickListener implements View.OnClickListener {
    private static class OnOkButtonPressed implements DialogInterface.OnClickListener {
        private final EditText phone;
        private final SmsOnClickListener parent;

        private OnOkButtonPressed(final SmsOnClickListener parent, final EditText phone) {
            super();

            this.phone = phone;
            this.parent = parent;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            final String phoneStr = this.phone.getText().toString();

            final String locale = SystemUtils.getLocaleLanguage(this.parent.context.getApplicationContext());
            WhizzThroughServerFactory.getDefaultServer().grabTicket(
                    this.parent.context,
                    new UserGrabTicketRequest(
                        "sms://IL//" + phoneStr,
                        this.parent.queueDefinition.getUniqueCode(),
                        null,
                        locale),
                    new OnGrabTicket(this.parent.context) {
                        @Override
                        public void onResponse(UserGrabTicketResponse response, Object eventsData) {
                            System.out.println("done");
                        }
                    },
                    null);
        }
    }

    private static class OnCancelButtonPressed implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
        }
    }

    private final Context context;
    private final QueueDefinition queueDefinition;

    public SmsOnClickListener(final Context context, final QueueDefinition queueDefinition) {
        super();

        this.context = context;
        this.queueDefinition = queueDefinition;
    }

    @Override
    public void onClick(View v) {
        TypePhoneNumberDialog.show2(
                context,
                (Activity) context,
                new TypePhoneNumberDialog.OnGetPhoneNumberListener() {
                    @Override
                    public void onPhoneNumber(String phoneNumber) throws Exception {
                        // http://freegeoip.net/json/129.42.38.1?callback=jQuery111104414094038016968_1462129493465&_=1462129493469
                        final String country = "IL";

                        IPhoneNumberValidator validator = PhoneNumberValidatorRegistar.getValidator(country);
                        if (!validator.validate(phoneNumber)) {
                            throw new Exception("The phone number is invalid");
                        }

                        String locale = SystemUtils.getLocaleLanguage(context.getApplicationContext());
                        if (locale == null) {
                            locale = "en";
                        }
                        locale = locale.toLowerCase();

                        if (locale.equals("he") || locale.equals("iw")) {
                            locale = "he_IL";
                        } else if (locale.equals("fr")) {
                            locale = "fr_FR";
                        } else {
                            locale = "en_EN";
                        }

                        WhizzThroughServerFactory.getDefaultServer().grabTicket(
                                context,
                                new UserGrabTicketRequest(
                                    "sms://IL//" + phoneNumber,
                                     queueDefinition.getUniqueCode(),
                                     null,
                                     locale),
                                new OnGrabTicket(context) {
                                    @Override
                                    public void onResponse(UserGrabTicketResponse response, Object eventsData) {
                                        if (SmsOnClickListener.this.context instanceof Activity) {
                                            ((Activity)SmsOnClickListener.this.context).finish();
                                        }
                                    }
                                },
                                null);

                    }
                }
        );
    }
}
