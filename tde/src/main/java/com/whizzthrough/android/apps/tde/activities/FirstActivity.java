package com.whizzthrough.android.apps.tde.activities;

import com.whizzthrough.android.apps.tde.R;
import com.whizzthrough.android.apps.tde.activities.selectqueues.SelectQueueActivity;
import com.whizzthrough.android.commons.activities.BlankActivity;

/**
 * Created by yoram on 24/05/16.
 */
public class FirstActivity extends BlankActivity {
    @Override
    protected Class<?> getNextActivityClass() {
        return SelectQueueActivity.class;
    }

    @Override
    protected int getDefinitionXml() {
        return R.raw.bootstrap;
    }
}