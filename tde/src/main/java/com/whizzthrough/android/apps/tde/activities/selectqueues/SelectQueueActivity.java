package com.whizzthrough.android.apps.tde.activities.selectqueues;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.*;
import com.whizzthrough.android.apps.tde.R;
import com.whizzthrough.android.apps.tde.activities.MainActivity;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.OnListQueues;
import com.whizzthrough.android.commons.server.json.AppListQueuesResponse;
import com.whizzthrough.android.commons.server.json.QueueDefinition;
import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.SystemUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by yoram on 24/05/16.
 */
public class SelectQueueActivity extends AppCompatActivity {
    private QueueListItemAdapter adp;

    public void refresh() {
        TextView lbl= (TextView) findViewById(R.id.tde_select_queue_title);
        lbl.setText(getResources().getText(R.string.tde_take_a_ticket));

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_queue);

        final GridView grid = (GridView)findViewById(R.id.asq_grid);
        grid.setClickable(true);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                QueueDefinition q = (QueueDefinition)adp.getItem(position);
                Intent intent = new Intent(SelectQueueActivity.this, MainActivity.class);
                intent.putExtra("q", q);
                startActivity(intent);
            }
        });

        if (Config.getQueueDefinition() == null) {
            WhizzThroughServerFactory.getDefaultServer().listQueues(
                    this,
                    Config.getCookie(),
                    0,
                    20,
                    new OnListQueues(this) {
                        @Override
                        public void onResponse(final AppListQueuesResponse response, Object eventsData) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    List<QueueDefinition> list = response.getQueues();
                                    Collections.sort(list, new Comparator<QueueDefinition>() {
                                        @Override
                                        public int compare(QueueDefinition lhs, QueueDefinition rhs) {
                                            return lhs.getDescription().compareTo(rhs.getDescription());
                                        }
                                    });

                                    adp = new QueueListItemAdapter(
                                            SelectQueueActivity.this,
                                            list.toArray(new QueueDefinition[0]));
                                    grid.setAdapter(adp);
                                    adp.notifyDataSetChanged();
                                }
                            });

                        }
                    },
                    null);
        } else {
            grid.setAdapter(adp);
            adp.notifyDataSetChanged();
        }

        Button btnFr = (Button)findViewById(R.id.tde_btn_lang_fr);
        btnFr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemUtils.changeLanguage(getApplicationContext(), "fr");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refresh();
                    }
                });
            }
        });

        Button btnHe = (Button)findViewById(R.id.tde_btn_lang_he);
        btnHe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemUtils.changeLanguage(getApplicationContext(), "he");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refresh();
                    }
                });
            }
        });

        Button btnEn = (Button)findViewById(R.id.tde_btn_lang_en);
        btnEn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemUtils.changeLanguage(getApplicationContext(), "en");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refresh();
                    }
                });
            }
        });

        Button btnRu = (Button)findViewById(R.id.tde_btn_lang_ru);
        btnRu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemUtils.changeLanguage(getApplicationContext(), "ru");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refresh();
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
