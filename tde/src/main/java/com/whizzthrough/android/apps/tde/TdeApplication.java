package com.whizzthrough.android.apps.tde;

import android.app.Application;
import android.content.Intent;
import android.content.IntentFilter;
import com.whizzthrough.android.apps.tde.services.printer.PrinterService;
import com.whizzthrough.android.commons.receivers.ErrorBroadcastReceiver;
import com.whizzthrough.android.commons.receivers.ShowProgressBroadcastReceiver;

/**
 * Created by yoram on 10/01/16.
 */
public class TdeApplication extends Application {
    private Intent printerServiceIntent;
    private ShowProgressBroadcastReceiver showProgressBroadcastReceiver;
    private ErrorBroadcastReceiver errorBroadcastReceiver;

    @Override
    public void onCreate() {
        super.onCreate();

        printerServiceIntent = new Intent(this, PrinterService.class);

        startService(printerServiceIntent);

        showProgressBroadcastReceiver = new ShowProgressBroadcastReceiver();
        IntentFilter filter = showProgressBroadcastReceiver.getIntentFilter();
        registerReceiver(showProgressBroadcastReceiver, filter);

        errorBroadcastReceiver = new ErrorBroadcastReceiver();
        filter = errorBroadcastReceiver.getIntentFilter();
        registerReceiver(errorBroadcastReceiver, filter);

        //messageServiceIntent = new Intent(this, MessageService.class);
        //startService(messageServiceIntent);
    }

    @Override
    public void onTerminate() {
        //stopService(messageServiceIntent);
        //messageServiceIntent = null;

        stopService(printerServiceIntent);
        unregisterReceiver(showProgressBroadcastReceiver);

        super.onTerminate();
    }
}
