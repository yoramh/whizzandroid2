package com.whizzthrough.android.apps.tde.services.printer;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Messenger;
import com.whizzthrough.android.apps.tde.printer.api.IPrinter;
import com.whizzthrough.android.apps.tde.printer.impl.BlankPrinter;
import com.whizzthrough.android.commons.utils.AndroidUtils;
import com.whizzthrough.android.commons.utils.SystemUtils;

/**
 * Created by yoram on 25/01/16.
 */
public class PrinterService extends Service {
    public static final String FILTER_PRINTER_STATUS_ON = PrinterService.class.getName() + ".FILTER_PRINTER_STATUS_ON";
    public static final String FILTER_PRINTER_STATUS_OFF = PrinterService.class.getName() + ".FILTER_PRINTER_STATUS_OFF";

    private static IPrinter printer = new BlankPrinter();
    private final Messenger messenger = new Messenger(new PrinterHandler(this));

    public static IPrinter getPrinter() {
        return printer;
    }

    public static void setPrinter(IPrinter printer) {
        PrinterService.printer = printer;
    }

    private PrinterCheckerRunnable currentChecker;

    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!AndroidUtils.isBluetoothSupported()) {
            return START_NOT_STICKY;
        } else {
            if (currentChecker == null || !currentChecker.isAlive()) {
                currentChecker = new PrinterCheckerRunnable(this);
            }

            currentChecker.start();

            return START_STICKY;
        }
    }

    @Override
    public boolean stopService(Intent name) {
        this.currentChecker.finishRequest();

        while (this.currentChecker.isAlive()) {
            SystemUtils.sleep(500);
        }

        this.currentChecker = null;
        return true;
    }

    @Override
    public void onDestroy() {
        stopService(null);
    }
}
