package com.whizzthrough.android.apps.tde.services.printer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;
import android.util.Printer;
import com.whizzthrough.android.apps.tde.printer.impl.BlankPrinter;
import com.whizzthrough.android.apps.tde.printer.impl.koolertron.Koolertron58;
import com.whizzthrough.android.commons.utils.SystemUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.Set;
import java.util.UUID;

/**
 * Created by yoram on 26/05/16.
 */
public class PrinterCheckerRunnable extends Thread {
    /*
     * Hint: If you are connecting to a Bluetooth serial board then try using the well-known
     * SPP UUID 00001101-0000-1000-8000-00805F9B34FB.
     * http://developer.android.com/reference/android/bluetooth/BluetoothDevice.html
     */
    private static final UUID USB_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private boolean finish = false;
    private final WeakReference<PrinterService> printerService;
    private final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    public PrinterCheckerRunnable(final PrinterService printerService) {
        super();

        this.printerService = new WeakReference<>(printerService);
    }

    @Override
    public void run() {
        final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        while (true) {
            try {
                if (finish) {
                    return;
                }

                final PrinterService service = this.printerService.get();

                if (service == null) {
                    return;
                }

                final Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

                if (pairedDevices.size() == 0) {
                    Intent i = new Intent(PrinterService.FILTER_PRINTER_STATUS_OFF);
                    service.sendBroadcast(i);

                    SystemUtils.sleep(1000);

                    continue;
                }

                final BluetoothDevice device = pairedDevices.iterator().next();

                // Cancel discovery because it will slow down the connection
                bluetoothAdapter.cancelDiscovery();

                final BluetoothSocket socket = device.createRfcommSocketToServiceRecord(USB_UUID);

                try {
                    socket.connect();

                    Intent i = new Intent(PrinterService.FILTER_PRINTER_STATUS_ON);
                    service.sendBroadcast(i);
                } catch (IOException e) {
                    Intent i = new Intent(PrinterService.FILTER_PRINTER_STATUS_OFF);
                    service.sendBroadcast(i);

                    continue;
                }

                final InputStream in = socket.getInputStream();
                final OutputStream out = socket.getOutputStream();

                PrinterService.setPrinter(new Koolertron58(out));

                final byte[] buffer = new byte[1024];  // buffer store for the stream

                while (true) {
                    if (finish) {
                        return;
                    }

                    in.read(buffer);

                    SystemUtils.sleep(1000);
                }
            } catch (IOException e) {
                PrinterService.setPrinter(new BlankPrinter());
            }
        }

    }

    public void finishRequest() {
        this.finish = true;
    }
}
