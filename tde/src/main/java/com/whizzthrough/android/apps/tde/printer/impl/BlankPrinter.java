package com.whizzthrough.android.apps.tde.printer.impl;

import com.whizzthrough.android.apps.tde.printer.api.EPrintFont;
import com.whizzthrough.android.apps.tde.printer.api.IPrinter;

import java.io.IOException;

/**
 * Created by yoram on 01/02/16.
 */
public class BlankPrinter implements IPrinter {
    @Override
    public boolean isReal() {
        return false;
    }

    @Override
    public void setPrintFont(EPrintFont printFont) throws IOException {

    }

    @Override
    public void setUnderline(boolean underline) throws IOException {

    }

    @Override
    public void println(String message) throws IOException {

    }

    @Override
    public void print(String message) throws IOException {

    }

    @Override
    public void println() throws IOException {

    }

    @Override
    public void setBold(boolean bold) throws IOException {

    }

    @Override
    public void left() {

    }

    @Override
    public void right() {

    }

    @Override
    public void center() {

    }
}
