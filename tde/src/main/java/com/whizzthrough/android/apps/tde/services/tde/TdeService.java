package com.whizzthrough.android.apps.tde.services.tde;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Messenger;

import com.whizzthrough.android.apps.tde.services.printer.PrinterHandler;
import com.whizzthrough.android.commons.server.json.UserGrabTicketResponse;
import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.JsonUtils;
import com.whizzthrough.android.commons.utils.SystemUtils;
import com.whizzthrough.android.commons.utils.eventsource.InputStreamResult;
import com.whizzthrough.android.commons.utils.eventsource.sync.ISyncEventSourceEvents;
import com.whizzthrough.android.commons.utils.eventsource.sync.SyncEventSource;
import com.whizzthrough.android.commons.utils.eventsource.sync.WhyWasIClosed;

import java.net.HttpURLConnection;

/**
 * Created by yoram on 21/10/15.
 */
public class TdeService extends IntentService {
    public static final String EXTRA_QUEUEID = TdeService.class.getName() + ".EXTRA.QUEUEID";

    public static final String FILTER_MESSAGE_TICKET_GRABBED = TdeService.class.getName() + ".FILTER.TICKET_GRABBED";
    public static final String BUNDLE_JSON_DATA = TdeService.class.getName() + ".BUNDLE.BUNDLE_JSON_DATA";

    private SyncEventSource eventSource;

    public TdeService() {
        this(null);
    }
    public TdeService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final long queueId = intent.getLongExtra(EXTRA_QUEUEID, 0);

        if (eventSource == null || !eventSource.isRunning()) {
            try {
                eventSource = new SyncEventSource(new ISyncEventSourceEvents() {
                    @Override
                    public void onMessage(String id, String event, String data) {
                        if (event.equals("ticket-grabbed")) {
                            JsonUtils.broadcastJson(
                                    TdeService.this,
                                    FILTER_MESSAGE_TICKET_GRABBED,
                                    BUNDLE_JSON_DATA,
                                    data,
                                    UserGrabTicketResponse.class);
                        }
                    }

                    @Override
                    public void onError(HttpURLConnection conn, Throwable t) {

                    }

                    @Override
                    public void onConnectionError(HttpURLConnection conn, InputStreamResult res) {

                    }

                    @Override
                    public void onDisconnected() {

                    }

                    @Override
                    public void onClose(WhyWasIClosed reason) {

                    }

                    @Override
                    public void onConnecting() {

                    }

                    @Override
                    public void onConnected() {
                    }
                },
                        Config.getRegisterTdeUrl() + "/" + queueId,
                        Config.getCookie());

                eventSource.run();
            } catch (Throwable t) {
                this.eventSource = null;
            }
        }
    }

    @Override
    public void onDestroy() {
        if (this.eventSource != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    TdeService.this.eventSource.close();

                    while (TdeService.this.eventSource.isRunning()) {
                        SystemUtils.sleep(500);
                    }

                    TdeService.this.eventSource = null;
                }
            }).start();
        }
    }
}
