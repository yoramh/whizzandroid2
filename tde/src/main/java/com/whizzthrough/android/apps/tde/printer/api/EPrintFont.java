package com.whizzthrough.android.apps.tde.printer.api;

/**
 * Created by yoram on 25/01/16.
 */
public enum EPrintFont {
    PF_NORMAL,
    PF_DOUBLE_HEIGHT,
    PF_DOUBLE_WIDTH,
    PF_DOUBLE_HEIGHT_AND_DOUBLE_WIDTH
}
