package com.whizzthrough.android.apps.tde.services.printer;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

/**
 * Created by yoram on 02/02/16.
 */
public class PrinterHandler extends Handler {
    public static final int MSG_SAY_HELLO = 1;

    private final Context context;

    public PrinterHandler(final Context context) {
        super();

        this.context = context;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case MSG_SAY_HELLO:
                Toast.makeText(context, "hello!", Toast.LENGTH_SHORT).show();
                break;
            default:
                super.handleMessage(msg);
        }
    }
}
