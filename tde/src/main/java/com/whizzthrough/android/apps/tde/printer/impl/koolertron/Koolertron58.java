package com.whizzthrough.android.apps.tde.printer.impl.koolertron;

import com.whizzthrough.android.apps.tde.printer.api.EPrintFont;
import com.whizzthrough.android.apps.tde.printer.api.IPrinter;

import java.io.*;

/**
 * Created by yoram on 24/01/16.
 */
public class Koolertron58 implements IPrinter {
    private final OutputStream out;

    public Koolertron58(final OutputStream out) {
        super();

        this.out = out;
    }

    @Override
    public boolean isReal() {
        return true;
    }

    /**
     * Font 1 = Normal
     * Font 2 = Double Height
     * Font 3 = Double Width
     * Font 4 = Double Height and Double Width
     */
    @Override
    public void setPrintFont(final EPrintFont printFont) throws IOException {
        final byte[] cmd = new byte[3];
        cmd[0] = 0x1b;
        cmd[1] = 0x21;

        switch (printFont) {
            case PF_DOUBLE_WIDTH:
                cmd[2] |= 0x20;
                out.write(cmd);
                break;

            case PF_DOUBLE_HEIGHT:
                cmd[2] |= 0x10;
                out.write(cmd);
                break;

            case PF_DOUBLE_HEIGHT_AND_DOUBLE_WIDTH:
                cmd[2] |= 0x10;
                out.write(cmd);
                cmd[2] |= 0x20;
                out.write(cmd);
                break;

            case PF_NORMAL:
                cmd[2] |= 0x00;
                out.write(cmd);
                break;
        }
    }

    @Override
    public void setUnderline(final boolean underline) throws IOException {
        final byte[] cmd = new byte[3];
        cmd[0] = 0x1b;
        cmd[1] = 0x21;

        cmd[2] = underline ? (byte)0x80 : 0;
        out.write(cmd);
    }

    @Override
    public void println(final String message)throws IOException {
        print(message + "\r\n");
    }

    @Override
    public void print(final String message) throws IOException{
        final byte[] cmd = new byte[3];
        cmd[0] = 0x1b;
        cmd[1] = 0x61;
        cmd[2] = 25;
        out.write(cmd);
        out.write(message.getBytes());

        out.write(new byte[] {0});
    }

    @Override
    public void println() throws IOException {
        println(" ");
    }

    @Override
    public void setBold(final boolean bold) throws IOException {
        final byte[] cmd = new byte[3];
        cmd[0] = 0x1b;
        cmd[1] = 0x21;

        cmd[2] = bold ? (byte)0x08 : 0;
        out.write(cmd);
    }

    @Override
    public void left() throws IOException {
        final byte[] cmd = new byte[] {0x1B, 0x61, 0};
        out.write(cmd);
    }

    @Override
    public void right() throws IOException {
        final byte[] cmd = new byte[] {0x1B, 0x61, 50};
        out.write(cmd);
    }

    @Override
    public void center() throws IOException {
        final byte[] cmd = new byte[] {0x1B, 0x61, 1};
        out.write(cmd);
    }
}
