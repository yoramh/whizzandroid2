package com.whizzthrough.android.apps.tde.utils.phones.impl;

import com.whizzthrough.android.apps.tde.utils.phones.api.IPhoneNumberValidator;

/**
 * Created by yoram on 01/05/16.
 */
public class IsraelPhoneNumberValidator implements IPhoneNumberValidator {
    @Override
    public boolean validate(String phone) {
        if (phone.startsWith("+")) {
            return true;
        }

        if (phone.length() != 10) {
            return false;
        }

        if (!phone.startsWith("05")) {
            return false;
        }

        return true;
    }
}
