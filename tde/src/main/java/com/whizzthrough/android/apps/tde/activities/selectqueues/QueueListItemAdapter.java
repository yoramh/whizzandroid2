package com.whizzthrough.android.apps.tde.activities.selectqueues;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.whizzthrough.android.apps.tde.R;
import com.whizzthrough.android.commons.components.RoundImage;
import com.whizzthrough.android.commons.server.json.QueueDefinition;

/**
 * Created by yoram on 01/03/16.
 */
public class QueueListItemAdapter extends BaseAdapter {
    private final QueueDefinition[] items;
    private final Context context;
    private int selectedIndex = -1;

    public QueueListItemAdapter(final Context context, final QueueDefinition... items) {
        super();

        this.context = context;

        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.length;
    }

    @Override
    public Object getItem(int position) {
        return this.items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final QueueDefinition item = this.items[position];

        if (convertView == null) {
            v = inflater.inflate(R.layout.queue_list_item, parent, false);
            v.setTag(position);
        } else {
            v = convertView;
        }

        final ImageView img = (ImageView) v.findViewById(R.id.queue_list_item_image);
        RoundImage.round(img, item.getBase64DefaultIcon());

        final TextView txt = (TextView) v.findViewById(R.id.queue_list_item_caption);
        txt.setText(item.getDescription());

        return v;
    }

    public int getSelectedIndex() {
        return this.selectedIndex;
    }

    public void setSelectedIndex(final int selectedIndex) {
        this.selectedIndex = selectedIndex;
        notifyDataSetChanged();
    }
}
