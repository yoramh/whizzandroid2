package com.whizzthrough.android.apps.tde.receivers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.ImageView;
import com.whizzthrough.android.apps.tde.R;
import com.whizzthrough.android.apps.tde.services.printer.PrinterService;
import com.whizzthrough.android.commons.receivers.AbstractBroadcastReceiver;

/**
 * Created by yoram on 02/02/16.
 */
public class PrinterBroadcastReceiver extends AbstractBroadcastReceiver {
    private final Activity activity;

    public PrinterBroadcastReceiver() {
        this(null);
    }

    public PrinterBroadcastReceiver(final Activity activity) {
        super();

        this.activity = activity;
    }

    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter res = new IntentFilter();
        res.addAction(PrinterService.FILTER_PRINTER_STATUS_ON);
        res.addAction(PrinterService.FILTER_PRINTER_STATUS_OFF);

        return res;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ImageView img = (ImageView)this.activity.findViewById(R.id.tde_main_activity_print);

        if (intent.getAction().equals(PrinterService.FILTER_PRINTER_STATUS_ON)) {
            img.setAlpha((float) 1);
        } else {
            img.setAlpha((float) 0.4);
        }
    }
}
