package com.whizzthrough.android.apps.tde.utils.phones.api;

/**
 * Created by yoram on 01/05/16.
 */
public interface IPhoneNumberValidator {
    boolean validate(final String phone);
}
