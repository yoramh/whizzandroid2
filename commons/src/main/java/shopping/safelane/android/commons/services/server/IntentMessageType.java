package shopping.safelane.android.commons.services.server;

public interface IntentMessageType {
    String ROOT_KEY = IntentMessageType.class.getSimpleName();
    String FILTER_EVENT = ROOT_KEY + ".FILTER_EVENT";

    String MSG_DATA = ROOT_KEY + ".MSG_DATA";
    String MSG_ERROR = ROOT_KEY + ".MSG_ERROR";
}
