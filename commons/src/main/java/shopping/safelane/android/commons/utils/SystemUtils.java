package shopping.safelane.android.commons.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Random;

/**
 * Created by yoram on 17/10/15.
 */
public class SystemUtils {
    private static final Random RND = new SecureRandom();

    public static void quit(final Activity parent) {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        parent.startActivity(startMain);
    }

    public static void makeFullUi44(ViewGroup v) {
        v.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    public static void sleep(final long ms) {
        SystemClock.sleep(ms);
    }

    public static void changeLanguage(final Context context, final String code) {
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(code);
        res.updateConfiguration(conf, dm);
    }

    public static String getLocaleLanguage(final Context context) {
        Resources res = context.getResources();
        android.content.res.Configuration conf = res.getConfiguration();
        return conf.locale.getLanguage();
    }

    @NonNull
    public static byte[] random(int size) {
        final byte[] res = new byte[size];
        RND.nextBytes(res);
        return res;
    }
}
