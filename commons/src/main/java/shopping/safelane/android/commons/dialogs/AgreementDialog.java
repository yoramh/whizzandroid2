package shopping.safelane.android.commons.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.whizzthrough.android.commons.utils.AndroidUtils;
import com.whizzthrough.android.commons.utils.Config;

import shopping.safelane.android.commons.R;

public class AgreementDialog extends Dialog {
    public interface OnClickListener {
        void onDisagree(Dialog dialog);
    }

    public static void show(
            final Activity activity,
            final OnClickListener listener,
            final Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.agreement_dialog, null);
        builder.setView(layout);

        final Dialog dialog = builder.create();
        dialog.show();
        dialog.setCancelable(false);

        /**
         * Google signin - begin
         */
        final GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        final GoogleSignInClient client = GoogleSignIn.getClient(activity, gso);
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(activity);
        System.out.println(account);
        final GoogleApiClient mGoogleApiClient =
                new GoogleApiClient.Builder(activity)
                        .enableAutoManage((FragmentActivity) activity, new GoogleApiClient.OnConnectionFailedListener() {
                            @Override
                            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                                System.out.println("eee");
                            }})
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();

        SignInButton signInButton = layout.findViewById(R.id.gt_tc_agree_google_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

                if (savedInstanceState != null) {
                    final Parcel p = Parcel.obtain();
                    p.writeBundle(savedInstanceState);
                    signInIntent.putExtra("savedInstanceState", p.marshall());
                }
                activity.startActivityForResult(signInIntent, 1234/*RC_SIGN_IN*/);
                dialog.dismiss();
            }
        });
        /**
         * Google signin - end
         */

        final TextView disagree = (TextView) layout.findViewById(R.id.gt_tc_layout_disagree_link);
        AndroidUtils.undelineTextView(activity, disagree, R.string.slres_disagree_label);
        disagree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDisagree(dialog);
            }
        });

        final TextView tc = (TextView) layout.findViewById(R.id.gt_tc_launcher_terms);
        AndroidUtils.undelineTextView(activity, tc, R.string.slres_link_text);
        tc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(Config.getPrefixUrl() + "/licenses/en/eula.txt"));
                activity.startActivity(browserIntent);
            }
        });
    }

    public AgreementDialog(Context context, int theme) {
        super(context, theme);
    }

    public AgreementDialog(Context context) {
        super(context);
    }
}
