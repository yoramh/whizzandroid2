package shopping.safelane.android.commons.components.tickets;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.OnIncreaseCurrentTicket;
import com.whizzthrough.android.commons.server.json.AppAddUserOnQueueResponse;
import com.whizzthrough.android.commons.server.json.AppIncreaseCurrentTicketRequest2;
import com.whizzthrough.android.commons.server.json.AppIncreaseCurrentTicketResponse;
import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.Constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import shopping.safelane.android.commons.R;

public class UserView extends RecyclerView.ViewHolder {
    static String getDate(final long time) {
        DateFormat df;
        if (System.currentTimeMillis() - time >= TimeUnit.DAYS.toMillis(1)) {
            df = new SimpleDateFormat("MMM d");
        } else {
            df = new SimpleDateFormat("HH:mm");
        }

        return df.format(new Date(time));
    }
    @NonNull
    static String getString(final String value, final String default_) {
        return value == null ? default_ : value;
    }

    static String getEmail(final String s) {
        final int pos = s.indexOf("://");

        if (pos == -1) {
            return s;
        } else {
            if (s.indexOf('@') == -1) {
                return "NO EMAIL";
            }

            return s.substring(pos + 3);
        }
    }

    static String getTitleLine(final String s) {
        if (s == null) {
            return null;
        } else if (s.startsWith("printed://")) {
            return "PRINTED";
        } else if (s.startsWith("sms://")) {
            return s;
        } else {
            return getEmail(s);
        }
    }

    private final View view;
    private final TextView title;
    private final TextView time;
    private final TextView description;
    private final TextView icon;
    private final ImageButton callingButton;

    public UserView(@NonNull final View view) {
        super(view);

        this.view = view;
        title = view.findViewById(R.id.comp_ticket_summary_item_title);
        time =  view.findViewById(R.id.comp_ticket_summary_item_time);
        description =  view.findViewById(R.id.comp_ticket_summary_item_description);
        icon =  view.findViewById(R.id.comp_ticket_summary_item_icon);
        callingButton = view.findViewById(R.id.comp_ticket_summary_item_bellbutton);
    }

    void bindData(@NonNull final AppAddUserOnQueueResponse model, final boolean callable) {
        title.setText(getTitleLine(getString(model.getEmail(), "NO EMAIL")));
        time.setText(getDate(model.getTakenOn()));
        description.setText(getString(model.getDescription(), "NO DESCRIPTION"));
        icon.setText(Integer.toString(model.getTicket()));
        icon.setBackgroundResource(model.getCalledOn() == 0 ? R.drawable.circle : R.drawable.circle_called);
        callingButton.setVisibility(callable ? View.VISIBLE : View.GONE);
        callingButton.setOnClickListener(v -> {
            final ProgressDialog dialog = ProgressDialog.show(view.getContext(), "Loading", "Please wait...");
            final AppIncreaseCurrentTicketRequest2 request = new AppIncreaseCurrentTicketRequest2();
            request.setQueueId(Config.getQueueDefinition().getId());

            WhizzThroughServerFactory.getDefaultServer().increaseCurrentTicket(
                    view.getContext(),
                    Config.getCookie(),
                    Config.getIncreaseCurrentTicketUrl(),
                    request,
                    new OnIncreaseCurrentTicket(view.getContext()) {
                        @Override
                        public void onResponse(AppIncreaseCurrentTicketResponse response, Object eventsData) {
                            if (response.isNoTicketWaiting()) {

                            }
                        }
                    },
                    null);
        });
    }
}
