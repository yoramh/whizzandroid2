package shopping.safelane.android.commons.activities.startup;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.whizzthrough.android.commons.receivers.ErrorBroadcastReceiver;
import com.whizzthrough.android.commons.server.json.JsonErrorObject;
import com.whizzthrough.android.commons.utils.Config;

import java.util.ArrayList;

import shopping.safelane.android.commons.R;
import shopping.safelane.android.commons.activities.startup.dto.RegistrationHolder;

public class WelcomeFragment extends Fragment {
    private CheckBox agreementText;

    private RegistrationHolder holder;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View root = inflater.inflate(R.layout.startup_welcome, container, false);

        final Bundle bundle = getArguments();

        if (bundle == null) {
            throw new IllegalStateException("Bundle cannot be null");
        }

        holder = (RegistrationHolder)bundle.getSerializable(RegistrationHolder.BUNDLE_KEY);

        if (holder == null) {
            throw new IllegalStateException("Holder not set");
        }

        final Button continueButton = root.findViewById(R.id.continueButton);
        continueButton.setEnabled(false);
        continueButton.setOnClickListener(v -> {
            RegistrationFragment fragment2 = new RegistrationFragment();
            fragment2.setArguments(bundle);
            FragmentManager fragmentManager = getFragmentManager();

            if (fragmentManager !=  null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.hide(this)
                        .replace(R.id.container, fragment2)
                        .addToBackStack(this.getClass().getSimpleName())
                        .commit();
            }
        });
        continueButton.setAlpha(.5f);

        agreementText = root.findViewById(R.id.agreementText);
        agreementText.setOnCheckedChangeListener((buttonView, isChecked) -> {
            continueButton.setEnabled(isChecked);
            continueButton.setAlpha(isChecked ? 1f : .5f);
            agreementText.setChecked(isChecked);
        });
        final String agreementContent = getResources().getString(R.string.slres_welcome_tc);
        agreementText.setText(Html.fromHtml(agreementContent));
        agreementText.setClickable(true);
        agreementText.setMovementMethod(LinkMovementMethod.getInstance());

        return root;
    }
}
