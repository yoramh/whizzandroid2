package shopping.safelane.android.commons.services;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.NonNull;

import com.whizzthrough.android.commons.receivers.ErrorBroadcastReceiver;

import shopping.safelane.android.commons.utils.SystemUtils;
import com.whizzthrough.android.commons.utils.eventsource.InputStreamResult;
import com.whizzthrough.android.commons.utils.eventsource.sync.ISyncEventSourceEvents;
import com.whizzthrough.android.commons.utils.eventsource.sync.SyncEventSource;
import com.whizzthrough.android.commons.utils.eventsource.sync.WhyWasIClosed;

import java.net.HttpURLConnection;

/**
 * Created by yoram on 10/01/16.
 */
public abstract class AbstractSseService extends IntentService implements ISyncEventSourceEvents {
    private SyncEventSource evt;

    public AbstractSseService() {
        super(AbstractSseService.class.getName());
    }

    public AbstractSseService(final String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        while (getUrl(intent) == null) {
            SystemUtils.sleep(500);
        }

        if (needCookie()) {
            while (getCookie(intent) == null) {
                SystemUtils.sleep(500);
            }
        }

        try {
            evt = new SyncEventSource(
                    this,
                    getUrl(intent),
                    getCookie(intent));
        } catch (Throwable t) {
            evt = null;

            return;
        }

        evt.run();
    }

    @Override
    public void onDestroy() {
        evt.close();

        while (evt.isRunning()) {
            SystemUtils.sleep(500);
        }

        evt = null;

        super.onDestroy();
    }

    public abstract void onMessage(String id, String event, String data);

    @Override
    public void onError(HttpURLConnection conn, Throwable t) {
        onError(t.getMessage());
    }

    @Override
    public void onConnectionError(HttpURLConnection conn, InputStreamResult res) {
        onError(res.getError().getMessage());
    }

    @Override
    public void onDisconnected() {
        onError("disconnected");
    }

    @Override
    public void onClose(WhyWasIClosed reason) {
        onError("close");
    }

    @Override
    public void onConnecting() {
    }

    @Override
    public void onConnected() {
        //onError("connected");
    }

    protected abstract String getUrl(@NonNull final Intent intent);

    protected boolean needCookie() {
        return false;
    }

    protected String getCookie(@NonNull final Intent intent) {
        return null;
    }

    protected void onError(String errorMessage) {
        final Intent i = new Intent(ErrorBroadcastReceiver.FILTER_ERROR_MESSAGE);
        i.putExtra(ErrorBroadcastReceiver.BUNDLE_ERROR_MESSAGE, errorMessage);
        sendBroadcast(i);
    }

}
