package shopping.safelane.android.commons.services.server;

import androidx.annotation.NonNull;

import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.IOUtils;
import com.whizzthrough.android.commons.utils.eventsource.EventSourceUtils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import shopping.safelane.android.commons.services.utils.AbstractAsyncTask;

public class SyncEventSourceTask extends AbstractAsyncTask {
    public interface Events {
        void onSuccess(String data, SyncEventSourceTask source);
        void onError(Throwable t, SyncEventSourceTask source);
    }

    private final URL url;
    private final Events event;

    public SyncEventSourceTask(
            @NonNull final String url, @NonNull final Events event) throws MalformedURLException {
        super();

        this.url = new URL(url);
        this.event = event;
    }

    @Override
    protected void run() throws Exception {
        final HttpURLConnection conn = EventSourceUtils.getConnection(this.url, Config.getCookie());
        conn.setRequestProperty("Accept", "application/json");

        conn.setDoInput(true);

        final int code = conn.getResponseCode();

        try (final InputStream in = (code >= 200 && code <= 299) ? conn.getInputStream() : conn.getErrorStream()) {
            final String data = IOUtils.readStreamAsString(in);

            if (code < 200 || code > 299) {
                final Exception e = new Exception(data);
                event.onError(e, this);
            } else {
                event.onSuccess(data, this);
            }
        }
    }

    public URL getUrl() {
        return url;
    }
}
