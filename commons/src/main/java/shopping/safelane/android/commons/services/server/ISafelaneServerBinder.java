package shopping.safelane.android.commons.services.server;

import android.os.IBinder;

import androidx.annotation.NonNull;

import java.net.MalformedURLException;

public interface ISafelaneServerBinder {
    void addServerSentEvent(@NonNull String id, @NonNull String url) throws MalformedURLException;
    void removeServerSentEvent(@NonNull String id, @NonNull String url)throws MalformedURLException;
}
