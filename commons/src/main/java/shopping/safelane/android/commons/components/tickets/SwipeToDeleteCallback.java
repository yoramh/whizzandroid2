package shopping.safelane.android.commons.components.tickets;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.concurrent.atomic.AtomicInteger;

public class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback {
    private TicketSummaryItemAdapter adapter;
    private RecyclerView owner;
    private ItemTouchHelper helper;

    public SwipeToDeleteCallback(TicketSummaryItemAdapter adapter, RecyclerView owner) {
        super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);

        this.adapter = adapter;
        this.owner = owner;
    }
/*
    @Override
    public int getSwipeDirs(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        return ((TicketSummaryItemAdapter)recyclerView.getAdapter()).getTickets()[viewHolder.getAdapterPosition()].getCalledOn() == 0 ?
                ItemTouchHelper.ANIMATION_TYPE_SWIPE_CANCEL :
                super.getSwipeDirs(recyclerView, viewHolder);
    }*/

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        System.out.println("onMove");

        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        helper.attachToRecyclerView(null);
        helper.attachToRecyclerView(owner);
        /*
        //adapter.notifyDataSetChanged();
        adapter.notifyItemChanged(viewHolder.getAdapterPosition());
        Runnable task = () -> {
            try {
                Thread.sleep(2000);
                adapter.notifyItemChanged(viewHolder.getAdapterPosition());
                adapter.notifyDataSetChanged();
            } catch (Throwable t) {
                t.printStackTrace();
            }
        };

        new Handler(Looper.getMainLooper()).post(task);
        System.out.println("swipe");*/
    }

    public void setHelper(ItemTouchHelper helper) {
        this.helper = helper;
    }
}