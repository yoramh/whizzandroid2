package shopping.safelane.android.commons.activities.startup.challenge;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegSmsBroadcastReceiver extends BroadcastReceiver  {
    private final ChallengeFragment activity;

    public RegSmsBroadcastReceiver() {
        super();

        this.activity = null;
    }

    public RegSmsBroadcastReceiver(@NonNull final ChallengeFragment activity) {
        super();

        this.activity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();

            if (extras == null) {
                return;
            }

            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

            if (status == null) {
                return;
            }

            if (status.getStatusCode() == CommonStatusCodes.SUCCESS) {
                String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);

                if (message == null) {
                    return;
                }

                Pattern pattern = Pattern.compile("(\\d{4})");
                Matcher matcher = pattern.matcher(message);
                if (matcher.find()) {
                    final String code = matcher.group(1);

                    if (code != null && activity != null) {
                        activity.onCode(code);
                    }
                }
            }
                    /*
                case CommonStatusCodes.TIMEOUT:
                    // Waiting for SMS timed out (5 minutes)
                    // Handle the error ...
                    break;*/
        }
    }
}
