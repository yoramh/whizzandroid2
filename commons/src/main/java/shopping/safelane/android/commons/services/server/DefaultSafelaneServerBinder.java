package shopping.safelane.android.commons.services.server;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

public class DefaultSafelaneServerBinder implements ISafelaneServerBinder {
    private static class UrlKeep {
        private final Set<String> ids = new HashSet<>();
        private SyncEventSourceTask event;
    }

    public static class AsyncBroadcast implements Serializable {
        private final String url;
        private final String data;

        private AsyncBroadcast(@NonNull final String url, @NonNull final String data) {
            super();

            this.url = url;
            this.data = data;
        }

        public String getUrl() {
            return url;
        }

        public String getData() {
            return data;
        }
    }

    public static class AsyncBroadcastError implements Serializable {
        private final String url;
        private final Throwable error;

        private AsyncBroadcastError(@NonNull final String url, @NonNull final Throwable error) {
            super();

            this.error = error;
            this.url = url;
        }

        public String getUrl() {
            return url;
        }

        public Throwable getError() {
            return error;
        }
    }

    private final SyncEventSourceTask.Events events = new SyncEventSourceTask.Events() {
        @Override
        public void onSuccess(String data, SyncEventSourceTask source) {
            broadcastJson(
                    context,
                    IntentMessageType.FILTER_EVENT,
                    IntentMessageType.MSG_DATA,
                    new AsyncBroadcast(source.getUrl().toString(), data));
        }

        @Override
        public void onError(Throwable t, SyncEventSourceTask source) {
            broadcastJson(
                    context,
                    IntentMessageType.FILTER_EVENT,
                    IntentMessageType.MSG_ERROR,
                    new AsyncBroadcastError(source.getUrl().toString(), t));
        }
    };


    private final Map<String, UrlKeep> map = new HashMap<>();
    private final ReentrantLock lock = new ReentrantLock();
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);
    private final Context context;

    static void broadcastJson(
            final Context context,
            final String intentMessage,
            final String bundleName,
            final Serializable o) {

        final Intent i = new Intent(intentMessage);
        final Bundle b = new Bundle();
        b.putSerializable(bundleName, o);
        i.putExtras(b);
        context.sendBroadcast(i);

    }

    DefaultSafelaneServerBinder(@NonNull final Context context) {
        super();

        this.context = context;
    }

    @Override
    public void addServerSentEvent(@NonNull String id, @NonNull String url) throws MalformedURLException {
        lock.lock();

        try {
            UrlKeep keep =  map.get(url);

            if (keep == null) {
                keep = new UrlKeep();
                keep.event = new SyncEventSourceTask(url, events);
                map.put(url, keep);

                keep.event.execute();
            }

            keep.ids.add(id);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void removeServerSentEvent(@NonNull String id, @NonNull String url) {
        lock.lock();

        try {
            final UrlKeep keep =  map.get(url);

            if (keep != null) {
                keep.ids.remove(id);

                if (keep.ids.isEmpty()) {
                    keep.event.close();
                    keep.event.cancel(true);
                    map.remove(url);
                }
            }
        } finally {
            lock.unlock();
        }
    }
}
