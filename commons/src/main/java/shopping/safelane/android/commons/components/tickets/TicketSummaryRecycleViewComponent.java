package shopping.safelane.android.commons.components.tickets;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.AttributeSet;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.whizzthrough.android.commons.server.json.AppAddUserOnQueueResponse;
import com.whizzthrough.android.commons.server.json.UpdatedQueueInfoResponse;

import shopping.safelane.android.commons.services.server.DefaultSafelaneServerBinder;
import shopping.safelane.android.commons.services.server.ISafelaneServerBinder;
import shopping.safelane.android.commons.services.server.IntentMessageType;

/**
 * Created by yoram on 18/05/2020.
 */
public class TicketSummaryRecycleViewComponent extends RecyclerView {
    private AppAddUserOnQueueResponse[] tickets;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (IntentMessageType.FILTER_EVENT.equals(intent.getAction()) && intent.hasExtra(IntentMessageType.MSG_DATA)) {
                try {
                    DefaultSafelaneServerBinder.AsyncBroadcast o = (DefaultSafelaneServerBinder.AsyncBroadcast)intent.getSerializableExtra(IntentMessageType.MSG_DATA);

                    if (o != null) {
                        UpdatedQueueInfoResponse response = new Gson().fromJson(o.getData(), UpdatedQueueInfoResponse.class);
                        tickets = response.getUserOnQueues().toArray(new AppAddUserOnQueueResponse[0]);
                        final Adapter<UserView> adapter = new TicketSummaryItemAdapter(getContext(), tickets);
                        setAdapter(adapter);
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }
    };

    public TicketSummaryRecycleViewComponent(Context context) {
        super(context);

        init();
    }

    public TicketSummaryRecycleViewComponent(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public TicketSummaryRecycleViewComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    private void init() {
        setHasFixedSize(true);
        setLayoutManager(new LinearLayoutManager(this.getRootView().getContext()));

        final TicketSummaryItemAdapter adapter;
        if (isInEditMode()) {
            // Large list so we can see how it fits the screen
            final AppAddUserOnQueueResponse[] dummy = new AppAddUserOnQueueResponse[30];

            for (int i = 0; i < dummy.length; i++) {
                dummy[i] = new AppAddUserOnQueueResponse();
            }

            adapter = new TicketSummaryItemAdapter(getContext(), dummy);
        } else if (tickets == null) {
            adapter = new TicketSummaryItemAdapter(getContext(), new AppAddUserOnQueueResponse());
        } else {
            adapter = new TicketSummaryItemAdapter(getContext(), tickets);
        }

        setAdapter(adapter);
        SwipeToDeleteCallback cb = new SwipeToDeleteCallback(adapter, this);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(cb);
        cb.setHelper(itemTouchHelper);
        itemTouchHelper.attachToRecyclerView(this);

    }

    public TicketSummaryRecycleViewComponent tickets(AppAddUserOnQueueResponse[] tickets) {
        this.tickets = tickets;
        init();
        invalidate();

        return this;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(IntentMessageType.FILTER_EVENT);
        getActivity().registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        try {
            getActivity().unregisterReceiver(receiver);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private Activity getActivity() {
        Context context = getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }
}
