package shopping.safelane.android.commons.services.utils;

import android.os.AsyncTask;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AbstractAsyncTask extends AsyncTask<Void, Void, Void> {
    private final AtomicBoolean closed = new AtomicBoolean(false);
    private final AtomicBoolean interrupted = new AtomicBoolean(false);
    private Throwable error;

    @Override
    protected Void doInBackground(Void... strings) {
        try {
            while (!closed.get()) {
                try {
                    run();
                } catch (Throwable t) {
                    t.printStackTrace();
                }

                Thread.sleep(sleepBetweenRun());
            }
        } catch (InterruptedException e) {
            interrupted.set(true);
        } catch (Throwable t) {
            error = t;
        }

        closed.set(true);

        return null;
    }

    protected abstract void run() throws Exception;

    protected long sleepBetweenRun() {
        return TimeUnit.SECONDS.toMillis(2);
    }

    public void close() {
        closed.set(true);
    }
}
