package shopping.safelane.android.commons.fragments.impl;

import com.whizzthrough.android.commons.fragments.impl.AbstractCommand;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.ISynchroWhizzThroughServer;
import com.whizzthrough.android.commons.utils.Config;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 22/05/20.
 */
public class SetQueueOpenCloseCommand extends AbstractCommand {
    private static final ISynchroWhizzThroughServer SERVER = (ISynchroWhizzThroughServer) WhizzThroughServerFactory.getDefaultServer();

    public static final String COMMAND_NAME = SetQueueOpenCloseCommand.class.getName();

    private final long queueId;
    private final boolean open;

    public SetQueueOpenCloseCommand(final long queueId, final boolean open) {
        super();

        this.queueId = queueId;
        this.open = open;
    }

    @Override
    protected void doExecute() throws Exception {
        if (open) {
            SERVER.openQueue(Config.getCookie(), queueId);
        } else {
            SERVER.closeQueue(Config.getCookie(), queueId);
        };
    }

    @Override
    public String name() {
        return COMMAND_NAME;
    }

    @Override
    public int label() {
        return R.string.slres_progress_action_get_queue_status;
    }
}
