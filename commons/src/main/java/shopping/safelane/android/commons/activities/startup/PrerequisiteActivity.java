package shopping.safelane.android.commons.activities.startup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.whizzthrough.android.commons.fragments.impl.AbstractCommand;
import com.whizzthrough.android.commons.server.DefaultWhizzThroughServer;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.json.EApplication;
import com.whizzthrough.android.commons.server.json.ErrorResponse;
import com.whizzthrough.android.commons.utils.Config;

import shopping.safelane.android.commons.R;

public class PrerequisiteActivity  extends AppCompatActivity {
    /*
    public static final String EXTRA_MESSAGE = "MESSAGE";
    public static final String EXTRA_APPLICATION = "APPLICATION";
    public static final int RESULT_NOT_SUPPORTED = 0;
    public static final int RESULT_SUPPORTED = 1;
    public static final int RESULT_EXIT = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        setContentView(R.layout.startup_prerequisite);

        final TextView messageText = findViewById(R.id.messageText);
        final String message = getString(getIntent().getIntExtra(EXTRA_MESSAGE, 0));
        messageText.setText(message);
        final EApplication application = (EApplication)getIntent().getSerializableExtra(EXTRA_APPLICATION);

        WhizzThroughServerFactory.getDefaultServer().getVersionInfo(
                getApplicationContext(),
                Config.getGetVersionInfoUrl(),
                new IOnGetVersionInfo() {
                    @Override
                    public void onResponse(PubGetVersionInfoResponse response, Object eventsData) {
                        setResult(
                                response.isVersionSupported() ? RESULT_SUPPORTED : RESULT_NOT_SUPPORTED,
                                new Intent());
                        finish();
                    }

                    @Override
                    public void onError(@NonNull Throwable t, @Nullable Object eventsData) {
                        onResponse(new PubGetVersionInfoResponse().versionSupported(true), eventsData);
                    }

                    @Override
                    public void onError(@NonNull ErrorResponse response, @Nullable Object eventsData) {
                        onResponse(new PubGetVersionInfoResponse().versionSupported(true), eventsData);
                    }

                    @Override
                    public void onUnauthorized() {
                        onResponse(new PubGetVersionInfoResponse().versionSupported(true), null);
                    }
                },
                null,
                application);

    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_EXIT, new Intent());
        finish();
    }*/
}

