package shopping.safelane.android.commons.activities.startup.dto;

import com.whizzthrough.android.commons.server.json.PubRegisterAccountResponse;

import java.io.Serializable;

public class RegistrationHolder implements Serializable {
    public static String BUNDLE_KEY = RegistrationHolder.class.getSimpleName() + ".BUNDLE_KEY";

    private String registrationNameLabel;
    private String name;
    private String phone;
    private PubRegisterAccountResponse registerAccountResponse;
    private String code;
    private String password;

    public String getRegistrationNameLabel() {
        return registrationNameLabel;
    }

    public RegistrationHolder registrationNameLabel(String registrationNameLabel) {
        this.registrationNameLabel = registrationNameLabel;

        return this;
    }

    public String getName() {
        return name;
    }

    public RegistrationHolder name(String name) {
        this.name = name;

        return this;
    }

    public String getPhone() {
        return phone;
    }

    public RegistrationHolder phone(String phone) {
        this.phone = phone;

        return this;
    }

    public PubRegisterAccountResponse getRegisterAccountResponse() {
        return registerAccountResponse;
    }

    public RegistrationHolder registerAccountResponse(PubRegisterAccountResponse registerAccountResponse) {
        this.registerAccountResponse = registerAccountResponse;

        return this;
    }

    public String getCode() {
        return code;
    }

    public RegistrationHolder code(String code) {
        this.code = code;

        return this;
    }

    public String getPassword() {
        return password;
    }

    public RegistrationHolder password(String password) {
        this.password = password;

        return this;
    }
}
