package shopping.safelane.android.commons.activities.startup.challenge;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.OnLogin;
import com.whizzthrough.android.commons.server.api.OnResendRegistrationSms;
import com.whizzthrough.android.commons.server.api.OnValidateRegistration;
import com.whizzthrough.android.commons.server.json.EApplication;
import com.whizzthrough.android.commons.server.json.PubRegisterAccountResponse;
import com.whizzthrough.android.commons.server.json.PubResendRegistrationSmsRequest;
import com.whizzthrough.android.commons.server.json.PubValidateRegistrationRequest;
import com.whizzthrough.android.commons.utils.AppSignatureHelper;
import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.threads.AbstractRunnable;
import com.whizzthrough.android.commons.utils.threads.EThreadAction;

import shopping.safelane.android.commons.R;
import shopping.safelane.android.commons.activities.startup.OnFinishEvent;
import shopping.safelane.android.commons.activities.startup.dto.RegistrationHolder;

public class ChallengeFragment extends Fragment {
    private class TimerResend extends AbstractRunnable {
        private Long endTime;
        private final long time;

        private TimerResend(final long time) {
            super();

            this.time = time;
        }

        @Override
        protected EThreadAction doRun() {
            try {
                if (endTime == null) {
                    endTime = System.currentTimeMillis() + time;
                }

                getActivity().runOnUiThread(() -> {
                        try {
                            sendAgainButton.setText(
                                    getString(
                                            R.string.slres_button_resend,
                                            (endTime - System.currentTimeMillis()) / 1000));
                        } catch (Throwable t) {
                            cancel();
                        }
                });

                final EThreadAction res = endTime <= System.currentTimeMillis() ? EThreadAction.FINISH : EThreadAction.CONTINUE;

                if (res == EThreadAction.FINISH) {
                    canResend();
                }

                return res;
            } catch (Throwable t) {
                return EThreadAction.FINISH;
            }
        }
    }

    public static final int RESULT_OK = 0;
    public static final int RESULT_EXIT = 1;

    public static final String EXTRA_PHONE = "PHONE";
    public static final String EXTRA_FROM_SERVER = "FROM_SERVER";
    public static final String EXTRA_UNIQUE_ID = "UNIQUE_ID";
    public static final String EXTRA_USER_ID = "USER_ID";
    public static final String EXTRA_CODE = "CODE";

    private EditText code;
    private Button sendAgainButton;
    private TimerResend resendRunnable;
    private RegSmsBroadcastReceiver registerReceiver = new RegSmsBroadcastReceiver(this);

    private RegistrationHolder holder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final View root = inflater.inflate(R.layout.startup_challenge, container, false);

        final Bundle bundle = getArguments();

        if (bundle == null) {
            throw new IllegalStateException("Bundle cannot be null");
        }

        holder = (RegistrationHolder)bundle.getSerializable(RegistrationHolder.BUNDLE_KEY);

        if (holder == null) {
            throw new IllegalStateException("Holder not set");
        }

        if (holder.getPhone() == null) {
            throw new IllegalArgumentException("Intent does not include EXTRA_PHONE");
        }

        if (holder.getRegisterAccountResponse() == null) {
            throw new IllegalArgumentException("Intent does not include EXTRA_FROM_SERVER");
        }

        if (holder.getRegisterAccountResponse().getUniqueId() == null) {
            throw new IllegalArgumentException("Intent does not include EXTRA_UNIQUE_ID");
        }

        final TextView phoneNumber = root.findViewById(R.id.phoneNumber);
        phoneNumber.setText(holder.getPhone());

        code = root.findViewById(R.id.codeEdit);

        final Button verifyButton = root.findViewById(R.id.verifyButton);
        verifyButton.setEnabled(false);
        verifyButton.setAlpha(0.5f);
        verifyButton.setOnClickListener(v -> verify());

        final TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                final boolean enabled = code.getText().length() == 4;
                verifyButton.setEnabled(enabled);
                verifyButton.setAlpha(enabled ? 1f : .5f);
            }
        };
        code.addTextChangedListener(watcher);

        sendAgainButton = root.findViewById(R.id.sendAgainButton);
        sendAgainButton.setEnabled(false);
        sendAgainButton.setAlpha(.5f);
        sendAgainButton.setOnClickListener(v -> {
            final PubResendRegistrationSmsRequest request = new PubResendRegistrationSmsRequest()
                    .userId(holder.getRegisterAccountResponse().getUserId())
                    .callerReturnInfo(new AppSignatureHelper(this.getContext()).getAppSignature())
                    .uniqueId(holder.getRegisterAccountResponse().getUniqueId());

            WhizzThroughServerFactory.getDefaultServer().resendRegistrationSms(
                    this.getContext(),
                    request,
                    new OnResendRegistrationSms(this.getContext()) {
                        @Override
                        public void onResponse(Object eventsData) {
                            resendRunnable = new TimerResend(holder.getRegisterAccountResponse().getRetryTimeOut());
                            new Thread(resendRunnable).start();
                            verifyButton.setEnabled(false);
                            verifyButton.setAlpha(0.5f);
                        }
                    },
                    null,
                    null);
        });

        final SmsRetrieverClient client = SmsRetriever.getClient(this.getContext());
        client.startSmsRetriever();

        IntentFilter i = new IntentFilter("com.google.android.gms.auth.api.phone.SMS_RETRIEVED");
        getActivity().registerReceiver(registerReceiver, i);

        resendRunnable = new TimerResend(holder.getRegisterAccountResponse().getRetryTimeOut());
        new Thread(resendRunnable).start();

        return root;

    }

    @Override
    public void onDestroyView() {
        if (resendRunnable != null) {
            resendRunnable.cancel();
        }

        //unregisterReceiver(registerReceiver);

        super.onDestroyView();
    }


    private void canResend() {
        getActivity().runOnUiThread(() -> {
            sendAgainButton.setAlpha(1f);
            sendAgainButton.setEnabled(true);
            sendAgainButton.setText(getString(R.string.slres_button_resend_enabled));
        });
    }

    private void verify() {
        final PubValidateRegistrationRequest request = new PubValidateRegistrationRequest()
                .challengeData(code.getText().toString())
                .uniqueId(holder.getRegisterAccountResponse().getUniqueId())
                .application(EApplication.SAFELANE);

        WhizzThroughServerFactory.getDefaultServer().validateRegistration(
                this.getContext(),
                request,
                new OnValidateRegistration(Config.getCurrentContext() == null ? this.getContext() : Config.getCurrentContext()) {
                    @Override
                    public void onResponse(Object eventsData) {
                        if (resendRunnable != null) {
                            resendRunnable.cancel();
                            resendRunnable = null;
                        }

                        if (getContext() instanceof OnFinishEvent) {
                            getActivity().runOnUiThread(() -> ((OnFinishEvent) getContext()).start());
                        }

                        holder.code(code.getText().toString());
                    }
                },
                null,
                null);
    }

    void onCode(@NonNull final String code) {
        this.code.setText(code);

        if (resendRunnable != null) {
            resendRunnable.cancel();
        }

        //verify();
    }
}
