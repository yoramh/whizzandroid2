package shopping.safelane.android.commons.services.server;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SafelaneServerService extends Service {
    public class LocalBinder extends Binder {
        public SafelaneServerService getService() {
            // Return this instance of LocalService so clients can call public methods
            return SafelaneServerService.this;
        }
    }

    private final IBinder mBinder = new LocalBinder();
    private final ISafelaneServerBinder safelaneServerBinder;

    public SafelaneServerService(@NonNull final ISafelaneServerBinder safelaneServerBinder) {
        super();

        this.safelaneServerBinder = safelaneServerBinder;
    }

    public SafelaneServerService() {
        super();

        safelaneServerBinder = new DefaultSafelaneServerBinder(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public ISafelaneServerBinder getSafelaneServerBinder() {
        return safelaneServerBinder;
    }
}
