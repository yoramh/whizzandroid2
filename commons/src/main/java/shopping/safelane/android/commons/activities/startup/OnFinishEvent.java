package shopping.safelane.android.commons.activities.startup;

public interface OnFinishEvent {
    void start();
}
