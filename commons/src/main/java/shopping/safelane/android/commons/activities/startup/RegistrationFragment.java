package shopping.safelane.android.commons.activities.startup;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.common.util.Hex;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.hbb20.CountryCodePicker;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.OnRegister;
import com.whizzthrough.android.commons.server.json.EApplication;
import com.whizzthrough.android.commons.server.json.ERegistrationChallenge;
import com.whizzthrough.android.commons.server.json.PubRegisterAccountRequest;
import com.whizzthrough.android.commons.server.json.PubRegisterAccountResponse;
import com.whizzthrough.android.commons.utils.AppSignatureHelper;

import shopping.safelane.android.commons.R;
import shopping.safelane.android.commons.activities.startup.challenge.ChallengeFragment;
import shopping.safelane.android.commons.activities.startup.dto.RegistrationHolder;
import shopping.safelane.android.commons.utils.SystemUtils;

public class RegistrationFragment extends Fragment {
    private static final String TAG = RegistrationFragment.class.getSimpleName();

    public static final int RESULT_OK = 0;
    public static final int RESULT_EXIT = 1;

    public static final String EXTRA_NAME_LABEL = "NAME_LABEL";
    public static final String EXTRA_NAME = "NAME";
    public static final String EXTRA_PHONE = "PHONE";

    private Button verifyButton;
    private EditText name;
    private EditText phone;

    private RegistrationHolder holder;

    @SuppressLint("SetTextI18n")
    @Override
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final View root = inflater.inflate(R.layout.startup_registration, container, false);

        final Bundle bundle = getArguments();

        if (bundle == null) {
            throw new IllegalStateException("Bundle cannot be null");
        }

        holder = (RegistrationHolder)bundle.getSerializable(RegistrationHolder.BUNDLE_KEY);

        if (holder == null) {
            throw new IllegalStateException("Holder not set");
        }

        final TextView nameLabel = root.findViewById(R.id.nameLabel);
        nameLabel.setText(holder.getRegistrationNameLabel());

        name = root.findViewById(R.id.nameText);
        name.setText(holder.getName() != null ? holder.getName() : "");

        final CountryCodePicker ccp = root.findViewById(R.id.phoneCCP);

        phone = root.findViewById(R.id.phoneText);
        phone.setText(holder.getPhone() != null ? holder.getName() : "");
        ccp.registerCarrierNumberEditText(phone);

        if (holder.getPhone() != null) {
            try {
                final Phonenumber.PhoneNumber o = PhoneNumberUtil.getInstance().parse("+" + holder.getPhone(), "US");
                final int countryCode = o.getCountryCode();
                ccp.setCountryForPhoneCode(countryCode);
                phone.setText(Long.toString(o.getNationalNumber()));
                System.out.println(countryCode);
            } catch (Throwable t) {
                Log.d(TAG, t.getMessage(), t);
            }
        }

        verifyButton = root.findViewById(R.id.verifyButton);
        verifyButton.setEnabled(false);
        verifyButton.setOnClickListener(v -> {
            holder.name(name.getText().toString())
                    .phone(ccp.getFullNumber())
                    .password(Hex.bytesToStringLowercase(SystemUtils.random(16)));

            final PubRegisterAccountRequest request = new PubRegisterAccountRequest()
                    .application(EApplication.SAFELANE)
                    .name(holder.getName())
                    .password(holder.getPassword())
                    .userId(holder.getPhone())
                    .locale(SystemUtils.getLocaleLanguage(this.getContext()))
                    .callerReturnInfo(new AppSignatureHelper(this.getContext()).getAppSignature());

            WhizzThroughServerFactory.getDefaultServer().register(
                    this.getContext(),
                    request,
                    new OnRegister(this.getContext()) {
                        @Override
                        public void onResponse(PubRegisterAccountResponse response, Object eventsData) {
                            if (response.getChallenge() == ERegistrationChallenge.SMS) {
                                getActivity().runOnUiThread(() -> {
                                    holder.registerAccountResponse(response);
                                    ChallengeFragment fragment2 = new ChallengeFragment();
                                    fragment2.setArguments(bundle);
                                    FragmentManager fragmentManager = getFragmentManager();

                                    if (fragmentManager !=  null) {
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.hide(RegistrationFragment.this)
                                            .replace(R.id.container, fragment2)
                                                .addToBackStack(this.getClass().getSimpleName())
                                                .commit();
                                    }
                                });
                            }
                        }
                    },
                    null,
                    null);


            ////
        });

        final TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                refreshVerifyButtonEnabled();
            }
        };

        name.addTextChangedListener(watcher);
        phone.addTextChangedListener(watcher);
        refreshVerifyButtonEnabled();

        return root;
    }

    private void refreshVerifyButtonEnabled() {
        final boolean enabled = name.getText().length() > 1 && phone.getText().length() > 4;
        verifyButton.setEnabled(enabled);
        verifyButton.setAlpha(enabled ? 1f : .5f);
    }
}
