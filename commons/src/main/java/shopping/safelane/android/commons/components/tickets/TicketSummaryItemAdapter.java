package shopping.safelane.android.commons.components.tickets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.whizzthrough.android.commons.server.json.AppAddUserOnQueueResponse;
import com.whizzthrough.android.commons.utils.Constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 24/02/16.
 */
public class TicketSummaryItemAdapter extends RecyclerView.Adapter<UserView> {
    private final AppAddUserOnQueueResponse[] tickets;
    private final Context context;

    public TicketSummaryItemAdapter(final Context context, final AppAddUserOnQueueResponse... tickets) {
        super();

        this.tickets = tickets;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return this.tickets.length;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.comp_ticket_summary_item;
    }

    @NonNull
    @Override
    public UserView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);

        return new UserView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserView holder, final int position) {
        holder.bindData(tickets[position], position == 0 && tickets[position].getCalledOn() == 0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public AppAddUserOnQueueResponse[] getTickets() {
        return this.tickets;
    }
}
