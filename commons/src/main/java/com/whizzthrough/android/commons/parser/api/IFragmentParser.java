package com.whizzthrough.android.commons.parser.api;

import android.content.Context;
import com.whizzthrough.android.commons.fragments.AbstractFragment;

import java.util.Map;

/**
 * Created by yoram on 22/02/16.
 */
public interface IFragmentParser {
    AbstractFragment getFragment(Context context, Map<String, Object> properties);
}
