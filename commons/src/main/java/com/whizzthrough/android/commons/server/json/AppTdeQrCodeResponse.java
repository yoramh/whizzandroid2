package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 17/10/15.
 */
public class AppTdeQrCodeResponse extends BaseResponse {
    private static final long serialVersionUID = 5926071376375470802L;

    private String qrCode;

    public String getQrCode() {
        return this.qrCode;
    }

    public void setQrCode(final String qrCode) {
        this.qrCode = qrCode;
    }
}
