package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 21/10/15.
 */
public class UserGetCurrentQueueStatusResponse extends BaseResponse {
    private static final long serialVersionUID = 7311595145824212153L;

    private QueuePosition[] positions;

    public QueuePosition[] getPositions() {
        return this.positions;
    }

    public void setPositions(QueuePosition[] positions) {
        this.positions = positions;
    }
}
