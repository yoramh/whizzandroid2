package com.whizzthrough.android.commons.utils;

import java.io.*;
import java.util.Date;
import java.util.Properties;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import androidx.annotation.NonNull;
import android.util.Base64;
import android.util.Log;
import com.whizzthrough.android.commons.server.json.QueueDefinition;
import com.whizzthrough.android.commons.utils.http.HttpUtils;

public class Config {
    public enum EPrinterType {
        PRINTER_KOOLERTRON
    }

    private static Context currentContext;

    private static final Properties CONFIG = new Properties();

    private static QueueDefinition queueDefinition = null;

    public static Context getCurrentContext() {
        return currentContext;
    }

    public static void setCurrentContext(Context currentContext) {
        Config.currentContext = currentContext;
        Config.load();
    }

    static {
        loadExtraData();
    }

    private static void loadExtraData() {
        try {
            //Uri u = Uri.parse(Environment.getExternalStorageDirectory() + "/whizzthrough/config.properties");
            Uri u = Uri.parse("/data/data/shopping.safelane.android.apps.getticket/config.properties");
            File f = new File(u.getPath());
            if (f.exists()) {
                InputStream in = new FileInputStream(f);

                try {
                    final Properties tmp = new Properties();
                    tmp.load(in);

                    if (tmp.getProperty("enabled", "true").toLowerCase().equals("true")) {
                        Log.i("WT", "using override property file");
                        CONFIG.putAll(tmp);
                    } else {
                        Log.i("WT", "not using override property file");
                    }
                } finally {
                    in.close();
                }
            }
        } catch (Throwable t) {
            Log.e("WT", "couldn't load properties file, it is not in development mode: " + t.getMessage(), t);
            t.printStackTrace();
        }
    }
    private static void load() {
        try {
            File f = currentContext.getFilesDir();
            f.mkdir();
            InputStream in = currentContext.openFileInput("data.props");

            try {
                if (in != null) {
                    CONFIG.clear();
                    loadExtraData();
                    CONFIG.load(in);
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            Log.e("Config.load(" + currentContext + ")", e.getMessage(), e);
        }
    }

    public static void save() {
        try {
            OutputStream out = currentContext.openFileOutput("data.props", Context.MODE_PRIVATE);
            
            try {
                CONFIG.store(out, "Saved on " + new Date().toString());            
            } finally {
                out.close();
            }
        } catch (IOException e) {
            Log.e("Config.save()", e.getMessage(), e);
        }        
    }
    
    private static long getLong(String key, long defaultValue) {
        String s = CONFIG.getProperty(key);
        
        if (s != null) {
            try {
                return Long.parseLong(s);
            } catch (NumberFormatException e) {
            }
        }
        
        return defaultValue;
    }

    public static String getUsername() {
        return CONFIG.getProperty("credentials.username");
    }

    public static void setUsername(String username) {
        if (username != null) {
            username = username.trim();

            if (username.isEmpty()) {
                username = null;
            }
        }

        if (username == null) {
            CONFIG.remove("credential.username");
        } else {
            CONFIG.setProperty("credentials.username", username);
        }
    }
    
    public static String getPassword() {
        return CONFIG.getProperty("credentials.password");
    }
    
    public static void setPassword(final String password) {
        if (password == null) {
            CONFIG.remove("credential.password");
        } else {
            CONFIG.setProperty("credentials.password", password);
        }
    }
    
    public static long getWaitingPullingTime() {
        return getLong("time.pull.waiting", 10000);
    }
    
    public static void setWaitingPullingTime(long waitingPullingTime) {
        CONFIG.setProperty("time.pull.waiting", "" + waitingPullingTime);
    }

    public static boolean isAutomaticLogin() {
        final String autologin = CONFIG.getProperty("autologin");

        return autologin == null ? true : autologin.toLowerCase().equals("true");
    }

    public static void setAutomaticLogin(boolean automaticLogin) {
        CONFIG.setProperty("autologin", automaticLogin ? "true" : "false");
    }

    public static String getCookie() {
        return CONFIG.getProperty("cookie", null);
    }

    public static void setCookie(final String cookie) {
        if (cookie == null) {
            CONFIG.remove("cookie");
        } else {
            CONFIG.setProperty("cookie", cookie);
        }
    }

    public static long getAlarmTimeForTicket(final String alias) {
        final String key = "default.alarm.time." + alias;

        if (CONFIG.containsKey(key)) {
            return Long.parseLong(CONFIG.getProperty(key));
        } else {
            return -1;
        }
    }

    public static void setAlarmTimeForTicket(final String alias, final long defaultAlarmTime) {
        final String key = "default.alarm.time." + alias;
        CONFIG.setProperty(key, "" + defaultAlarmTime);
    }

    public static boolean hasAlarmed(String alias) {
        return CONFIG.containsKey("alias.has.alarmed" + alias);
    }

    public static void setAlarmed(String alias, boolean hasAlarmed) {
        if (!hasAlarmed) {
            CONFIG.remove("alias.has.alarmed" + alias);
        } else {
            CONFIG.setProperty("alias.has.alarmed" + alias, "true");
        }
    }

    public static boolean hasAlarmedCalled(String alias) {
        return CONFIG.containsKey("alias.has.alarmed.called" + alias);
    }

    public static void setAlarmedCalled(String alias, boolean hasAlarmed) {
        if (!hasAlarmed) {
            CONFIG.remove("alias.has.alarmed" + alias);
        } else {
            CONFIG.setProperty("alias.has.alarmed.called" + alias, "true");
        }
    }

    public static String getFeedbackUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/public/tickets/feedback";
    }

    public static String getRegisterUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/v3/public/registration/register";
    }

    public static String getRegisterValidateUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/v3/public/registration/validate";
    }

    public static String getResendRegistrationSms() {
        return getPrefixUrl() + "/wt-qmgr/services/v3/public/registration/resend-sms";
    }

    public static String getLoginUrl() {
        return getLoginUrl(getPrefixUrl());
    }

    public static String getSaveWtPassUrl(String prefixUrl) {
        return prefixUrl + "/wt-qmgr/services/v2/public/wtpass/save";
    }

    public static String getSaveWtPassUrl() {
        return getSaveWtPassUrl(getPrefixUrl());
    }

    public static String getLoginUrl(String prefixUrl) {
        return prefixUrl + "/wt-qmgr/services/public/login";
    }

    public static String getListQueuesUrl() {
        return getListQueuesUrl(getPrefixUrl());
    }

    public static String getListQueuesUrl(String prefixUrl) {
        return prefixUrl + "/wt-qmgr/services/app/queues/list";
    }

    public static String getQueueStatusUrl() {
        return getQueueStatusUrl(getPrefixUrl());
    }

    public static String getQueueStatusUrl(String prefixUrl) {
        return prefixUrl + "/wt-qmgr/services/v3/app/queue/status/get";
    }

    public static String setQueueStatusUrl() {
        return getQueueStatusUrl(getPrefixUrl());
    }

    public static String setQueueStatusUrl(String prefixUrl) {
        return prefixUrl + "/wt-qmgr/services/v3/app/queue/status/set";
    }

    public static String getListUserOnQueueUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/app/queues/manual/list";
    }


    public static String getCurrentQueueStatusUrl(String prefixUrl) {
        return prefixUrl + "/wt-qmgr/services/user/get-current-queue-status";
    }

    public static String getAllMyTicketsUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/v2/public/tickets/get";
    }

    public static String getRegisterTdeUrl(String prefixUrl) {
        return prefixUrl + "/wt-qmgr/services/v2/sse/terminal/queue/serveQueue";
    }

    public static String getRegisterTdeUrl() {
        return getRegisterTdeUrl(getPrefixUrl());
    }

    public static String getDeleteTicketUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/v2/public/tickets/delete";
    }

    public static String getGetUserStateUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/app/user-state";
    }

    public static String getGrabTicketUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/v2/public/tickets/grab";
    }

    public static String getIncreaseCurrentTicketUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/v2/app/ticket/queues/increase";
    }

    public static String getCollectAccountInformationUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/v2/public/accounts/collect";
    }

    public static String getSetTicketDescriptionUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/v2/app/tickets/set-description";
    }

    public static String getSendMessageUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/v2/app/tickets/send-message";
    }

    public static String getQueueInfoForTerminalUrl() {
        return getPrefixUrl() + "/wt-qmgr/services/v2/app/terminal/queue/info";
    }

    @NonNull
    public static String getMyTicketsSseUrl(@NonNull final String email) {
        return getPrefixUrl() + "/wt-qmgr/services/sse/public/mytickets?email=" + HttpUtils.encodeUrl(email);
    }

    public static String getPrefixUrl() {
        String res = CONFIG.getProperty("url.prefix");

        return (res == null) ? "https://safelane.shopping" : res;
    }
    
    public static void setPrefixUrl(String prefixUrl) {
        if (prefixUrl.length() == 0) {
            CONFIG.remove("url.prefix");
        } else {
            CONFIG.setProperty("url.prefix", prefixUrl);            
        }
    }    

    public static boolean isDisableSSlVerification() {
        String res = CONFIG.getProperty("ssl.disableVerification");
        
        return true;// (res == null) ? false : Boolean.valueOf(res);
    }

    public static EPrinterType getPrinterModel() {
        return EPrinterType.valueOf(CONFIG.getProperty("printer.model", EPrinterType.PRINTER_KOOLERTRON.toString()));
    }

    public static void setPrinterModel(final EPrinterType printerType) {
        if (printerType == null) {
            CONFIG.remove("printer.model");
        } else {
            CONFIG.setProperty("printer.model", printerType.toString());
        }
    }

    public static String getUniqueCode(final boolean create) {
        String res = CONFIG.getProperty("uniqueCode");

        if (res == null && create) {
            res = AndroidUtils.getDeviceId(currentContext);
            CONFIG.setProperty("uniqueCode", res);
            save();
        }

        return res;
    }

    public static void setUniqueCode(final String uniqueCode) {
        CONFIG.setProperty("uniqueCode", uniqueCode);
    }

    public static boolean hasUniqueCode() {
        return CONFIG.getProperty("uniqueCode") != null;
    }

    public static String getUniqueIdentifier(final boolean create) {
        final String uniqueCode = getUniqueCode(create);
        return (uniqueCode == null) ? null : "android://" + uniqueCode;
    }

    public static QueueDefinition getQueueDefinition(final String data) {
        if (data == null) {
            return null;
        } else {
            try {
                final ObjectInputStream in = new ObjectInputStream(
                        new ByteArrayInputStream(Base64.decode(data, Base64.NO_WRAP)));

                return (QueueDefinition)in.readObject();
            } catch (Throwable t) {
                Log.e("WT", t.getMessage(), t);
                return null;
            }
        }
    }

    public static QueueDefinition getQueueDefinition() {
        if (queueDefinition == null) {
            final String res = CONFIG.getProperty("queuedefinition");

            if (res != null) {
                queueDefinition = getQueueDefinition(res);
            }
        }

        return queueDefinition;
    }

    public static void setQueueDefinition(final QueueDefinition queueDefinition) {
        try {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final ObjectOutputStream out = new ObjectOutputStream(baos);

            out.writeObject(queueDefinition);
            CONFIG.put("queuedefinition", Base64.encodeToString(baos.toByteArray(), Base64.NO_WRAP));

            Config.queueDefinition = queueDefinition;
        } catch (Throwable t) {
            Log.e("WT", t.getMessage(), t);
        }
    }
}
