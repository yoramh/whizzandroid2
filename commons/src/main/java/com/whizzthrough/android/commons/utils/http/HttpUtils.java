package com.whizzthrough.android.commons.utils.http;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;

import android.util.Log;
import com.google.gson.Gson;
import com.whizzthrough.android.commons.server.exceptions.WhizzAuthenticationException;
import com.whizzthrough.android.commons.server.exceptions.WhizzAuthorizationException;
import com.whizzthrough.android.commons.server.exceptions.WhizzException;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.server.json.ErrorResponse;
import com.whizzthrough.android.commons.server.json.JsonErrorObject;
import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.SslHack;

public class HttpUtils {
	private static final String DEFAULT_AGENT = "WhizzThroughAndoid 1.0";

	static {
		SslHack.updateSsl();
	}

	private final Map<String, Object> headers = new HashMap<String, Object>();

	public static String encodeUrl(final String data) {
		try {
			return URLEncoder.encode(data, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			Log.e("WT", e.getMessage(), e);
			return data;
		}
	}

	private URL url;
	
	public HttpUtils() {
		this((URL)null);
	}
	
	public HttpUtils(final String url) throws MalformedURLException {
		this(new URL(url));
	}
	
	public HttpUtils(final URL url) {
		super();
		
		this.url = url;
	}
			
	private void initializeConnection(final HttpURLConnection conn ) {
		if (conn instanceof HttpsURLConnection && Config.isDisableSSlVerification()) {
			((HttpsURLConnection) conn).setHostnameVerifier(SslHack.NO_HOSTNAME_VERIFIER);
		}		
		
		for (final Entry<String, Object> entry: this.headers.entrySet()) {
			final String key = entry.getKey();
			final Object oValue = entry.getValue();
            final StringBuilder sb = new StringBuilder();

			if (oValue == null) {
				if (conn.getHeaderFields().containsKey(key)) {
					conn.getHeaderFields().remove(key);
				}
				
				continue;
			} else if (oValue instanceof List<?>) {
                final List<?> entryList = (List<?>)oValue;
				
				for (final Object value: entryList) {

                    if (sb.length() != 0) {
                        sb.append("; ");
                    }
					sb.append(value.toString());
				}
			} else {
				sb.append(oValue.toString());
			}

			conn.addRequestProperty(key, sb.toString());
		}

        if (!conn.getRequestProperties().containsKey("Content-Type")) {
            conn.addRequestProperty("Content-Type", getContentType());
        }

        if (!conn.getRequestProperties().containsKey("Accept")) {
            conn.addRequestProperty("Accept", getAccept());
        }

        if (!conn.getRequestProperties().containsKey("User-Agent")) {
            conn.addRequestProperty("User-Agent", getAgent());
        }
    }
	
	private HttpResponse handleConnectionReturn(final HttpURLConnection conn, final OutputStream response) throws IOException {
		final HttpResponse res = new HttpResponse(); 
		final byte[] buf = new byte[512];
		int nread;
		
		res.setResponseCode(conn.getResponseCode());
		res.setHeaders(conn.getHeaderFields());


		InputStream in;

        if (res.getResponseCode() == 200) {
            in = conn.getInputStream();
        } else {
            in = conn.getErrorStream();
        }
		
		try {
			while ((nread = in.read(buf)) != -1) {
				response.write(buf, 0, nread);
			}			
		} finally {
			in.close();			
		}
		
		return res;
	}
	
	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public Map<String, Object> getHeaders() {
		return headers;
	}

	public String getContentType() {
		if (headers.containsKey("Content-Type")) {
			return (String)headers.get("Content-Type");
		} else {
			return HttpMime.MIME_HTML.getMime();
		}
	}
	
	public void setContentType(final String mime) {
		headers.put("Content-Type", mime);
	}
	
	public void setContentType(final HttpMime mime) {
		headers.put("Content-Type", mime.getMime());
	}

	public String getAccept() {
		if (headers.containsKey("Accept")) {
			return (String)headers.get("Accept");
		} else {
			return HttpMime.MIME_ALL.getMime();
		}
	}
	
	public void setAccept(final String mime) {
		headers.put("Accept", mime);
	}
	
	public void setAccept(final HttpMime mime) {
		headers.put("Accept", mime.getMime());
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getCookies() {
		return (List<String>)headers.get("Cookie");
	}	

	public String getCookiesAsString() {
		final List<String> cookiesList = getCookies();
		
		if (cookiesList == null) {
			return null;
		} else {
			final StringBuilder sb = new StringBuilder();
			boolean first = true;
			
			for (final String cookie: cookiesList) {
				if (first) {
					first = false;
				} else {
					sb.append("; ");
				}
				
				sb.append(cookie);				
			}
			
			return sb.toString();
		}						
	}	

	public String getAgent() {
		if (headers.containsKey("User-Agent")) {
			return (String)headers.get("User-Agent");
		} else {
			return DEFAULT_AGENT;
		}
	}
	
	public void setAgent(final String agent) {
		headers.put("User-Agent", agent);
	}
		
	public HttpResponse post(final InputStream data, final OutputStream response) throws IOException {
		final HttpURLConnection conn = (HttpURLConnection) this.url.openConnection();

		initializeConnection(conn);
		
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setConnectTimeout(10000);

		final byte[] buf = new byte[512];
		int nread;
		final OutputStream out = conn.getOutputStream();
		
		try {
			while ((nread = data.read(buf)) != -1) {
				out.write(buf, 0, nread);
			}						
		} finally {
			out.close();
		}
			
		return handleConnectionReturn(conn, response);
	}

	public HttpResponse get(final OutputStream response) throws IOException {
		final HttpURLConnection conn = (HttpURLConnection) this.url.openConnection();
		
		initializeConnection(conn);
						
		return handleConnectionReturn(conn, response);
	}

	public void postJson(final Object request, final Class<?> responseClass, final JsonResponse jsonResponse) throws WhizzException {
		final Gson json = new Gson();		
		
		final String strRequest = json.toJson(request);
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();

		final String lastContentType = getContentType();
		final String lastAccept = getAccept();
		
		try {
			setContentType(HttpMime.MIME_JSON);
			setAccept(HttpMime.MIME_JSON);
			HttpResponse postResponse = post(new ByteArrayInputStream(strRequest.getBytes()), baos);

            if (jsonResponse != null) {
                jsonResponse.setResponseCode(postResponse.getResponseCode());
                jsonResponse.setHeaders(postResponse.getHeaders());

				switch (postResponse.getResponseCode()) {
					case 200:
						final BaseResponse res1 = (BaseResponse)json.fromJson(new String(baos.toByteArray()), responseClass);
						jsonResponse.setResponse(res1);
						break;

					case 400:
						final String s = new String(baos.toByteArray());
						final JsonErrorObject[] errs = json.fromJson(s, JsonErrorObject[].class);
						final ErrorResponse res2 = new ErrorResponse();
						res2.setErrors(errs);
						jsonResponse.setResponse(res2);
						break;

					case 401:
						throw new WhizzAuthenticationException();

					case 403:
						throw new WhizzAuthorizationException();

					default:
						throw new WhizzException("Error " + postResponse.getResponseCode());
				}
            }
		} catch (WhizzException e) {
			throw e;
		} catch (Throwable t) {
			throw new WhizzException(t.getMessage(), t);
		} finally {
			setContentType(lastContentType);
			setAccept(lastAccept);
		}
	}

	public void getJson(final Class<?> responseClass, final JsonResponse jsonResponse) throws IOException {
		final Gson json = new Gson();		
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		final String lastContentType = getContentType();
		final String lastAccept = getAccept();
		
		try {
			setContentType(HttpMime.MIME_JSON);
			setAccept(HttpMime.MIME_JSON);
			HttpResponse getResponse = get(baos);

			jsonResponse.setResponseCode(getResponse.getResponseCode());

			if (getResponse.getResponseCode() == 200) {
				final BaseResponse res = (BaseResponse)json.fromJson(new String(baos.toByteArray()), responseClass);
				jsonResponse.setResponse(res);
			} else {
				final JsonErrorObject[] errs = json.fromJson(new String(baos.toByteArray()), JsonErrorObject[].class);
				final ErrorResponse res = new ErrorResponse();
				res.setErrors(errs);
				jsonResponse.setResponse(res);
			}
		} finally {
			setContentType(lastContentType);
			setAccept(lastAccept);
		}
	}
}
