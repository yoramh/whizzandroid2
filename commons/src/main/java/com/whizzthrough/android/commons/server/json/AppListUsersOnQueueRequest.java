package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 12/11/15.
 */
public class AppListUsersOnQueueRequest extends BaseRequest {
    private static final long serialVersionUID = -8699254387567280331L;

    private String queueName;
    private int startAt;
    private int count;
    private boolean liveTicket = true;
    private boolean oldTicket = false;

    public int getStartAt() {
        return startAt;
    }

    public void setStartAt(int startAt) {
        this.startAt = startAt;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isLiveTicket() {
        return liveTicket;
    }

    public void setLiveTicket(boolean liveTicket) {
        this.liveTicket = liveTicket;
    }

    public boolean isOldTicket() {
        return oldTicket;
    }

    public void setOldTicket(boolean oldTicket) {
        this.oldTicket = oldTicket;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }
}
