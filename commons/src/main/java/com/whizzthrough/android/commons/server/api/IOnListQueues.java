package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.AppListQueuesResponse;

/**
 * Created by yoram on 13/10/15.
 */
public interface IOnListQueues extends IOnWhizzThroughServerEvents {
    void onResponse(AppListQueuesResponse response, Object eventsData);
}
