package com.whizzthrough.android.commons.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Patterns;

import com.whizzthrough.android.commons.activities.CustomDialog;

import shopping.safelane.android.commons.R;

public class FormUtils {
    public static boolean isEmailValid(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static CustomDialog confirmYesNo(
            final Context context,
            final String titleStr,
            final String messageStr,
            final DialogInterface.OnClickListener onPositiveClick) {
        CustomDialog.Builder customBuilder = new CustomDialog.Builder(context);
        customBuilder
                .setTitle(titleStr)
                .setMessage(messageStr)
                .setNegativeButton(R.string.wtcres_dlg_no_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.wtcres_dlg_yes_btn, onPositiveClick);
        CustomDialog dialog = customBuilder.create();
        dialog.setCancelable(false);
        dialog.show();
        return dialog;
    }

    public static CustomDialog showMessage(
            final Context context,
            final String titleStr,
            final String messageStr,
            final DialogInterface.OnClickListener onPositiveClick) {
        CustomDialog.Builder customBuilder = new CustomDialog.Builder(context);
        customBuilder
                .setTitle(titleStr)
                .setMessage(messageStr)
                .setPositiveButton(R.string.wtcres_dlg_ok_btn, onPositiveClick);
        CustomDialog dialog = customBuilder.create();
        dialog.show();
        return dialog;
    }

    public static void showErrorMessage(
            final Context context,
            final String messageStr,
            final DialogInterface.OnClickListener onPositiveClick) {
        showMessage(
                context,
                context.getResources().getString(R.string.wtcres_dlg_err_title),
                messageStr,
                onPositiveClick);
    }
}
