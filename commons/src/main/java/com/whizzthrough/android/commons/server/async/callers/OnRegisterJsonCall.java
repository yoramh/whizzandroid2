package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnRegister;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.server.json.PubRegisterAccountResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 21/10/15.
 */
public class OnRegisterJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnRegister;
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return PubRegisterAccountResponse.class;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final IOnRegister events = (IOnRegister) job.getEvents();
        events.onResponse((PubRegisterAccountResponse) response.getResponse(), job.getEventData());
    }

    @Override
    public String getAction() {
        return getContext().getResources().getString(R.string.wtcres_progress_action_registering);
    }
}
