package com.whizzthrough.android.commons.server.json;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by yoram on 13/10/15.
 */
public class BaseRequestResponse implements Serializable {
    private static final long serialVersionUID = -5753654986072330238L;

    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
