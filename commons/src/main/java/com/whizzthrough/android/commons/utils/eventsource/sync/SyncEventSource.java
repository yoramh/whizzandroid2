package com.whizzthrough.android.commons.utils.eventsource.sync;

import com.whizzthrough.android.commons.utils.SslHack;
import shopping.safelane.android.commons.utils.SystemUtils;
import com.whizzthrough.android.commons.utils.eventsource.EventSourceUtils;
import com.whizzthrough.android.commons.utils.eventsource.InputStreamResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

/**
 * Created by yoram on 11/01/16.
 */
public final class SyncEventSource {
    static {
        SslHack.updateSsl();
    }

    private final ISyncEventSourceEvents events;
    private final URL url;
    private String cookie;

    private boolean closed = false;
    private boolean running = false;

    private int connectionTimeoutMs = 5000;
    private int readTimeoutMs = 1000;

    public SyncEventSource(
            final ISyncEventSourceEvents events,
            final String url,
            final String cookie) throws MalformedURLException {
        super();

        this.events = events;

        this.url = new URL(url);
        this.cookie = cookie;
    }

    public SyncEventSource(
            final ISyncEventSourceEvents events,
            final String url) throws MalformedURLException {
        this(events, url, null);
    }

    public void run() {
        this.running = true;

        int retry = 1000;
        String id = null;
        String event = null;
        String data = null;

        while (!this.closed) {
            HttpURLConnection conn = null;
            try {
                events.onConnecting();

                conn = EventSourceUtils.getConnection(this.url, this.cookie);

                final InputStreamResult openResult = EventSourceUtils.getInputStreamResult(
                        conn,
                        3,
                        this.connectionTimeoutMs);

                if (openResult.getInputStream() == null) {
                    SystemUtils.sleep(this.connectionTimeoutMs);
                    continue;
                } else if (openResult.getResponseCode() != 200) {
                    events.onConnectionError(conn, openResult);
                    SystemUtils.sleep(this.connectionTimeoutMs);
                    continue;
                }

                events.onConnected();

                conn.setReadTimeout(this.readTimeoutMs);

                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(openResult.getInputStream()));

                    while (true) {
                         String line;

                        try {
                            line = reader.readLine();
                        } catch (SocketTimeoutException e) {
                            line = null;
                        } catch (IOException e) {
                            break;
                        }

                        if (this.closed) {
                            break;
                        }

                        if (line != null && !line.isEmpty()) {
                            final String[] parts = line.split(":");

                            if (parts.length >= 2) {
                                final String leftStr = parts[0].toLowerCase().trim();
                                final String rightStr = line.substring(parts[0].length() + 1).trim();

                                if (leftStr.equals("id")) {
                                    id = rightStr;
                                } else if (leftStr.equals("event")) {
                                    event = rightStr;
                                } else if (leftStr.equals("data")) {
                                    data = data == null ? rightStr : data + "\n" + rightStr;
                                } else if (leftStr.equals("retry")) {
                                    try {
                                        retry = Integer.valueOf(rightStr);
                                    } catch (NumberFormatException e) {

                                    }
                                }
                            }
                        } else if (line.isEmpty()) {
                            events.onMessage(id, event, data);
                            id = null;
                            event = null;
                            data = null;
                        }

                        if (this.closed) {
                            break;
                        }
                    }
                } finally {
                    openResult.getInputStream().close();
                }
            } catch (Throwable t) {
                events.onError(conn, t);
            }

            events.onDisconnected();

            if (this.closed) {
                break;
            }

            SystemUtils.sleep(retry);
        }

        events.onClose(WhyWasIClosed.CR_NORMAL);
        this.running = false;
    }

    public void close() {
        this.closed = true;
    }

    public String getCookie() {
        return this.cookie;
    }

    public void setCookie(final String cookie) {
        this.cookie = cookie;
    }

    public int getConnectionTimeoutMs() {
        return this.connectionTimeoutMs;
    }

    public void setConnectionTimeoutMs(final int connectionTimeoutMs) {
        this.connectionTimeoutMs = connectionTimeoutMs;
    }

    public boolean isRunning() {
        return this.running;
    }

    public int getReadTimeoutMs() {
        return this.readTimeoutMs;
    }

    public void setReadTimeoutMs(final int readTimeoutMs) {
        this.readTimeoutMs = readTimeoutMs;
    }
}
