package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnIncreaseCurrentTicket;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.AppIncreaseCurrentTicketResponse;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

/**
 * Created by yoram on 20/10/15.
 */
public class IncreaseCurrentTicketJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnIncreaseCurrentTicket;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final AppIncreaseCurrentTicketResponse res = (AppIncreaseCurrentTicketResponse) response.getResponse();
        final IOnIncreaseCurrentTicket events = (IOnIncreaseCurrentTicket) job.getEvents();
        events.onResponse(res, job.getEventData());
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return AppIncreaseCurrentTicketResponse.class;
    }

    @Override
    public String getAction() {
        return "Increasing Ticket";
    }
}
