package com.whizzthrough.android.commons.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.whizzthrough.android.commons.utils.AndroidUtils;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 18/02/16.
 */
public class RegisterOrSigninFragment extends AbstractFragment {
    private static final long serialVersionUID = -2481179878725818137L;

    private int logoDrawableId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.frag_register_or_sign_in, container, false);

        TextView txt = (TextView)rootView.findViewById(R.id.wtc_frag_register_or_sign_in_logoText);
        final Drawable logo;
        if (savedInstanceState == null) {
            logo = getResources().getDrawable(this.logoDrawableId);
        } else {
            logo = getResources().getDrawable(savedInstanceState.getInt("logoDrawableId"));
        }
        txt.setCompoundDrawablesWithIntrinsicBounds(null, logo, null, null);

        txt = (TextView)rootView.findViewById(R.id.wtc_frag_register_or_sign_in_next_button);
        AndroidUtils.undelineTextView(rootView.getContext(), txt, txt.getText().toString());
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioGroup group = (RadioGroup)rootView.findViewById(R.id.wtc_frag_register_or_sign_in_choices);
                int i = group.getCheckedRadioButtonId();

                if (i == R.id.wtc_frag_register_or_sign_in_regbutton) {
                    getController().onNext(0, getControllerData());
                } else {
                    getController().onNext(1, getControllerData());
                }
            }
        });

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("logoDrawableId", this.getLogoDrawableId());
    }

    public int getLogoDrawableId() {
        return this.logoDrawableId;
    }

    public void setLogoDrawableId(final int logoDrawableId) {
        this.logoDrawableId = logoDrawableId;
    }
}
