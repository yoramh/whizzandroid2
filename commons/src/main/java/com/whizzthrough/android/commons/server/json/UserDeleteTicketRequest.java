package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 28/10/15.
 */
public class UserDeleteTicketRequest extends BaseRequest {
    private static final long serialVersionUID = 3403806473943948270L;

    private String id;
    private String code;

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(final String code) {
        this.code = code;
    }
}
