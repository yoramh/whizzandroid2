package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 17/10/15.
 */
public class AppTdeCurrentTicketResponse extends BaseResponse {
    private static final long serialVersionUID = -9192633671046763233L;

    private int currentTicket;

    public int getCurrentTicket() {
        return currentTicket;
    }

    public void setCurrentTicket(int currentTicket) {
        this.currentTicket = currentTicket;
    }
}
