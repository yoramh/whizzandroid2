package com.whizzthrough.android.commons.server.exceptions;

/**
 * Created by yoram on 13/01/16.
 */
public class WhizzAuthenticationException extends WhizzException {
    private static final long serialVersionUID = 8834300304744894909L;

    public WhizzAuthenticationException() {
        super();
    }
}
