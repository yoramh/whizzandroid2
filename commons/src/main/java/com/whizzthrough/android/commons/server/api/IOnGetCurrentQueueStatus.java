package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.UserGetCurrentQueueStatusResponse;

/**
 * Created by yoram on 21/10/15.
 */
public interface IOnGetCurrentQueueStatus extends IOnWhizzThroughServerEvents {
    void onResponse(UserGetCurrentQueueStatusResponse response, Object eventsData);
}
