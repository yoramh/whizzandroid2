package com.whizzthrough.android.commons.server.json;

public final class PubRegisterAccountResponse extends BaseResponse {
	private ERegistrationChallenge challenge;
	private String uniqueId;
	private long retryTimeOut;
	private String userId;

	public ERegistrationChallenge getChallenge() {
		return challenge;
	}

	public PubRegisterAccountResponse challenge(final ERegistrationChallenge challenge) {
		this.challenge = challenge;

		return this;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public PubRegisterAccountResponse uniqueId(final String uniqueId) {
		this.uniqueId = uniqueId;

		return this;
	}

	public long getRetryTimeOut() {
		return retryTimeOut;
	}

	public PubRegisterAccountResponse retryTimeOut(final long retryTimeOut) {
		this.retryTimeOut = retryTimeOut;

		return this;
	}

	public String getUserId() {
		return userId;
	}

	public PubRegisterAccountResponse userId(final String userId) {
		this.userId = userId;

		return this;
	}
}
