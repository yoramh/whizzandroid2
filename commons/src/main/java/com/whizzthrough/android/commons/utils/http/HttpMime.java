package com.whizzthrough.android.commons.utils.http;

public enum HttpMime {
	MIME_ALL ("*/*"),
	MIME_TEXT ("text/plain"),
	MIME_HTML ("text/html"),
	MIME_JSON ("application/json");
	
	private final String mime;
	
	HttpMime(final String mime) {
		this.mime = mime;
	}

	public String getMime() {
		return mime;
	}	
}
