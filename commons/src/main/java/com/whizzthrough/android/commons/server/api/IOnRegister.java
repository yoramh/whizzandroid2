package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.PubRegisterAccountResponse;

/**
 * Created by yoram on 18/10/15.
 */
public interface IOnRegister extends IOnWhizzThroughServerEvents {
    void onResponse(PubRegisterAccountResponse response, Object eventsData);
}
