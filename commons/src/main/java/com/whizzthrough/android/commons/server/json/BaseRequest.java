package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 13/10/15.
 */
public class BaseRequest extends BaseRequestResponse {
    private static final long serialVersionUID = -7043714602833741968L;

    private String locale;

    public String getLocale() {
        return locale;
    }

    public BaseRequest locale(final String locale) {
        this.locale = locale;

        return this;
    }

}
