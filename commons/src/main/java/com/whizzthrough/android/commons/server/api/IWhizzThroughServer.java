package com.whizzthrough.android.commons.server.api;

import android.content.Context;

import com.whizzthrough.android.commons.receivers.ErrorBroadcastReceiver;
import com.whizzthrough.android.commons.server.exceptions.WhizzException;
import com.whizzthrough.android.commons.server.json.*;

public interface IWhizzThroughServer {
	void login(final Context context, String email, String password, IOnLogin events, ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent, Object eventsData);
	void register(final Context context, PubRegisterAccountRequest request, IOnRegister events, ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent, Object eventsData);
	void validateRegistration(final Context context, PubValidateRegistrationRequest request, IOnValidateRegistration events, ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent, Object eventsData);
	void resendRegistrationSms(final Context context, PubResendRegistrationSmsRequest request, IOnResendRegistrationSms events, ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent, Object eventsData);

	void listQueues(final Context context, String cookie, int startAt, int count, IOnListQueues events, Object eventsData);

	void grabTicket(final Context context, UserGrabTicketRequest request, IOnGrabTicket events, Object eventsData);

	void deleteTicket(final Context context, String id, String code, String url, IOnDeleteTicket events, Object eventsData);

	void getUserState(final Context context, String cookie, String url, IOnGetUserState events, Object eventsData);

	void collectAccountInformation2(
			Context context,
			String url,
			PubCollectAccountInformationRequest request,
			IOnCollectAccountInformation events,
			Object eventsData);

	void increaseCurrentTicket(
			Context context,
			String cookie,
			String url,
			AppIncreaseCurrentTicketRequest2 request,
			IOnIncreaseCurrentTicket events,
			Object eventsData);

	void setTicketDescription(
			Context context,
			AppSetTicketDescriptionRequest request,
			IOnSetTicketDescription events,
			Object eventsData);

	void listUsersOnQueue(
			Context context,
			AppListUsersOnQueueRequest request,
			IOnListUsersOnQueue events,
			Object eventsData) throws WhizzException;

	void sendMessage(
			Context context,
			AppSendMessageRequest request,
			IOnSendMessage events,
			Object eventsData);

	void getQueueInfoForTerminal(
			Context context,
			AppGetQueueInfoForTerminalRequest request,
			IOnGetQueueInfoForTerminal events,
			Object eventsData);
}
    
