package com.whizzthrough.android.commons.server.api;

import android.content.Context;

public abstract class OnResendRegistrationSms extends AbstractWhizzThroughEvents implements IOnResendRegistrationSms {
    public OnResendRegistrationSms(Context context) {
        super(context);
    }
}
