package com.whizzthrough.android.commons.utils;

import android.util.Log;

import java.io.*;

/**
 * Created by yoram on 24/10/15.
 */
public class IOUtils {
    public static void readStream(final InputStream in, final OutputStream out) throws IOException {
        byte[] buf = new byte[1024];
        int nread;

        while ((nread = in.read(buf)) > 0) {
            out.write(buf, 0, nread);
        }
    }

    public static byte[] readStreamAsByteArray(final InputStream in) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        readStream(in, baos);
        return baos.toByteArray();
    }

    public static String readStreamAsString(final InputStream in) throws IOException {
        return new String(readStreamAsByteArray(in));
    }

    public static void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                Log.e("WT", "Exception occurs whilst closing stream but app can continue fine: " + e.getMessage(), e);
            }
        }
    }
}
