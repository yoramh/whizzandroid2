package com.whizzthrough.android.commons.server.exceptions;

/**
 * Created by yoram on 13/01/16.
 */
public class WhizzException extends Exception {
    private static final long serialVersionUID = -8921880379355733527L;

    public WhizzException() {
        super();
    }

    public WhizzException(String detailMessage) {
        super(detailMessage);
    }

    public WhizzException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public WhizzException(Throwable throwable) {
        super(throwable);
    }
}
