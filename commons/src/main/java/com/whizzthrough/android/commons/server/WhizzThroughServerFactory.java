package com.whizzthrough.android.commons.server;


import com.whizzthrough.android.commons.server.api.IWhizzThroughServer;

public class WhizzThroughServerFactory {
    private static IWhizzThroughServer defaultServer = null;
    
    public static IWhizzThroughServer getDefaultServer() {
        if (defaultServer == null) {
            defaultServer = new DefaultWhizzThroughServer();
        }
        
        return defaultServer;
    }

    public static void setDefaultServer(IWhizzThroughServer defaultServer) {
        WhizzThroughServerFactory.defaultServer = defaultServer;
    }
}
