package com.whizzthrough.android.commons.server.api;

public interface IOnResendRegistrationSms extends IOnWhizzThroughServerEvents {
    void onResponse(Object eventsData);
}
