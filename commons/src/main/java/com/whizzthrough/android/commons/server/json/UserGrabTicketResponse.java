package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 18/10/15.
 */
public class UserGrabTicketResponse extends BaseResponse {
    private static final long serialVersionUID = 3327730812955807471L;

    private String company;
    private String logoBase64;
    private String description;
    private String queueName;

    private int ticketNumber;
    private int currentTicket;

    private long calledOn;

    private long expectedTimeRemaining;

    private long minimumTimeRemaining;

    private long soonAlarmTime;

    private String code;

    private String qrCodeUserToken;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLogoBase64() {
        return logoBase64;
    }

    public void setLogoBase64(String logoBase64) {
        this.logoBase64 = logoBase64;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public int getCurrentTicket() {
        return currentTicket;
    }

    public void setCurrentTicket(int currentTicket) {
        this.currentTicket = currentTicket;
    }

    public long getCalledOn() {
        return calledOn;
    }

    public void setCalledOn(long calledOn) {
        this.calledOn = calledOn;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public long getExpectedTimeRemaining() {
        return expectedTimeRemaining;
    }

    public void setExpectedTimeRemaining(long expectedTimeRemaining) {
        this.expectedTimeRemaining = expectedTimeRemaining;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public long getMinimumTimeRemaining() {
        return this.minimumTimeRemaining;
    }

    public void setMinimumTimeRemaining(final long minimumTimeRemaining) {
        this.minimumTimeRemaining = minimumTimeRemaining;
    }

    public long getSoonAlarmTime() {
        return this.soonAlarmTime;
    }

    public void setSoonAlarmTime(final long soonAlarmTime) {
        this.soonAlarmTime = soonAlarmTime;
    }

    public String getQrCodeUserToken() {
        return this.qrCodeUserToken;
    }

    public void setQrCodeUserToken(final String qrCodeUserToken) {
        this.qrCodeUserToken = qrCodeUserToken;
    }
}
