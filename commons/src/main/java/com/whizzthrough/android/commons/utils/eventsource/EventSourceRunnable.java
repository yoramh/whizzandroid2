package com.whizzthrough.android.commons.utils.eventsource;

import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.SslHack;
import shopping.safelane.android.commons.utils.SystemUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by yoram on 24/10/15.
 */
public class EventSourceRunnable implements Runnable {
    static {
        SslHack.updateSsl();
    }

    private static InputStreamResult getInputStream(final HttpURLConnection conn, int retry) {
        final InputStreamResult res = new InputStreamResult();

        conn.setConnectTimeout(60000);

        for (int i = 0; i < retry; i++) {
            try {
                res.setResponseCode(conn.getResponseCode());

                if (res.getResponseCode() == 200) {
                    res.setInputStream(conn.getInputStream());
                } else {
                    res.setInputStream(conn.getErrorStream());
                }

                return res;
            } catch (SocketTimeoutException e) {
                res.setError(e);
            } catch (IOException e) {
                res.setError(e);
            }
        }

        return res;
    }

    private final IOnEventSource eventSource;
    private final URL url;
    private String cookie;
    private boolean closed = false;

    public EventSourceRunnable(
            final IOnEventSource eventSource,
            final String url,
            final String cookie) throws MalformedURLException {
        super();

        this.eventSource = eventSource;

        this.url = new URL(url);
        this.cookie = cookie;
    }

    public EventSourceRunnable(
            final IOnEventSource eventSource,
            final String url) throws MalformedURLException {
        this(eventSource, url, null);
    }

    private HttpURLConnection getConnection() throws IOException {
        final HttpURLConnection res = (HttpURLConnection) this.url.openConnection();

        if (res instanceof HttpsURLConnection && Config.isDisableSSlVerification()) {
            ((HttpsURLConnection) res).setHostnameVerifier(SslHack.NO_HOSTNAME_VERIFIER);
        }

        if (this.cookie != null) {
            res.addRequestProperty("Cookie", cookie);
        }

        return res;
    }

    @Override
    public void run() {
        int retry = 1000;
        String id = null;
        String event = null;
        String data = null;

        while (true) {
            HttpURLConnection conn = null;
            try {
                conn = getConnection();

                eventSource.onConnecting();

                conn.setConnectTimeout(5000);
                final InputStreamResult openResult = getInputStream(conn, 3);
                if (openResult.getInputStream() == null) {
                    if (this.closed) {
                        return;
                    }

                    SystemUtils.sleep(retry);

                    continue;
                } else if (openResult.getResponseCode() != 200) {
                    eventSource.onConnectionError(conn, openResult);

                    SystemUtils.sleep(retry);
                    continue;
                }

                eventSource.onConnected();

                conn.setReadTimeout(1000);

                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(openResult.getInputStream()));

                    while (true) {
                        String line;

                        try {
                            line = reader.readLine();
                        } catch (SocketTimeoutException e) {
                            if (this.closed) {
                                break;
                            } else {
                                continue;
                            }
                        } catch (IOException e) {
                            break;
                        }

                        if (line == null || line.isEmpty()) {
                            eventSource.onMessage(id, event, data);

                            id = null;
                            data = null;
                        } else {
                            final String[] parts = line.split(":");

                            if (parts.length >= 2) {
                                final String leftStr = parts[0].toLowerCase().trim();
                                final String rightStr = line.substring(parts[0].length() + 1).trim();

                                if (leftStr.equals("id")) {
                                    id = rightStr;
                                } else if (leftStr.equals("event")) {
                                    event = rightStr;
                                } else if (leftStr.equals("data")) {
                                    data = rightStr;
                                } else if (leftStr.equals("retry")) {
                                    try {
                                        retry = Integer.valueOf(rightStr);
                                    } catch (NumberFormatException e) {

                                    }
                                }
                            }
                        }

                        if (this.closed) {
                            break;
                        }
                    }
                } finally {
                    openResult.getInputStream().close();
                }
            } catch (Throwable t) {
                eventSource.onError(conn, t);
            }

            eventSource.onDisconnected();

            if (this.closed) {
                return;
            }
            SystemUtils.sleep(retry);
        }
    }

    public void close() {
        this.closed = true;
    }

    public void setCookie(final String cookie) {
        this.cookie = cookie;
    }
}
