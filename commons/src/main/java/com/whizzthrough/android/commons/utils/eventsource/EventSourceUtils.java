package com.whizzthrough.android.commons.utils.eventsource;

import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.SslHack;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

/**
 * Created by yoram on 11/01/16.
 */
public final class EventSourceUtils {
    private EventSourceUtils() {

    }

    public static InputStreamResult getInputStreamResult(
            final HttpURLConnection conn,
            final int retry,
            final int timeout) {
        final InputStreamResult res = new InputStreamResult();

        conn.setConnectTimeout(timeout);

        for (int i = 0; i < retry; i++) {
            try {
                res.setResponseCode(conn.getResponseCode());

                if (res.getResponseCode() == 200) {
                    res.setInputStream(conn.getInputStream());
                } else {
                    res.setInputStream(conn.getErrorStream());
                }

                res.setError(null);
                return res;
            } catch (SocketTimeoutException e) {
                res.setError(e);
            } catch (IOException e) {
                res.setError(e);
            }
        }

        return res;
    }

    public static HttpURLConnection getConnection(final URL url, final String cookie) throws IOException {
        final HttpURLConnection res = (HttpURLConnection) url.openConnection();

        if (res instanceof HttpsURLConnection && Config.isDisableSSlVerification()) {
            ((HttpsURLConnection) res).setHostnameVerifier(SslHack.NO_HOSTNAME_VERIFIER);
        }

        if (cookie != null) {
            res.addRequestProperty("Cookie", cookie);
        }

        return res;
    }

}
