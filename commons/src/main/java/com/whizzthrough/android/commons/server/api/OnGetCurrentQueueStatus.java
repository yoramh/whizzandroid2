package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 21/10/15.
 */
public abstract class OnGetCurrentQueueStatus extends AbstractWhizzThroughEvents implements IOnGetCurrentQueueStatus {
    public OnGetCurrentQueueStatus(final Context context) {
        super(context);
    }
}
