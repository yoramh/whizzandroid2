package com.whizzthrough.android.commons.components;

import android.app.Activity;
import android.content.*;
import android.util.AttributeSet;
import android.util.Log;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.whizzthrough.android.commons.server.json.AppAddUserOnQueueResponse;
import com.whizzthrough.android.commons.server.json.UpdatedQueueInfoResponse;

import java.net.URLEncoder;

import shopping.safelane.android.commons.components.tickets.TicketSummaryItemAdapter;
import shopping.safelane.android.commons.components.tickets.UserView;
import shopping.safelane.android.commons.services.server.DefaultSafelaneServerBinder;
import shopping.safelane.android.commons.services.server.IntentMessageType;

/**
 * Created by yoram on 25/02/16.
 */
public class QueueCurrentTicketComponent extends AppCompatTextView {
    private static final String TAG = QueueCurrentTicketComponent.class.getSimpleName();

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (IntentMessageType.FILTER_EVENT.equals(intent.getAction()) && intent.hasExtra(IntentMessageType.MSG_DATA)) {
                try {
                    DefaultSafelaneServerBinder.AsyncBroadcast o = (DefaultSafelaneServerBinder.AsyncBroadcast)intent.getSerializableExtra(IntentMessageType.MSG_DATA);

                    if (o != null) {
                        UpdatedQueueInfoResponse response = new Gson().fromJson(o.getData(), UpdatedQueueInfoResponse.class);
                        setText("" + response.getCurrentTicket());
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }
    };

    public QueueCurrentTicketComponent(Context context) {
        super(context);
        setText("0");
    }

    public QueueCurrentTicketComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        setText("0");
    }

    public QueueCurrentTicketComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setText("0");
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(IntentMessageType.FILTER_EVENT);
        getActivity().registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        try {
            getActivity().unregisterReceiver(receiver);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private Activity getActivity() {
        Context context = getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }
}
