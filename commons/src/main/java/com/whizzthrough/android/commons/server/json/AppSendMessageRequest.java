package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 13/03/16.
 */
public class AppSendMessageRequest extends BaseRequest {
    private static final long serialVersionUID = 324797226356504779L;

    private String code;
    private String message;

    public String getMessage() {
        return this.message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(final String code) {
        this.code = code;
    }
}
