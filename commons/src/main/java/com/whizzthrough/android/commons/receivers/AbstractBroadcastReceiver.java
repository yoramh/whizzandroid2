package com.whizzthrough.android.commons.receivers;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;

/**
 * Created by yoram on 25/10/15.
 */
public abstract class AbstractBroadcastReceiver extends BroadcastReceiver {
    public abstract IntentFilter getIntentFilter();
}
