package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 25/10/15.
 */
public abstract class OnIncreaseCurrentTicket extends AbstractWhizzThroughEvents implements IOnIncreaseCurrentTicket {
    public OnIncreaseCurrentTicket(final Context context) {
        super(context);
    }
}