package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.UserGrabTicketResponse;

/**
 * Created by yoram on 20/10/15.
 */
public interface IOnGrabTicket extends IOnWhizzThroughServerEvents {
    void onResponse(UserGrabTicketResponse response, Object eventsData);
}
