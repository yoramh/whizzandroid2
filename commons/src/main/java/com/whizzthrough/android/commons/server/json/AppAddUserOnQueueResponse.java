package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 11/11/15.
 */
public class AppAddUserOnQueueResponse extends BaseResponse {
    private static final long serialVersionUID = -8935315020977547792L;

    private String code;
    private long takenOn;
    private long calledOn;
    private String email;
    private int ticket;
    private String description;

    public String getCode() {
        return this.code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public long getTakenOn() {
        return this.takenOn;
    }

    public void setTakenOn(long takenOn) {
        this.takenOn = takenOn;
    }

    public long getCalledOn() {
        return this.calledOn;
    }

    public void setCalledOn(long calledOn) {
        this.calledOn = calledOn;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTicket() {
        return this.ticket;
    }

    public void setTicket(int ticket) {
        this.ticket = ticket;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
