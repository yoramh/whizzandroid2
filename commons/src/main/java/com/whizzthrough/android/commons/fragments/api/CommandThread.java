package com.whizzthrough.android.commons.fragments.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import shopping.safelane.android.commons.utils.SystemUtils;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by yoram on 18/02/16.
 */
public class CommandThread extends Thread implements ICommand {
    public interface Events {
        void onResult(CommandThread t);
    }

    private final ICommand command;

    public static CommandThread runSynchronous(
            @NonNull final ICommand command, final @Nullable Context context, @Nullable final Events after) {
        final AtomicReference<CommandThread> res = new AtomicReference<>(new CommandThread(command));

        final Runnable action = () -> {
            try {
                res.get().start();
            } catch (Throwable t) {
                res.get().setSuccess(false);

                t.printStackTrace();

                return;
            }

            final long now = System.currentTimeMillis();
            while (!res.get().finished()) {
                SystemUtils.sleep(500);

                if (System.currentTimeMillis() - now > TimeUnit.SECONDS.toMillis(30)) {
                    res.get().setSuccess(false);

                    break;
                }
            }
        };

        if (context != null) {
            final ProgressDialog dialog = ProgressDialog.show(context, "Loading", "Please wait...", true);

            new Thread(() -> {
                try {
                    action.run();

                    if (after != null) {
                        new Handler(Looper.getMainLooper()).post(() -> after.onResult(res.get()));
                    }
                } catch (Throwable t) {
                    res.get().setSuccess(false);

                    if (after != null) {
                        new Handler(Looper.getMainLooper()).post(() -> after.onResult(res.get()));
                    }
                } finally {
                    dialog.dismiss();
                }
            }).start();
        } else {
            action.run();
        }

        return res.get();
    }

    @Deprecated
    public static CommandThread runSynchronous(final ICommand command) {
        return runSynchronous(command, null, null);
    }

    public CommandThread(final ICommand command) {
        super();

        this.command = command;
    }

    @Override
    public void run() {
        begin();
        execute();
    }

    @Override
    public void begin() {
        this.command.begin();
    }

    @Override
    public void execute() {
        this.command.execute();
    }

    @Override
    public boolean success() {
        return this.command.success();
    }

    @Override
    public void setSuccess(boolean value) {
        this.command.setSuccess(value);
    }

    @Override
    public Object response() {
        return this.command.response();
    }

    @Override
    public Throwable exception() {
        return this.command.exception();
    }

    @Override
    public boolean finished() {
        return this.command.finished();
    }

    @Override
    public String name() {
        return this.command.name();
    }

    @Override
    public int label() {
        return this.command.label();
    }
}
