package com.whizzthrough.android.commons.utils;

/**
 * Created by yoram on 25/10/15.
 */
public class TimeUtils {
    public static final long SECOND = 1000;
    public static final long MINUTE = SECOND * 60;
    public static final long HOUR = MINUTE * 60;
    public static final long DAY = HOUR * 24;
}
