package com.whizzthrough.android.commons.utils;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.TextView;

import java.util.UUID;

/**
 * Created by yoram on 12/01/16.
 */
public final class AndroidUtils {
    private AndroidUtils() {

    }

    public static boolean isNetworkConnected(final Context c) {
        final ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);

        try {
            final NetworkInfo ni = cm.getActiveNetworkInfo();
            return ni != null && ni.isConnectedOrConnecting();
        } catch (Throwable t) {
            t.printStackTrace();
            return false;
        }
    }

    public static boolean isBluetoothSupported() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        return mBluetoothAdapter != null;
    }

    public static String getDeviceId(Context context) {
        try {
            String res = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

            if (res == null) {
                res = UUID.randomUUID().toString();
            }

            return res;
        } catch (Throwable t) {
            return UUID.randomUUID().toString();
        }

    }

    public static void undelineTextView(final Context context, final TextView tv, final int stringId) {
        undelineTextView(context, tv, context.getResources().getString(stringId));
    }

    public static void undelineTextView(final Context context, final TextView tv, final String message) {
        final SpannableString content = new SpannableString(message);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tv.setText(content);
    }
}
