package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 26/05/16.
 */
public class AppGetQueueInfoForTerminalRequest extends BaseRequest {
    private static final long serialVersionUID = 2203284453948536561L;

    private long queueId;
    private String qrCodeUserToken;
    private int qrCodeWidth;
    private int qrCodeHeight;

    public AppGetQueueInfoForTerminalRequest() {
        this(-1, null, 200, 200);
    }

    public AppGetQueueInfoForTerminalRequest(
            final long queueId,
            final String qrCodeUserToken,
            final int qrCodeWidth,
            final int qrCodeHeight) {
        super();

        this.queueId = queueId;
        this.qrCodeUserToken = qrCodeUserToken;
        this.qrCodeWidth = qrCodeWidth;
        this.qrCodeHeight = qrCodeHeight;
    }

    public long getQueueId() {
        return queueId;
    }

    public void setQueueId(long queueId) {
        this.queueId = queueId;
    }

    public String getQrCodeUserToken() {
        return qrCodeUserToken;
    }

    public void setQrCodeUserToken(String qrCodeUserToken) {
        this.qrCodeUserToken = qrCodeUserToken;
    }

    public int getQrCodeWidth() {
        return qrCodeWidth;
    }

    public void setQrCodeWidth(int qrCodeWidth) {
        this.qrCodeWidth = qrCodeWidth;
    }

    public int getQrCodeHeight() {
        return qrCodeHeight;
    }

    public void setQrCodeHeight(int qrCodeHeight) {
        this.qrCodeHeight = qrCodeHeight;
    }
}
