package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 20/10/15.
 */
public abstract class OnGrabTicket extends AbstractWhizzThroughEvents implements IOnGrabTicket {
    public OnGrabTicket(final Context context) {
        super(context);
    }
}
