package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 28/11/15.
 */
public abstract class OnGetUserState extends AbstractWhizzThroughEvents implements IOnGetUserState {
    public OnGetUserState(final Context context) {
        super(context);
    }
}
