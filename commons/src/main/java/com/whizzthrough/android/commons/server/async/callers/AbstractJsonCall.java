package com.whizzthrough.android.commons.server.async.callers;

import android.content.Context;

/**
 * Created by yoram on 25/10/15.
 */
public abstract class AbstractJsonCall implements IJsonCall {
    private Context context;

    @Override
    public Context getContext() {
        return this.context;
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }
}
