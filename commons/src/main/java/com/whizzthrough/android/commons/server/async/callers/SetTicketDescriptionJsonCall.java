package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnSetTicketDescription;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.AppAddUserOnQueueResponse;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

/**
 * Created by yoram on 29/02/16.
 */
public class SetTicketDescriptionJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnSetTicketDescription;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final AppAddUserOnQueueResponse res = (AppAddUserOnQueueResponse) response.getResponse();
        final IOnSetTicketDescription events = (IOnSetTicketDescription) job.getEvents();
        events.onResponse(res, job.getEventData());
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return AppAddUserOnQueueResponse.class;
    }

    @Override
    public String getAction() {
        return null;
    }
}