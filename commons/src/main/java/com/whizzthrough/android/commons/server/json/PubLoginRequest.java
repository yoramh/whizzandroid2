package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 12/10/15.
 */
public class PubLoginRequest extends BaseRequest {
    private static final long serialVersionUID = 2118534789868629782L;

    private String email;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
