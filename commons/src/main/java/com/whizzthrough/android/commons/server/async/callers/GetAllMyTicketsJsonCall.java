package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnGetAllMyTickets;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.server.json.UserGetAllMyTicketsResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

/**
 * Created by yoram on 20/10/15.
 */
public class GetAllMyTicketsJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnGetAllMyTickets;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final UserGetAllMyTicketsResponse res = (UserGetAllMyTicketsResponse) response.getResponse();
        final IOnGetAllMyTickets events = (IOnGetAllMyTickets) job.getEvents();
        events.onResponse(res, job.getEventData());
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return UserGetAllMyTicketsResponse.class;
    }

    @Override
    public String getAction() {
        return null;
    }
}
