package com.whizzthrough.android.commons.utils.eventsource;

import java.net.HttpURLConnection;

/**
 * Created by yoram on 17/10/15.
 */
public interface IOnEventSource {
    void onMessage(String id, String event, String data);

    void onError(HttpURLConnection conn, Throwable t);

    void onConnectionError(HttpURLConnection conn, InputStreamResult res);

    void onDisconnected();

    void onClose(CloseReason reason);

    void onConnecting();

    void onConnected();
}
