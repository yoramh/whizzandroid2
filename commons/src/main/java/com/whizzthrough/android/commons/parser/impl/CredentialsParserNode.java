package com.whizzthrough.android.commons.parser.impl;

import android.content.Context;
import com.whizzthrough.android.commons.fragments.AbstractFragment;
import com.whizzthrough.android.commons.fragments.CredentialsFragment;
import com.whizzthrough.android.commons.parser.api.IFragmentParser;

import java.util.Map;

/**
 * Created by yoram on 23/02/16.
 */
public class CredentialsParserNode extends AbstractParserNode implements IFragmentParser {
    private static final long serialVersionUID = 3559750166772185513L;

    @Override
    public AbstractFragment getFragment(Context context, Map<String, Object> properties) {
        final CredentialsFragment res = new CredentialsFragment();

        final NodeParser.ResourceProperty logoIdProp = (NodeParser.ResourceProperty)properties.get("logoid");
        final int logoId = context.getResources().getIdentifier(
                logoIdProp.getResourceName(),
                logoIdProp.getDefinition(),
                logoIdProp.getPackage_());
        res.setLogoDrawableId(logoId);

        res.setRegisterOrSignin((String)properties.get("registerOrSignin"));
        return res;
    }
}
