package com.whizzthrough.android.commons.parser.impl;

import android.content.Context;
import android.util.Log;
import com.whizzthrough.android.commons.fragments.AbstractFragment;
import com.whizzthrough.android.commons.fragments.ErrorFragment;
import com.whizzthrough.android.commons.parser.api.IFragmentParser;

import java.util.Map;

/**
 * Created by yoram on 22/02/16.
 */
public class ErrorParserNode extends AbstractParserNode implements IFragmentParser {
    @Override
    public AbstractFragment getFragment(Context context, Map<String, Object> properties) {
        final ErrorFragment res = new ErrorFragment();

        NodeParser.ResourceProperty prop = (NodeParser.ResourceProperty)properties.get("errorMessageId");
        int id = prop.findResource(context);

        res.setErrorMessage(context.getString(id));

        prop = (NodeParser.ResourceProperty)properties.get("clickHereId");
        id = prop.findResource(context);

        res.setClickHereMessage(context.getString(id));

        res.setLink((String)properties.get("link"));

        return res;
    }
}
