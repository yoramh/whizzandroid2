package com.whizzthrough.android.commons.receivers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import com.google.android.material.snackbar.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.TimeUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import shopping.safelane.android.commons.R;
import shopping.safelane.android.commons.utils.SystemUtils;

/**
 * Created by yoram on 25/10/15.
 */
public class ErrorBroadcastReceiver extends AbstractBroadcastReceiver {
    public static final String FILTER_ERROR_MESSAGE = ErrorBroadcastReceiver.class.getName() + ".FILTER.ERROR_MESSAGE";

    public static final String BUNDLE_ERROR_MESSAGE = ErrorBroadcastReceiver.class.getName() + ".BUNDLE.ERROR_MESSAGE";
    public static final String BUNDLE_ERROR_MESSAGE_IDS = ErrorBroadcastReceiver.class.getName() + ".BUNDLE.ERROR_MESSAGE_IDS";
    public static final String BUNDLE_CLOSE = ErrorBroadcastReceiver.class.getName() + ".BUNDLE.CLOSE";
    public static final String BUNDLE_EVENT = ErrorBroadcastReceiver.class.getName() + ".BUNDLE.EVENT";

    public interface OnAfterErrorEvent extends Serializable {
        void afterError();
        void setContext(@Nullable Context context);
    }

    private long lastMessageTime = 0;

    public ErrorBroadcastReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null &&
                lastMessageTime < (System.currentTimeMillis() - TimeUtils.SECOND)) {
            lastMessageTime = System.currentTimeMillis();

            final boolean close = intent.getBooleanExtra(BUNDLE_CLOSE, false);
            final OnAfterErrorEvent event = (OnAfterErrorEvent)intent.getSerializableExtra(BUNDLE_EVENT);
            final List<Integer> messageIds = intent.hasExtra(BUNDLE_ERROR_MESSAGE_IDS) ?
                    intent.getIntegerArrayListExtra(BUNDLE_ERROR_MESSAGE_IDS) :
                    null;

            final String text;

            if (messageIds != null) {
                String s = "";

                final String packageName = context.getPackageName();
                for (final Integer id: messageIds) {
                    final int resId = context.getResources().getIdentifier(String.format("server_message_" + id), "string", packageName);
                    if (resId == 0) {
                        s = intent.getStringExtra(BUNDLE_ERROR_MESSAGE);

                        break;
                    }

                    s += context.getString(resId) + "\n";
                }

                text = s;
            } else {
                text = intent.getStringExtra(BUNDLE_ERROR_MESSAGE);
            }

            if (text != null && !text.isEmpty() && context != null) {
                new AlertDialog.Builder(Config.getCurrentContext() == null ? context : Config.getCurrentContext(), R.style.SafelaneDarkThemeAlertDialog)
                        .setTitle("Error")
                        .setMessage(text)
                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(
                                android.R.string.ok,
                                (dialog, which) -> {
                                    if (close) {
                                        SystemUtils.quit(context instanceof Activity ? (Activity)context : (Activity)Config.getCurrentContext());
                                        return;
                                    }

                                    if (event != null) {
                                        event.setContext(context);
                                        event.afterError();
                                    }
                                })
                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }
    }

    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter res = new IntentFilter();
        res.addAction(FILTER_ERROR_MESSAGE);
        return res;
    }
}
