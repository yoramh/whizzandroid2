package com.whizzthrough.android.commons.fragments;

import androidx.fragment.app.Fragment;

import com.whizzthrough.android.commons.parser.impl.NodeRunner;

import java.io.Serializable;

/**
 * Created by yoram on 22/02/16.
 */
public abstract class AbstractFragment extends Fragment implements Serializable {
    private static final long serialVersionUID = 5527062624094518285L;

    private transient NodeRunner.IResultController controller;
    private Object controllerData;

    public NodeRunner.IResultController getController() {
        return controller;
    }

    public void setController(NodeRunner.IResultController controller) {
        this.controller = controller;
    }

    public Object getControllerData() {
        return controllerData;
    }

    public void setControllerData(Object controllerData) {
        this.controllerData = controllerData;
    }
}
