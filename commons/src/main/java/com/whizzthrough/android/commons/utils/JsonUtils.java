package com.whizzthrough.android.commons.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by yoram on 24/10/15.
 */
public class JsonUtils {
    private static final Gson GSON = new Gson();

    public static void broadcastJson(
            final Context context,
            final String intentMessage,
            final String bundleName,
            final String jsonObject,
            final Class clazz) {

        final Object res = GSON.fromJson(jsonObject, clazz);
        final Serializable serializable = (Serializable) res;
        final Intent i = new Intent(intentMessage);
        final Bundle b = new Bundle();
        b.putSerializable(bundleName, serializable);
        i.putExtras(b);
        context.sendBroadcast(i);

    }
}
