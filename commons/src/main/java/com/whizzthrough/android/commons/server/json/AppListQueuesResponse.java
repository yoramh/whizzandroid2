package com.whizzthrough.android.commons.server.json;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yoram on 13/10/15.
 */
public class AppListQueuesResponse extends BaseResponse {
    private static final long serialVersionUID = -966866760875009614L;

    private int totalConsumedQueues;
    private int maximumAvailableQueues;
    private List<QueueDefinition> queues = new ArrayList<>();

    public int getTotalConsumedQueues() {
        return totalConsumedQueues;
    }

    public void setTotalConsumedQueues(int totalConsumedQueues) {
        this.totalConsumedQueues = totalConsumedQueues;
    }

    public int getMaximumAvailableQueues() {
        return maximumAvailableQueues;
    }

    public void setMaximumAvailableQueues(int maximumAvailableQueues) {
        this.maximumAvailableQueues = maximumAvailableQueues;
    }

    public List<QueueDefinition> getQueues() {
        return queues;
    }

    public void setQueues(List<QueueDefinition> queues) {
        this.queues = queues;
    }
}
