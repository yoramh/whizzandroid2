package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnCollectAccountInformation;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

/**
 * Created by yoram on 15/02/16.
 */
public class CollectAccountInformationJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnCollectAccountInformation;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final BaseResponse res = response.getResponse();
        final IOnCollectAccountInformation events = (IOnCollectAccountInformation ) job.getEvents();
        events.onResponse(res, job.getEventData());
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return BaseResponse.class;
    }

    @Override
    public String getAction() {
        return null;
    }
}
