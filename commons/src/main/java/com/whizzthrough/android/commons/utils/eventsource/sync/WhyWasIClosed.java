package com.whizzthrough.android.commons.utils.eventsource.sync;

/**
 * Created by yoram on 16/02/16.
 */
public enum WhyWasIClosed {
    CR_NORMAL,
    CR_ERROR
}
