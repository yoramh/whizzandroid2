package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.AppAddUserOnQueueResponse;

/**
 * Created by yoram on 29/02/16.
 */
public interface IOnSetTicketDescription extends IOnWhizzThroughServerEvents {
    void onResponse(AppAddUserOnQueueResponse response, Object eventsData);
}
