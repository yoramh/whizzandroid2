package com.whizzthrough.android.commons.fragments.impl;

import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.ISynchroWhizzThroughServer;
import com.whizzthrough.android.commons.server.json.AppListUsersOnQueueRequest;
import com.whizzthrough.android.commons.utils.Config;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 26/02/16.
 */
public class ListUserOnQueueCommand extends AbstractCommand {
    private static final long serialVersionUID = 3081185965963985538L;

    private static final ISynchroWhizzThroughServer SERVER = (ISynchroWhizzThroughServer) WhizzThroughServerFactory.getDefaultServer();

    public static final String COMMAND_NAME = ListUserOnQueueCommand.class.getName();

    private boolean liveTicket = true;
    private boolean oldTicket = false;

    @Override
    protected void doExecute() throws Exception {
        final AppListUsersOnQueueRequest request = new AppListUsersOnQueueRequest();
        request.setQueueName(Config.getQueueDefinition().getName());
        request.setCount(1000);
        request.setStartAt(0);
        request.setLiveTicket(this.liveTicket);
        request.setOldTicket(this.oldTicket);

        this.response = SERVER.listUserOnQueue(Config.getCookie(), request);
    }

    @Override
    public String name() {
        return COMMAND_NAME;
    }

    @Override
    public int label() {
        return R.string.wtcres_progress_action_list_user_on_queues;
    }

    public boolean isOldTicket() {
        return oldTicket;
    }

    public void setOldTicket(boolean oldTicket) {
        this.oldTicket = oldTicket;
    }

    public boolean isLiveTicket() {
        return liveTicket;
    }

    public void setLiveTicket(boolean liveTicket) {
        this.liveTicket = liveTicket;
    }
}
