package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.AppUserStateResponse;

/**
 * Created by yoram on 28/11/15.
 */
public interface IOnGetUserState extends IOnWhizzThroughServerEvents {
    void onResponse(AppUserStateResponse response, Object eventsData);
}
