package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnGetQueueInfoForTerminal;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.AppGetQueueInfoForTerminalResponse;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

/**
 * Created by yoram on 26/05/16.
 */
public class GetQueueInfoForTerminalJsonCall  extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnGetQueueInfoForTerminal;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final AppGetQueueInfoForTerminalResponse res = (AppGetQueueInfoForTerminalResponse)response.getResponse();
        final IOnGetQueueInfoForTerminal events = (IOnGetQueueInfoForTerminal) job.getEvents();
        events.onResponse(res, job.getEventData());
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return AppGetQueueInfoForTerminalResponse.class;
    }

    @Override
    public String getAction() {
        return null;
    }
}
