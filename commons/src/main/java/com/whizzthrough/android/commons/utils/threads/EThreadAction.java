package com.whizzthrough.android.commons.utils.threads;

public enum EThreadAction {
    CONTINUE,
    FINISH;
}
