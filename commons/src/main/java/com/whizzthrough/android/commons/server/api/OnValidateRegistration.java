package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 18/10/15.
 */
public abstract class OnValidateRegistration extends AbstractWhizzThroughEvents implements IOnValidateRegistration {
    public OnValidateRegistration(final Context context) {
        super(context);
    }
}
