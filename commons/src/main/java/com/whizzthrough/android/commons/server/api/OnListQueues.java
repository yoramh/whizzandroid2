package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 15/10/15.
 */
public abstract class OnListQueues extends AbstractWhizzThroughEvents implements IOnListQueues {
    public OnListQueues(final Context context) {
        super(context);
    }
}
