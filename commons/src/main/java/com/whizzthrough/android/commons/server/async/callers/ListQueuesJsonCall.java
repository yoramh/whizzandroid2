package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnListQueues;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.AppListQueuesResponse;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 14/10/15.
 */
public class ListQueuesJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnListQueues;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final AppListQueuesResponse res = (AppListQueuesResponse)response.getResponse();
        final IOnListQueues events = (IOnListQueues)job.getEvents();
        events.onResponse(res, job.getEventData());
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return AppListQueuesResponse.class;
    }

    @Override
    public String getAction() {
        return getContext().getResources().getString(R.string.wtcres_progress_action_list_queue);
    }
}
