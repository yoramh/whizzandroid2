package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnDeleteTicket;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 28/10/15.
 */
public class DeleteTicketJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnDeleteTicket;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final BaseResponse res = (BaseResponse) response.getResponse();
        final IOnDeleteTicket events = (IOnDeleteTicket) job.getEvents();
        events.onResponse(res, job.getEventData());
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return BaseResponse.class;
    }

    @Override
    public String getAction() {
        return getContext().getResources().getString(R.string.wtcres_progress_action_delete_ticket);
    }
}
