package com.whizzthrough.android.commons.fragments.impl;

import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.ISynchroWhizzThroughServer;
import com.whizzthrough.android.commons.server.exceptions.WhizzException;
import com.whizzthrough.android.commons.server.json.PubLoginRequest;
import com.whizzthrough.android.commons.utils.Config;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 23/02/16.
 */
public class CheckStoreCredentialsCommand extends AbstractCommand {
    private static final long serialVersionUID = 2070987514601242660L;

    public static final String COMMAND_NAME = CheckStoreCredentialsCommand.class.getName();

    @Override
    protected void doExecute() throws Exception {
        if (!Config.isAutomaticLogin()) {
            throw new Exception("Login is needed...");
        }

        if (Config.getUsername() == null || Config.getPassword() == null) {
            throw new Exception("Login is needed...");
        }

        if (Config.getCookie() == null) {
            throw new Exception("Login is needed...");
        }

        final ISynchroWhizzThroughServer server = (ISynchroWhizzThroughServer) WhizzThroughServerFactory.getDefaultServer();
        if (server.getUserState(Config.getCookie()) == null) {
            final PubLoginRequest request = new PubLoginRequest();
            request.setEmail(Config.getUsername());
            request.setPassword(Config.getPassword());

            final String cookie = server.signin(request);
            Config.setCookie(cookie);
            Config.save();
            this.response = 0;
        } else {
            this.response = 0;
        }
    }

    @Override
    public String name() {
        return COMMAND_NAME;
    }

    @Override
    public int label() {
        return R.string.wtcres_progress_action_check_store_credentials;
    }
}
