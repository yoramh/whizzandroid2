package com.whizzthrough.android.commons.utils;

/**
 * Created by yoram on 21/10/15.
 */
public interface Constants {
    interface PermissionResult {
        int PERM_REQ_CAMERA = 100;
    }

    interface ServerErrors {
        int USER_ALREADY_ON_QUEUE = 1232;
    }
}
