package com.whizzthrough.android.commons.server.json;

import java.io.Serializable;

/**
 * Created by yoram on 15/02/16.
 */
public class AccountItem implements Serializable {
    private static final long serialVersionUID = -1056185145036400094L;

    /**
     * android, iphone, etc...
     */
    private String source;

    /**
     * Waze, com.google, etc...
     */
    private String type;
    private String name;

    public String getSource() {
        return this.source;
    }

    public void setSource(final String source) {
        this.source = source;
    }

    public String getType() {
        return this.type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
