package com.whizzthrough.android.commons.fragments.impl;

import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.ISynchroWhizzThroughServer;
import com.whizzthrough.android.commons.server.json.PubLoginRequest;

/**
 * Created by yoram on 23/02/16.
 */
public class SigninCommand extends AbstractCommand {
    private static final long serialVersionUID = 726760488385103359L;

    private final String email;
    private final String password;

    public SigninCommand(final String email, final String password) {
        super();

        this.email = email;
        this.password = password;
    }

    @Override
    protected void doExecute() throws Exception {
        final ISynchroWhizzThroughServer server = (ISynchroWhizzThroughServer) WhizzThroughServerFactory.getDefaultServer();
        final PubLoginRequest request = new PubLoginRequest();

        request.setEmail(this.email);
        request.setPassword(this.password);

        this.response = server.signin(request);
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public int label() {
        return 0;
    }
}
