package com.whizzthrough.android.commons.server.json;

import java.io.Serializable;

/**
 * Created by yoram on 13/10/15.
 */
public class QueueDefinition implements Serializable {
    private static final long serialVersionUID = -4037441801455388968L;

    private long id;
    private String name;
    private String description;
    private int currentTicket = 1;
    private int nextTicket = 2;
    private String base64DefaultIcon;
    private long lastModifiedOn;
    private long createdOn;
    private String uniqueCode;
    private boolean open;

    public QueueDefinition() {
        super();
    }

    public QueueDefinition(final String name, final String description) {
        super();

        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCurrentTicket() {
        return currentTicket;
    }

    public void setCurrentTicket(int currentTicket) {
        this.currentTicket = currentTicket;
    }

    public int getNextTicket() {
        return nextTicket;
    }

    public void setNextTicket(int nextTicket) {
        this.nextTicket = nextTicket;
    }

    public String getBase64DefaultIcon() {
        return base64DefaultIcon;
    }

    public void setBase64DefaultIcon(String base64DefaultIcon) {
        this.base64DefaultIcon = base64DefaultIcon;
    }

    public long getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(long lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(long createdOn) {
        this.createdOn = createdOn;
    }

    public String getUniqueCode() {
        return this.uniqueCode;
    }

    public void setUniqueCode(final String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }
}
