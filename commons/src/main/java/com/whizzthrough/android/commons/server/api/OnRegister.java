package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 18/10/15.
 */
public abstract class OnRegister extends AbstractWhizzThroughEvents implements IOnRegister {
    public OnRegister(final Context context) {
        super(context);
    }
}
