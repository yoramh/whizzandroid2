package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnSendMessage;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 13/03/16.
 */
public class SendMessageJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnSendMessage;
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return BaseResponse.class;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final IOnSendMessage events = (IOnSendMessage) job.getEvents();
        events.onResponse(job.getEventData());
    }

    @Override
    public String getAction() {
        return getContext().getResources().getString(R.string.wtcres_progress_action_sending_message);
    }
}
