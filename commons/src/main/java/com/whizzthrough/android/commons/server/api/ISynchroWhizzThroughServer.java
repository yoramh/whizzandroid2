package com.whizzthrough.android.commons.server.api;

import android.content.Context;
import com.whizzthrough.android.commons.server.exceptions.WhizzException;
import com.whizzthrough.android.commons.server.json.*;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

/**
 * Created by yoram on 13/01/16.
 */
public interface ISynchroWhizzThroughServer {
    String signin(PubLoginRequest request) throws WhizzException;
    AppUserStateResponse getUserState(String cookie);
    void openQueue(String cookie, long queueId) throws WhizzException;
    void closeQueue(String cookie, long queueId) throws WhizzException;
    AppListQueuesResponse listQueues(String cookie, AppListQueuesRequest request) throws WhizzException;
    UpdatedQueueInfoResponse getQueueStatus(String cookie, long queueId) throws WhizzException;
    AppListUsersOnQueueResponse listUserOnQueue(String cookie, AppListUsersOnQueueRequest request) throws WhizzException;
    void sendFeedback(String alias, int rating);
}
