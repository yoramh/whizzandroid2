package com.whizzthrough.android.commons.server.json;

import java.io.Serializable;

/**
 * Created by yoram on 21/10/15.
 */
public class QueuePosition implements Serializable {
    private static final long serialVersionUID = 462763390379347913L;

    private String alias;
    private int ticketNumber;
    private long calledOn;
    private int currentTicketOnQueue;

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getTicketNumber() {
        return this.ticketNumber;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public long getCalledOn() {
        return this.calledOn;
    }

    public void setCalledOn(long calledOn) {
        this.calledOn = calledOn;
    }

    public int getCurrentTicketOnQueue() {
        return this.currentTicketOnQueue;
    }

    public void setCurrentTicketOnQueue(int currentTicketOnQueue) {
        this.currentTicketOnQueue = currentTicketOnQueue;
    }
}
