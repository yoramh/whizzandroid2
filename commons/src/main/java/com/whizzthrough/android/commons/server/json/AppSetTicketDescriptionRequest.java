package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 29/02/16.
 */
public class AppSetTicketDescriptionRequest extends BaseRequest {
    private static final long serialVersionUID = -7403570346209275336L;

    private String code;

    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
