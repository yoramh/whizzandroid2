package com.whizzthrough.android.commons.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 20/12/15.
 */
public final class LoginUtils {
    public static final int CURRENT_VERSION = 5;

    private LoginUtils() { }

    public static void askForUpdate(final Activity context, final IDone event) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.wtcres_upgrade_message)
                .setPositiveButton(R.string.wtcres_dlg_ok_btn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        event.done();
                    }
                });
        // Create the AlertDialog object and return it
        builder.create().show();
    }

    public static void askToRelog(final Context context, final IDone event) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.wtcres_relog_message)
                .setPositiveButton(R.string.wtcres_dlg_ok_btn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        event.done();
                    }
                });
        // Create the AlertDialog object and return it
        builder.create().show();
    }
}
