package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 13/03/16.
 */
public abstract class OnSendMessage extends AbstractWhizzThroughEvents implements IOnSendMessage {
    public OnSendMessage(final Context context) {
        super(context);
    }
}
