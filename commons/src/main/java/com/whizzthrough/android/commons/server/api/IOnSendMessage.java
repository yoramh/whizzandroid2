package com.whizzthrough.android.commons.server.api;

/**
 * Created by yoram on 13/03/16.
 */
public interface IOnSendMessage extends IOnWhizzThroughServerEvents {
    void onResponse(Object eventsData);
}
