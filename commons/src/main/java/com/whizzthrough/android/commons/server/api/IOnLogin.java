package com.whizzthrough.android.commons.server.api;

/**
 * Created by yoram on 12/10/15.
 */
public interface IOnLogin extends IOnWhizzThroughServerEvents {
    void onResponse(String cookie, Object eventsData);
}
