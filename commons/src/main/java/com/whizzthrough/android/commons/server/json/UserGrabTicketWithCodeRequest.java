package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 18/10/15.
 */
public class UserGrabTicketWithCodeRequest extends BaseRequest {
    private static final long serialVersionUID = 4239854979956489614L;

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }
}
