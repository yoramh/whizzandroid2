package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.AppIncreaseCurrentTicketResponse;

/**
 * Created by yoram on 25/02/16.
 */
public interface IOnIncreaseCurrentTicket extends IOnWhizzThroughServerEvents {
    void onResponse(AppIncreaseCurrentTicketResponse response, Object eventsData);
}
