package com.whizzthrough.android.commons.server.json;

public class AppIncreaseCurrentTicketResponse extends AppTdeCurrentTicketResponse {
	private static final long serialVersionUID = -9040285888459399931L;

    private boolean noTicketWaiting;

    public boolean isNoTicketWaiting() {
        return noTicketWaiting;
    }

    public void setNoTicketWaiting(boolean noTicketWaiting) {
        this.noTicketWaiting = noTicketWaiting;
    }
}
