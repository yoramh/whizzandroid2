package com.whizzthrough.android.commons.fragments.api;

/**
 * Created by yoram on 18/02/16.
 */
public interface ICommand {
    void begin();
    void execute();
    boolean success();
    void setSuccess(boolean value);
    Object response();
    Throwable exception();
    boolean finished();
    String name();
    int label();
}
