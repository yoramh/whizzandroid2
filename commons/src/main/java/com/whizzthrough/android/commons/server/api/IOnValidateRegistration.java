package com.whizzthrough.android.commons.server.api;

public interface IOnValidateRegistration extends IOnWhizzThroughServerEvents {
    void onResponse(Object eventsData);
}
