package com.whizzthrough.android.commons.parser.impl;

import android.content.Context;
import com.whizzthrough.android.commons.fragments.AbstractFragment;
import com.whizzthrough.android.commons.fragments.RegisterOrSigninFragment;
import com.whizzthrough.android.commons.parser.api.IFragmentParser;

import java.util.Map;

/**
 * Created by yoram on 22/02/16.
 */
public class RegisterOrSigninParserNode extends AbstractParserNode implements IFragmentParser {
    @Override
    public AbstractFragment getFragment(Context context, Map<String, Object> properties) {
        final RegisterOrSigninFragment res = new RegisterOrSigninFragment();

        final NodeParser.ResourceProperty logoIdProp = (NodeParser.ResourceProperty)properties.get("logoid");
        final int logoId = context.getResources().getIdentifier(
                logoIdProp.getResourceName(),
                logoIdProp.getDefinition(),
                logoIdProp.getPackage_());
        res.setLogoDrawableId(logoId);

        return res;
    }
}
