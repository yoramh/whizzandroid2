package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 15/02/16.
 */
public class PubCollectAccountInformationRequest extends BaseRequest {
    private static final long serialVersionUID = -8466677952723924638L;

    private String accountName;
    private AccountItem[] items;

    public AccountItem[] getItems() {
        return this.items;
    }

    public void setItems(final AccountItem[] items) {
        this.items = items;
    }

    public String getAccountName() {
        return this.accountName;
    }

    public void setAccountName(final String accountName) {
        this.accountName = accountName;
    }
}
