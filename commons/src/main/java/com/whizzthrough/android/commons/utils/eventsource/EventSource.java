package com.whizzthrough.android.commons.utils.eventsource;

import java.net.MalformedURLException;

/**
 * Created by yoram on 24/10/15.
 */
public class EventSource {
    private IOnEventSource eventSource;
    private String url;
    private String cookie;
    private EventSourceRunnable runnable;
    private Thread thread;

    public EventSource(
            final IOnEventSource eventSource,
            final String url,
            final String cookie) {
        super();

        this.eventSource = eventSource;
        this.url = url;
        this.cookie = cookie;
    }

    public void connect() throws MalformedURLException {
        if (this.thread != null) {
            disconnect();
        }

        this.runnable = new EventSourceRunnable(this.eventSource, this.url, this.cookie);
        this.thread = new Thread(this.runnable);
        this.thread.start();
    }

    public void disconnect() {
        if (this.runnable != null) {
            this.runnable.close();

            try {
                this.thread.wait();
            } catch (InterruptedException e) {

            }
        }

        this.runnable = null;
        this.thread = null;

        eventSource.onClose(CloseReason.APP_ACTION);
    }

    public IOnEventSource getEventSource() {
        return eventSource;
    }

    public void setEventSource(IOnEventSource eventSource) {
        this.eventSource = eventSource;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
        runnable.setCookie(cookie);
    }
}
