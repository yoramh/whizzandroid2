package com.whizzthrough.android.commons.server.api;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.whizzthrough.android.commons.receivers.ErrorBroadcastReceiver;
import com.whizzthrough.android.commons.server.json.ErrorResponse;
import com.whizzthrough.android.commons.server.json.JsonErrorObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by yoram on 15/10/15.
 */
public abstract class AbstractWhizzThroughEvents implements IOnWhizzThroughServerEvents {
    protected final Context context;

    protected AbstractWhizzThroughEvents() {
        this(null);
    }

    protected AbstractWhizzThroughEvents(final Context context) {
        super();

        this.context = context;
    }

    @Override
    public void onError(@Nullable ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent, @NonNull Throwable t, @Nullable Object eventsData) {
        final ErrorResponse errorResponse = new ErrorResponse();

        errorResponse.setErrors(new JsonErrorObject[] {new JsonErrorObject().defaultMessage(t.getMessage()).messageId(-1)});

        onError(afterErrorEvent, errorResponse, eventsData);
    }

    @Override
    public void onError(@NonNull final Throwable t, final @Nullable Object eventsData) {
        onError(null, t, eventsData);
    }

    @Override
    public void onError(@Nullable ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent, @NonNull ErrorResponse response, @Nullable Object eventsData) {
        final StringBuilder sb = new StringBuilder();

        for (JsonErrorObject error: response.getErrors()) {
            if (sb.length() != 0) {
                sb.append("\n");
            }

            sb.append(error.getDefaultMessage());
        }
        final Intent i = new Intent(ErrorBroadcastReceiver.FILTER_ERROR_MESSAGE);
        i.putExtra(ErrorBroadcastReceiver.BUNDLE_ERROR_MESSAGE, sb.toString());

        boolean close = false;

        final ArrayList<Integer> ids = new ArrayList<>();

        for (final JsonErrorObject o: response.getErrors()) {
            if (o.isFatal()) {
                close = true;
            }

            ids.add(o.getMessageId());
        }

        i.putExtra(ErrorBroadcastReceiver.BUNDLE_CLOSE, close);
        i.putIntegerArrayListExtra(ErrorBroadcastReceiver.BUNDLE_ERROR_MESSAGE_IDS, ids);

        if (afterErrorEvent != null) {
            i.putExtra(ErrorBroadcastReceiver.BUNDLE_EVENT, afterErrorEvent);
        }

        this.context.sendBroadcast(i);
    }

    @Override
    public void onError(final ErrorResponse response, final Object eventsData) {
        onError(null, response, eventsData);
    }

    @Override
    public void onUnauthorized() {
    }
}
