package com.whizzthrough.android.commons.server.json;

import java.util.Collection;

public class UpdatedQueueInfoResponse extends BaseResponse {
    private boolean open;
    private Collection<AppAddUserOnQueueResponse> userOnQueues;
    private int currentTicket;

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public Collection<AppAddUserOnQueueResponse> getUserOnQueues() {
        return userOnQueues;
    }

    public void setUserOnQueues(Collection<AppAddUserOnQueueResponse> userOnQueues) {
        this.userOnQueues = userOnQueues;
    }

    public int getCurrentTicket() {
        return currentTicket;
    }

    public void setCurrentTicket(int currentTicket) {
        this.currentTicket = currentTicket;
    }
}
