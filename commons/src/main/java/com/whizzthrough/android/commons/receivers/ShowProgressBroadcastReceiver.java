package com.whizzthrough.android.commons.receivers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.whizzthrough.android.commons.utils.Config;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 25/10/15.
 */
public class ShowProgressBroadcastReceiver extends AbstractBroadcastReceiver {
    public static final String FILTER_START_PROGRESS = ShowProgressBroadcastReceiver.class.getName() + ".FILTER.START_PROGRESS";
    public static final String FILTER_END_PROGRESS = ShowProgressBroadcastReceiver.class.getName() + ".FILTER.END_PROGRESS";

    public static final String BUNDLE_ACTION = ShowProgressBroadcastReceiver.class.getName() + ".BUNDLE.ACTION";
    public static final String BUNDLE_PERCENT = ShowProgressBroadcastReceiver.class.getName() + ".BUNDLE.PERCENT";

    public static void runProgress(final Context context, final String message, final int pct) {
        final Intent i = new Intent(FILTER_START_PROGRESS);
        i.putExtra(BUNDLE_ACTION, message);
        i.putExtra(BUNDLE_PERCENT, pct);
        context.sendBroadcast(i);
    }

    public static void stopProgress(final Context context) {
        final Intent i = new Intent(FILTER_END_PROGRESS);
        context.sendBroadcast(i);
    }

    private Set<Context> contexts = new CopyOnWriteArraySet<>();
    private ProgressDialog progressDialog = null;

    public ShowProgressBroadcastReceiver() {
        super();
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (intent != null) {
            if (intent.getAction().equals(FILTER_START_PROGRESS)) {
                showProgress(context, intent);
            } else {
                dismissProgress(context, intent);
            }
        }
    }

    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter res = new IntentFilter();
        res.addAction(FILTER_START_PROGRESS);
        res.addAction(FILTER_END_PROGRESS);
        return res;
    }

    private void showProgress(final Context context, final Intent intent) {
        if (progressDialog == null && Config.getCurrentContext() != null) {
            progressDialog = new ProgressDialog(Config.getCurrentContext(), R.style.SafelaneDarkTheme);
            progressDialog.setMax(100);
            progressDialog.setMessage(intent.getStringExtra(BUNDLE_ACTION));
            progressDialog.setCancelable(false);
            progressDialog.show();
        } else {
            progressDialog.setMessage(intent.getStringExtra(BUNDLE_ACTION));
            progressDialog.setProgress(intent.getIntExtra(BUNDLE_PERCENT, 0));
        }

        contexts.add(context);
    }

    private void dismissProgress(final Context context, final Intent intent) {
        if (progressDialog != null) {
            if (contexts.contains(context)) {
                contexts.remove(context);
            }

            if (contexts.size() == 0) {
                try {
                    progressDialog.dismiss();
                } catch (Throwable t) {
                    t.printStackTrace();
                }
                progressDialog = null;
            }
        }
    }
}
