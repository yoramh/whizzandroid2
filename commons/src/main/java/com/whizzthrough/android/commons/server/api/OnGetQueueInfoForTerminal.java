package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 26/05/16.
 */
public abstract class OnGetQueueInfoForTerminal extends AbstractWhizzThroughEvents implements IOnGetQueueInfoForTerminal{
    public OnGetQueueInfoForTerminal(final Context context) {
        super(context);
    }
}
