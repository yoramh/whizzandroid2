package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 12/11/15.
 */
public class AppListUsersOnQueueResponse extends BaseResponse {
    private static final long serialVersionUID = 4816429046789968257L;

    private int totalNumberOfTickets;
    private AppAddUserOnQueueResponse[] rows;

    public int getTotalNumberOfTickets() {
        return totalNumberOfTickets;
    }

    public void setTotalNumberOfTickets(int totalNumberOfTickets) {
        this.totalNumberOfTickets = totalNumberOfTickets;
    }

    public AppAddUserOnQueueResponse[] getRows() {
        return rows;
    }

    public void setRows(AppAddUserOnQueueResponse[] rows) {
        this.rows = rows;
    }
}
