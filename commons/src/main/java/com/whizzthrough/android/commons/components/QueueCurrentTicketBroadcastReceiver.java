package com.whizzthrough.android.commons.components;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.TextView;
import com.whizzthrough.android.commons.receivers.AbstractBroadcastReceiver;
import com.whizzthrough.android.commons.server.json.AppTdeCurrentTicketResponse;
import com.whizzthrough.android.commons.server.json.SseBackOfficeServeQueueResponse;
import com.whizzthrough.android.commons.utils.Config;

/**
 * Created by yoram on 25/02/16.
 */
public class QueueCurrentTicketBroadcastReceiver extends AbstractBroadcastReceiver {
    private static final String TAG = QueueCurrentTicketBroadcastReceiver.class.getSimpleName();
    private static final String ROOT_KEY = QueueCurrentTicketBroadcastReceiver.class.getName();
    public static final String FILTER_EVENT = ROOT_KEY + ".FILTER_EVENT";
    public static final String MSG_CURRENT_TICKET_CHANGE = ROOT_KEY + ".MSG_CURRENT_TICKET_CHANGE";
    public static final String MSG_SSE_BACK_OFFICE_SERVE_QUEUE = ROOT_KEY + ".MSG_SSE_BACK_OFFICE_SERVE_QUEUE";

    private TextView label;

    public QueueCurrentTicketBroadcastReceiver() {
        super();
    }

    public QueueCurrentTicketBroadcastReceiver(final TextView label) {
        super();

        this.label = label;
    }

    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter res = new IntentFilter();
        res.addAction(FILTER_EVENT);
        return res;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (this.label != null) {
            final int currentTicket;

            if (intent.hasExtra(MSG_CURRENT_TICKET_CHANGE)) {
                currentTicket = ((AppTdeCurrentTicketResponse)intent.getSerializableExtra(MSG_CURRENT_TICKET_CHANGE)).getCurrentTicket();
            } else if (intent.hasExtra(MSG_SSE_BACK_OFFICE_SERVE_QUEUE)) {
                currentTicket = ((SseBackOfficeServeQueueResponse)intent.getSerializableExtra(MSG_SSE_BACK_OFFICE_SERVE_QUEUE)).getCurrentTicket().getCurrentTicket();
            } else {
                Log.w(TAG, "Unknown message");
                return;
            }

            this.label.setText("" + currentTicket);

            Config.getQueueDefinition().setCurrentTicket(currentTicket);
            Config.save();
        }
    }

}
