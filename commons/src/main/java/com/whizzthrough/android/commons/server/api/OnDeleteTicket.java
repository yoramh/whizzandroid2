package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 28/10/15.
 */
public abstract class OnDeleteTicket extends AbstractWhizzThroughEvents implements IOnDeleteTicket {
    public OnDeleteTicket(final Context context) {
        super(context);
    }
}