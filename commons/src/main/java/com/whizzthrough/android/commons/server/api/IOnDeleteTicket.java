package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.BaseResponse;

/**
 * Created by yoram on 28/10/15.
 */
public interface IOnDeleteTicket extends IOnWhizzThroughServerEvents {
    void onResponse(BaseResponse response, Object eventsData);
}
