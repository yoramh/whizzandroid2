package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnListUsersOnQueue;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.AppListUsersOnQueueResponse;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 02/03/16.
 */
public class ListUsersOnQueueJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnListUsersOnQueue;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final AppListUsersOnQueueResponse res = (AppListUsersOnQueueResponse)response.getResponse();
        final IOnListUsersOnQueue events = (IOnListUsersOnQueue)job.getEvents();
        events.onResponse(res, job.getEventData());
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return AppListUsersOnQueueResponse.class;
    }

    @Override
    public String getAction() {
        return getContext().getResources().getString(R.string.wtcres_progress_action_list_ticket);
    }
}
