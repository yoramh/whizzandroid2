package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.AppListUsersOnQueueResponse;

/**
 * Created by yoram on 02/03/16.
 */
public interface IOnListUsersOnQueue extends IOnWhizzThroughServerEvents {
    void onResponse(AppListUsersOnQueueResponse response, Object eventsData);
}
