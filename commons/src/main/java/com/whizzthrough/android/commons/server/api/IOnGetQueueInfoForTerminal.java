package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.AppGetQueueInfoForTerminalResponse;

/**
 * Created by yoram on 26/05/16.
 */
public interface IOnGetQueueInfoForTerminal extends IOnWhizzThroughServerEvents {
    void onResponse(AppGetQueueInfoForTerminalResponse response, Object eventsData);
}
