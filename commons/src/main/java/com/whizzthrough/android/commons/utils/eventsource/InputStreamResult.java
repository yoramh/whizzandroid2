package com.whizzthrough.android.commons.utils.eventsource;

import java.io.InputStream;

/**
 * Created by yoram on 25/10/15.
 */
public class InputStreamResult {
    private int responseCode = -1;
    private InputStream inputStream;
    private Throwable error;

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int returnCode) {
        this.responseCode = returnCode;
    }
}
