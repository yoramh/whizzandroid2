package com.whizzthrough.android.commons.server.async.callers;

import android.content.Context;

import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

/**
 * Created by yoram on 14/10/15.
 */
public interface IJsonCall {
    boolean supports(IOnWhizzThroughServerEvents events);
    void afterRun(JsonResponse response, JsonJob job) throws Exception;
    Class<? extends BaseResponse> getResponseClass();

    String getAction();

    void setContext(Context context);

    Context getContext();
}
