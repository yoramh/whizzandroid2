package com.whizzthrough.android.commons.utils.threads;

import android.os.SystemClock;

import java.time.Clock;
import java.util.concurrent.atomic.AtomicBoolean;

import shopping.safelane.android.commons.utils.SystemUtils;

public abstract class AbstractRunnable implements Runnable {
    private final AtomicBoolean finished = new AtomicBoolean(false);
    private final AtomicBoolean interrupted = new AtomicBoolean(false);
    private final AtomicBoolean canceled = new AtomicBoolean(false);
    private Throwable error;

    @Override
    public final void run() {
        try {
            while (!finished.get() && !canceled.get()) {
                if (doRun() == EThreadAction.FINISH) {
                    finished.set(true);

                    return;
                }

                SystemClock.sleep(sleepTime());
            }
        } catch (InterruptedException e) {
            interrupted.set(true);
            finished.set(true);
        } catch (Throwable t) {
            error = t;
            finished.set(true);
        }
    }

    public void cancel() {
        canceled.set(true);
    }

    public boolean isFinished() {
        return finished.get();
    }

    public boolean isCancelled() {
        return canceled.get();
    }
    public boolean isInterrupted() {
        return interrupted.get();
    }

    public Throwable getError() {
        return error;
    }

    abstract protected EThreadAction doRun() throws Exception;

    protected long sleepTime() {
        return 1000;
    }
}
