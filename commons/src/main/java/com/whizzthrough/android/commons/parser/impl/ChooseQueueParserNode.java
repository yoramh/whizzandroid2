package com.whizzthrough.android.commons.parser.impl;

import android.content.Context;
import com.whizzthrough.android.commons.fragments.AbstractFragment;
import com.whizzthrough.android.commons.fragments.ChooseQueueFragment;
import com.whizzthrough.android.commons.parser.api.IFragmentParser;

import java.util.Map;

/**
 * Created by yoram on 24/02/16.
 */
public class ChooseQueueParserNode extends AbstractParserNode implements IFragmentParser {
    private static final long serialVersionUID = 8652663922675226371L;

    @Override
    public AbstractFragment getFragment(Context context, Map<String, Object> properties) {
        final ChooseQueueFragment res = new ChooseQueueFragment();

        final NodeParser.ResourceProperty logoIdProp = (NodeParser.ResourceProperty)properties.get("logoid");
        final int logoId = logoIdProp.findResource(context);
        res.setLogoDrawableId(logoId);

        return res;
    }
}
