package com.whizzthrough.android.commons.server.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.whizzthrough.android.commons.receivers.ErrorBroadcastReceiver;
import com.whizzthrough.android.commons.server.json.ErrorResponse;

public interface IOnWhizzThroughServerEvents {
    @Deprecated
    void onError(@NonNull Throwable t, @Nullable Object eventsData);
    @Deprecated
    void onError(@NonNull ErrorResponse response, @Nullable Object eventsData);
    default void onError(@Nullable ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent, @NonNull ErrorResponse response, @Nullable Object eventsData) {}
    default void onError(@Nullable ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent, @NonNull Throwable t, @Nullable Object eventsData) {}
    void onUnauthorized();
}
