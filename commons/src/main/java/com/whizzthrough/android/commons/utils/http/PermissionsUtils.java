package com.whizzthrough.android.commons.utils.http;

import android.app.Activity;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/**
 * Created by yoram on 19/10/15.
 */
public class PermissionsUtils {
    public static boolean requestPermission(
            final Activity activity,
            final String[] permissions,
            final int requestCode) {
        boolean allgranted = true;

        for (final String permission: permissions) {
            if (ContextCompat.checkSelfPermission(
                    activity,
                    permission) != PackageManager.PERMISSION_GRANTED) {
                allgranted = false;
            }
        }

        if (!allgranted) {
            // Should we show an explanation?
            /*
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    permission)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.


                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }*/

            ActivityCompat.requestPermissions(activity, permissions, requestCode);
            return true;
        } else {
            return false;
        }

    }
}
