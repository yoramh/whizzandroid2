package com.whizzthrough.android.commons.server.exceptions;

/**
 * Created by yoram on 13/01/16.
 */
public class WhizzAuthorizationException extends WhizzException {
    private static final long serialVersionUID = -4115834149415023051L;

    public WhizzAuthorizationException() {
        super();
    }
}
