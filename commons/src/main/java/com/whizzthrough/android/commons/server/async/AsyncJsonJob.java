package com.whizzthrough.android.commons.server.async;

import android.content.Context;
import android.os.AsyncTask;

import android.util.Log;
import com.whizzthrough.android.commons.receivers.ShowProgressBroadcastReceiver;
import com.whizzthrough.android.commons.server.async.callers.*;
import com.whizzthrough.android.commons.server.exceptions.WhizzAuthenticationException;
import com.whizzthrough.android.commons.server.exceptions.WhizzAuthorizationException;
import com.whizzthrough.android.commons.server.json.ErrorResponse;
import com.whizzthrough.android.commons.utils.http.HttpUtils;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import shopping.safelane.android.commons.utils.SystemUtils;

/**
 * Created by yoram on 14/10/15.
 */
public class AsyncJsonJob extends AsyncTask<JsonJob, Void, Object> {
    private static final List<IJsonCall> JSON_CALLS = new ArrayList<>();

    static {
        register(new LoginJsonCall());
        register(new OnRegisterJsonCall());
        register(new OnValidateRegistrationJsonCall());
        register(new OnResendRegistrationSmsJsonCall());
        register(new ListQueuesJsonCall());
        register(new GrabTicketJsonCall());
        register(new GetCurrentQueueStatusJsonCall());
        register(new GetAllMyTicketsJsonCall());
        register(new DeleteTicketJsonCall());
        register(new GetUserStateJsonCall());
        register(new CollectAccountInformationJsonCall());
        register(new IncreaseCurrentTicketJsonCall());
        register(new SetTicketDescriptionJsonCall());
        register(new ListUsersOnQueueJsonCall());
        register(new SendMessageJsonCall());
        register(new GetQueueInfoForTerminalJsonCall());
    }

    public static void register(final IJsonCall jsonCall) {
        JSON_CALLS.add(jsonCall);
    }

    public static IJsonCall findSuitableCall(final JsonJob job) {
        for (final IJsonCall call: JSON_CALLS) {
            if (call.supports(job.getEvents())) {
                return call;
            }
        }

        return null;
    }

    private final String cookie;
    private final Context context;

    public AsyncJsonJob(final Context context) {
        this(context, null);
    }

    public AsyncJsonJob(final Context context, final String cookie) {
        super();

        this.cookie = cookie;
        this.context = context;
    }

    @Override
    protected Object doInBackground(JsonJob... params) {
        for (final JsonJob job: params) {
            try {
                Log.d("WT", "Calling url: " + job.getUrl() + " with data " + job.getRequest());
                IJsonCall caller = findSuitableCall(job);
                if (caller == null) {
                    throw new IOException(
                            "No suitable caller for " + job.getEvents().getClass().toString());
                }

                caller.setContext(context);
                String action = caller.getAction();
                if (action != null) {
                    action = action + "...";
                    ShowProgressBroadcastReceiver.runProgress(context, action, 0);
                }

                final HttpUtils utils = new HttpUtils(job.getUrl());
                final ByteArrayOutputStream baos = new ByteArrayOutputStream();

                if (action != null) {
                    ShowProgressBroadcastReceiver.runProgress(context, action, 50);
                }

                final JsonResponse httpResponse = new JsonResponse();

                final String locale = (job.getRequest().getLocale() == null) ? Locale.getDefault().toString() : job.getRequest().getLocale();
                job.getRequest().locale(locale);

                if (this.cookie != null) {
                    utils.getHeaders().put("Cookie", this.cookie);
                }
                utils.postJson(job.getRequest(), caller.getResponseClass(), httpResponse);

                if (action != null) {
                    ShowProgressBroadcastReceiver.runProgress(context, action, 75);
                }

                ShowProgressBroadcastReceiver.stopProgress(context);

                if (httpResponse.getResponse() instanceof ErrorResponse) {
                    if (job.getAfterErrorEvent() != null) {
                        job.getEvents().onError(job.getAfterErrorEvent(), (ErrorResponse) httpResponse.getResponse(), job.getEventData());
                    } else {
                        job.getEvents().onError((ErrorResponse) httpResponse.getResponse(), job.getEventData());
                    }
                } else {
                    caller.afterRun(httpResponse, job);
                }

                /*
                if (action != null) {
                    ShowProgressBroadcastReceiver.stopProgress(context);
                }*/
            } catch (WhizzAuthenticationException e) {
                ShowProgressBroadcastReceiver.stopProgress(context);
                job.getEvents().onUnauthorized();
            } catch (WhizzAuthorizationException e) {
                ShowProgressBroadcastReceiver.stopProgress(context);
                job.getEvents().onUnauthorized();
            } catch (Throwable t) {
                ShowProgressBroadcastReceiver.stopProgress(context);
                if (job.getAfterErrorEvent() != null) {
                    job.getEvents().onError(job.getAfterErrorEvent(), t, job.getEventData());
                } else {
                    job.getEvents().onError(t, job.getEventData());
                }
            } finally {
            }
        }

        return null;
    }
}