package com.whizzthrough.android.commons.server.json;

public final class PubRegisterAccountRequest extends BaseRequest {
	private static final long serialVersionUID = 7317148634017488015L;

	private String userId;
	private EApplication application;
	private String password;
	private String name;
	private String callerReturnInfo;
	private String deviceType = "ANDROID";

	public String getUserId() {
		return userId;
	}

	public PubRegisterAccountRequest userId(final String userId) {
		this.userId = userId;

		return this;
	}

	public EApplication getApplication() {
		return application;
	}

	public PubRegisterAccountRequest application(final EApplication application) {
		this.application = application;

		return this;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public PubRegisterAccountRequest deviceType(String deviceType) {
		throw new UnsupportedOperationException();
	}

	public String getPassword() {
		return password;
	}

	public PubRegisterAccountRequest password(final String password) {
		this.password = password;

		return this;
	}

	public String getName() {
		return name;
	}

	public PubRegisterAccountRequest name(final String name) {
		this.name = name;

		return this;
	}

	@Override
	public PubRegisterAccountRequest locale(String locale) {
		return (PubRegisterAccountRequest)super.locale(locale);
	}


	public String getCallerReturnInfo() {
		return callerReturnInfo;
	}

	public PubRegisterAccountRequest callerReturnInfo(final String callerReturnInfo) {
		this.callerReturnInfo = callerReturnInfo;

		return this;
	}
}
