package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 26/05/16.
 */
public class AppGetQueueInfoForTerminalResponse extends BaseResponse {
    private static final long serialVersionUID = 7938966518576557246L;

    private String b64QrCode;

    public String getB64QrCode() {
        return b64QrCode;
    }

    public void setB64QrCode(String b64QrCode) {
        this.b64QrCode = b64QrCode;
    }
}
