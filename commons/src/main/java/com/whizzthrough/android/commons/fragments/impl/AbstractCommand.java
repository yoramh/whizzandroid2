package com.whizzthrough.android.commons.fragments.impl;

import com.whizzthrough.android.commons.fragments.api.ICommand;

import java.io.Serializable;

/**
 * Created by yoram on 23/02/16.
 */
public abstract class AbstractCommand implements ICommand, Serializable {
    protected boolean success = false;
    protected boolean finished = false;
    protected Object response = null;
    protected Throwable exception = null;

    @Override
    public void begin() {

    }

    protected abstract void doExecute() throws Exception;

    @Override
    public void execute() {
        try {
            doExecute();
            this.success = true;
        } catch (Throwable t) {
            this.exception = t;
            this.success = false;
        }

        this.finished = true;
    }

    @Override
    public boolean success() {
        return this.success;
    }

    @Override
    public Object response() {
        return this.response;
    }

    @Override
    public Throwable exception() {
        return this.exception;
    }

    @Override
    public boolean finished() {
        return this.finished;
    }

    @Override
    public void setSuccess(boolean success) {
        this.success = success;
    }
}
