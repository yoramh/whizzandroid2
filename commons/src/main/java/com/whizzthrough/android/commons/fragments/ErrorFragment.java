package com.whizzthrough.android.commons.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whizzthrough.android.commons.utils.AndroidUtils;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 22/02/16.
 */
public class ErrorFragment extends AbstractFragment {
    private static final long serialVersionUID = 4102811975535669612L;

    private String errorMessage;
    private String clickHereMessage;
    private String link;

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.frag_error, container, false);

        String s = rootView.getContext().getPackageName();

        if (savedInstanceState != null) {
            errorMessage = savedInstanceState.getString("errorMessage");
            clickHereMessage = savedInstanceState.getString("clickHereMessage");
            link = savedInstanceState.getString("link");
        }

        TextView txt = (TextView)rootView.findViewById(R.id.wtc_frag_error_message);
        txt.setText(this.errorMessage);

        txt = (TextView)rootView.findViewById(R.id.wtc_frag_error_link);
        if (link == null) {
            txt.setVisibility(View.GONE);
        } else {
            AndroidUtils.undelineTextView(this.getContext(), txt, this.clickHereMessage);
            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(link));
                    ErrorFragment.this.startActivity(browserIntent);
                }
            });
        }

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("errorMessage", errorMessage);
        outState.putString("clickHereMessage", clickHereMessage);
        outState.putString("link", link);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getClickHereMessage() {
        return clickHereMessage;
    }

    public void setClickHereMessage(String clickHereMessage) {
        this.clickHereMessage = clickHereMessage;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
