package com.whizzthrough.android.commons.receivers;

import android.content.Context;
import android.content.Intent;

/**
 * Created by yoram on 12/03/16.
 */
public abstract class AbstractServiceConnectivityReceiver extends AbstractBroadcastReceiver {
    public static final String ACTION_CONNECTED = AbstractServiceConnectivityReceiver.class.getName() + ".ACTION.CONNECTED";
    public static final String ACTION_DISCONNECTED = AbstractServiceConnectivityReceiver.class.getName() + ".ACTION.CONNECTED";

    public static final String EXTRA_SERVICE_CLASS_NAME = AbstractServiceConnectivityReceiver.class.getName() + ".EXTRA.SERVICE_CLASS_NAME";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_CONNECTED)) {
            onConnected(context, intent);
        } else if (intent.getAction().equals(ACTION_DISCONNECTED)) {
            onDisconnected(context, intent);
        }
    }

    protected abstract void onConnected(Context context, Intent intent);
    protected abstract void onDisconnected(Context context, Intent intent);
}
