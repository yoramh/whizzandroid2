package com.whizzthrough.android.commons.server.async;

import com.whizzthrough.android.commons.receivers.ErrorBroadcastReceiver;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.json.BaseRequest;

/**
 * Created by yoram on 14/10/15.
 */
public class JsonJob {
    public enum RequestType {
        RT_GET,
        RT_POST
    }

    private final BaseRequest request;
    private final String url;
    private final RequestType requestType;
    private final IOnWhizzThroughServerEvents events;
    private final Object eventData;
    private final ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent;
    //private final String cookie;

    public JsonJob(final BaseRequest request, final String url, final IOnWhizzThroughServerEvents events, final ErrorBroadcastReceiver.OnAfterErrorEvent onAfterErrorEvent, final Object eventData, final RequestType requestType) {
        super();
        this.request = request;
        this.url = url;
        this.requestType = requestType;
        this.events = events;
        this.eventData = eventData;
        this.afterErrorEvent = onAfterErrorEvent;
    }

    public JsonJob(final BaseRequest request, final String url, final IOnWhizzThroughServerEvents events, final Object eventData, final RequestType requestType) {
        this(request, url, events, null, eventData, requestType);
    }

    public JsonJob(final BaseRequest request, final String url, final IOnWhizzThroughServerEvents events, final Object eventData) {
        this(request, url, events, eventData, RequestType.RT_POST);
    }

    public BaseRequest getRequest() {
        return request;
    }

    public String getUrl() {
        return url;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public IOnWhizzThroughServerEvents getEvents() {
        return events;
    }

    public Object getEventData() {
        return eventData;
    }

    public ErrorBroadcastReceiver.OnAfterErrorEvent getAfterErrorEvent() {
        return afterErrorEvent;
    }
}
