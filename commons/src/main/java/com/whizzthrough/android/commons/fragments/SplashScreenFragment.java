package com.whizzthrough.android.commons.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whizzthrough.android.commons.fragments.api.CommandThread;
import com.whizzthrough.android.commons.fragments.api.ICommand;
import shopping.safelane.android.commons.utils.SystemUtils;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 18/02/16.
 */
public class SplashScreenFragment extends AbstractFragment {
    private static final long serialVersionUID = 5091170798595841293L;

    private int logoDrawableId;
    private ICommand[] commands;
    private ICommand[] runningCommands;

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.frag_splash_screen, container, false);

        final TextView logoText = (TextView)rootView.findViewById(R.id.wtc_frag_splash_screen_logoText);

        final Drawable logo;

        if (savedInstanceState == null) {
            logo = getResources().getDrawable(this.logoDrawableId);
        } else {
            logo = getResources().getDrawable(savedInstanceState.getInt("logoDrawableId"));
            this.commands = (ICommand[])savedInstanceState.getSerializable("commands");
        }

        logoText.setCompoundDrawablesWithIntrinsicBounds(null, logo, null, null);

        new Thread(new Runnable() {
            @Override
            public void run() {
                SplashScreenFragment.this.runningCommands = new ICommand[SplashScreenFragment.this.commands.length];

                final TextView actionText = (TextView)rootView.findViewById(R.id.wtc_frag_splash_screen_actionText);

                for (int i = 0; i < SplashScreenFragment.this.runningCommands.length; i++) {
                    final ICommand command = SplashScreenFragment.this.commands[i];
                    final CommandThread thread = new CommandThread(command);
                    SplashScreenFragment.this.runningCommands[i] = thread;
                    thread.start();

                    SplashScreenFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            actionText.setText(rootView.getResources().getString(thread.label()));
                        }
                    });
                }

                while (true) {
                    boolean allFinished = true;
                    for (final ICommand command: SplashScreenFragment.this.runningCommands) {
                        if (!command.finished()) {
                            allFinished = false;

                            break;
                        }
                    }

                    if (allFinished) {
                        Integer result = null;
                        for (int i = 0; i < SplashScreenFragment.this.runningCommands.length; i++) {
                            if (!SplashScreenFragment.this.runningCommands[0].success()) {
                                result = 0;
                                break;
                            }
                        }

                        SplashScreenFragment.this.getController().onNext(
                                result != null ? result : 1,
                                SplashScreenFragment.this.getControllerData());

                        break;
                    }

                    SystemUtils.sleep(500);
                }
            }
        }).start();

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("logoDrawableId", this.getLogoDrawableId());
        outState.putSerializable("commands", this.getCommands());
    }

    public int getLogoDrawableId() {
        return logoDrawableId;
    }

    public void setLogoDrawableId(int logoDrawableId) {
        this.logoDrawableId = logoDrawableId;
    }

    public ICommand[] getCommands() {
        return commands;
    }

    public void setCommands(ICommand[] commands) {
        this.commands = commands;
    }
}
