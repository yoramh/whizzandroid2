package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.UserGetAllMyTicketsResponse;

/**
 * Created by yoram on 25/10/15.
 */
public interface IOnGetAllMyTickets extends IOnWhizzThroughServerEvents {
    void onResponse(UserGetAllMyTicketsResponse response, Object eventsData);

}
