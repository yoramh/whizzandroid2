package com.whizzthrough.android.commons.server.json;

@Deprecated
public class AppIncreaseCurrentTicketRequest extends BaseRequest {
	private static final long serialVersionUID = -1907052363008829306L;

	private String queueName;

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }
}
