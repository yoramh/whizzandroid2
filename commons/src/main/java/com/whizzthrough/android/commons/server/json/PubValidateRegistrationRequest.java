package com.whizzthrough.android.commons.server.json;

public final class PubValidateRegistrationRequest extends BaseRequest {
	private String uniqueId;
	private String challengeData;
	private EApplication application;

	public String getChallengeData() {
		return challengeData;
	}

	public PubValidateRegistrationRequest challengeData(String challengeData) {
		this.challengeData = challengeData;

		return this;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public PubValidateRegistrationRequest uniqueId(String uniqueId) {
		this.uniqueId = uniqueId;

		return this;
	}

	public EApplication getApplication() {
		return application;
	}

	public PubValidateRegistrationRequest application(EApplication application) {
		this.application = application;

		return this;
	}
}
