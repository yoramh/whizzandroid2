package com.whizzthrough.android.commons.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import com.whizzthrough.android.commons.parser.impl.NodeParser;
import com.whizzthrough.android.commons.parser.impl.NodeRunner;
import com.whizzthrough.android.commons.utils.Config;

import java.io.Serializable;
import java.util.Stack;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 24/05/16.
 */
public abstract class BlankActivity extends AppCompatActivity implements NodeRunner.IActivityController {
    private static class Holder implements Serializable {
        private static final long serialVersionUID = 5299194042562114451L;

        private NodeParser.Node node;
        private Fragment fragment;
    }

    private boolean started = false;
    private NodeParser.Node currentNode;
    private Stack<Holder> backNode = new Stack<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_blank);

        Config.setCurrentContext(this);

        this.currentNode = null;
        if (savedInstanceState == null) {
            try {
                this.currentNode = NodeParser.parse(this, getDefinitionXml());
            } catch (Throwable t) {
                Log.e("WT", t.getMessage(), t);
            }
        } else {
            this.currentNode = (NodeParser.Node)savedInstanceState.getSerializable("currentNode");
            this.backNode = (Stack<Holder>)savedInstanceState.getSerializable("backNode");
            this.started = savedInstanceState.getBoolean("started", false);
        }

        if (started) {
            Intent intent = new Intent(this, getNextActivityClass());
            startActivity(intent);
            finish();
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    NodeRunner.run(BlankActivity.this.currentNode, BlankActivity.this, BlankActivity.this);
                }
            });
        }
    }

    @Override
    public void showFragment(final Fragment fragment, final NodeParser.Node node) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();

                transaction.replace(R.id.common_blank_activity_content_main_frame, fragment);
                transaction.addToBackStack(null);
                transaction.commit();

                Holder h = new Holder();
                h.fragment = fragment;
                h.node = node;
                BlankActivity.this.backNode.push(h);
            }
        });
    }

    @Override
    public void popBackableStack() {
        final FragmentManager fragmentManager = getSupportFragmentManager();

        this.backNode.clear();
    }

    @Override
    public void setCurrentNode(NodeParser.Node node) {
        this.currentNode = node;
    }

    @Override
    public void endProcess() {
        this.backNode = null;

        Intent i = new Intent(this, getNextActivityClass());
        startActivity(i);
    }

    protected abstract Class<?> getNextActivityClass();
    protected abstract int getDefinitionXml();
}
