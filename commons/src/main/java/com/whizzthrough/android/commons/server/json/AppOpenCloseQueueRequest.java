package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 26/05/16.
 */
public class AppOpenCloseQueueRequest extends BaseRequest {
    private Long queueId = null;
    private Boolean open;

    public Long getQueueId() {
        return queueId;
    }

    public AppOpenCloseQueueRequest queueId(Long queueId) {
        this.queueId = queueId;

        return this;
    }

    public Boolean getOpen() {
        return open;
    }

    public AppOpenCloseQueueRequest open(Boolean open) {
        this.open = open;

        return this;
    }
}
