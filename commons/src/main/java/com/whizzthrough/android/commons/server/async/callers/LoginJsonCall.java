package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnLogin;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

import java.io.IOException;
import java.util.List;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 14/10/15.
 */
public class LoginJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnLogin;
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return BaseResponse.class;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final IOnLogin events = (IOnLogin)job.getEvents();
        final StringBuilder sb = new StringBuilder();

        if (response.getHeaders().containsKey("Set-Cookie")) {
            final List<String> list = response.getHeaders().get("Set-Cookie");

            for (final String s: list) {
                if (sb.length() > 0) {
                    sb.append("; ");
                }

                sb.append(s);
            }
        } else {
            throw new IOException("Authentication Failed");
        }

        events.onResponse(sb.toString(), job.getEventData());
    }

    @Override
    public String getAction() {
        return getContext().getResources().getString(R.string.wtcres_progress_action_login);
    }
}
