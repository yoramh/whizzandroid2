package com.whizzthrough.android.commons.server.json;

public class JsonErrorObject extends BaseRequestResponse  {
	private static final long serialVersionUID = 5942733642182981857L;

	private String language;
	private String defaultMessage;
	private int messageId;
	private String propertyName;
	private String serverId;
	private boolean fatal = false;
		
	public JsonErrorObject() {
		super();
	}
			
	public JsonErrorObject(
			final String uid,
			final String language,
			final String defaultMessage, 
			final int messageId,
			final String propertyName,
			final String serverId) {
		super();
		setUid(uid);
		
		this.setLanguage(language);
		this.defaultMessage = defaultMessage;
		this.messageId = messageId;
		this.propertyName = propertyName;
		this.serverId = serverId;
	}
	
	public String getDefaultMessage() {
		return this.defaultMessage;
	}

	public JsonErrorObject defaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;

		return this;
	}

	public int getMessageId() {
		return this.messageId;
	}

	public JsonErrorObject messageId(int messageId) {
		this.messageId = messageId;

		return this;
	}

	public String getPropertyName() {
		return this.propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getServerId() {
		return this.serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public boolean isFatal() {
		return fatal;
	}

	public JsonErrorObject fatal(final boolean fatal) {
		this.fatal = fatal;

		return this;
	}
}
