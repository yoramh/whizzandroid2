package com.whizzthrough.android.commons.server.json;

public final class PubResendRegistrationSmsRequest extends BaseRequest {
	private String userId;
	private String deviceType = "ANDROID";
	private String callerReturnInfo;
	private String uniqueId;

	public String getUserId() {
		return userId;
	}

	public PubResendRegistrationSmsRequest userId(final String userId) {
		this.userId = userId;

		return this;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public PubResendRegistrationSmsRequest uniqueId(final String uniqueId) {
		this.uniqueId = uniqueId;

		return this;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public PubResendRegistrationSmsRequest deviceType(final String deviceType) {
		this.deviceType = deviceType;

		return this;
	}

	public String getCallerReturnInfo() {
		return callerReturnInfo;
	}

	public PubResendRegistrationSmsRequest callerReturnInfo(final String callerReturnInfo) {
		this.callerReturnInfo = callerReturnInfo;

		return this;
	}
}
