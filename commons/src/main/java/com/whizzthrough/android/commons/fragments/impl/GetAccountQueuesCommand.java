package com.whizzthrough.android.commons.fragments.impl;

import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.ISynchroWhizzThroughServer;
import com.whizzthrough.android.commons.server.json.AppListQueuesRequest;
import com.whizzthrough.android.commons.utils.Config;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 24/02/16.
 */
public class GetAccountQueuesCommand extends AbstractCommand {
    private static final long serialVersionUID = -8815911275960924942L;
    private static final ISynchroWhizzThroughServer SERVER = (ISynchroWhizzThroughServer) WhizzThroughServerFactory.getDefaultServer();

    public static final String COMMAND_NAME = GetAccountQueuesCommand.class.getName();

    @Override
    protected void doExecute() throws Exception {
        final AppListQueuesRequest request = new AppListQueuesRequest();
        request.setCount(20);
        request.setStartAt(0);

        this.response = SERVER.listQueues(Config.getCookie(), request);
    }

    @Override
    public String name() {
        return COMMAND_NAME;
    }

    @Override
    public int label() {
        return R.string.wtcres_progress_action_get_account_queues;
    }
}
