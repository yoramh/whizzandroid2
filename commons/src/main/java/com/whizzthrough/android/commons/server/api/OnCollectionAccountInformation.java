package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 15/02/16.
 */
public abstract class OnCollectionAccountInformation extends AbstractWhizzThroughEvents implements IOnCollectAccountInformation {
    public OnCollectionAccountInformation(final Context context) {
        super(context);
    }
}