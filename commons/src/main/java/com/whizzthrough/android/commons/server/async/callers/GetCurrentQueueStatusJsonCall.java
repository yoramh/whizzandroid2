package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnGetCurrentQueueStatus;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.server.json.UserGetCurrentQueueStatusResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

/**
 * Created by yoram on 21/10/15.
 */
public class GetCurrentQueueStatusJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnGetCurrentQueueStatus;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final UserGetCurrentQueueStatusResponse res = (UserGetCurrentQueueStatusResponse) response.getResponse();
        final IOnGetCurrentQueueStatus events = (IOnGetCurrentQueueStatus) job.getEvents();
        events.onResponse(res, job.getEventData());
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return UserGetCurrentQueueStatusResponse.class;
    }

    @Override
    public String getAction() {
        return null;
    }
}