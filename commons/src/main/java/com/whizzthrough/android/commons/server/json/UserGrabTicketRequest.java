package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 18/10/15.
 */
public class UserGrabTicketRequest extends BaseRequest {
    private static final long serialVersionUID = -2596959330528687391L;

    private String id;
    private String code;
    private String qrCodeUserToken;

    public UserGrabTicketRequest(final String id, final String code, final String qrCodeUserToken, final String locale) {
        super();
        this.id = id;
        this.code = code;
        this.qrCodeUserToken = qrCodeUserToken;
        locale(locale);
    }

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getQrCodeUserToken() {
        return this.qrCodeUserToken;
    }

    public void setQrCodeUserToken(final String qrCodeUserToken) {
        this.qrCodeUserToken = qrCodeUserToken;
    }
}
