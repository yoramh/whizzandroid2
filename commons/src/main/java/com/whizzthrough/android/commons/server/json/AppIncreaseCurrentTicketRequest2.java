package com.whizzthrough.android.commons.server.json;

public class AppIncreaseCurrentTicketRequest2 extends BaseRequest {
    private static final long serialVersionUID = -1907052363008829306L;

    private long queueId;

    public long getQueueId() {
        return queueId;
    }

    public void setQueueId(long queueId) {
        this.queueId = queueId;
    }
}
