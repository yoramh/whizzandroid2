package com.whizzthrough.android.commons.parser.impl;

import android.content.Context;
import android.util.Log;
import android.util.Xml;
import com.google.gson.Gson;
import com.whizzthrough.android.commons.parser.api.IParser;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.*;

/**
 * Created by yoram on 22/02/16.
 */
public class NodeParser {
    private static class KeyValuePair {
        private String name;
        private Object value;
    }


    public static class Node implements Serializable {
        private static final long serialVersionUID = 5595640677801422925L;

        private String clazz;
        private boolean showFragment = true;
        private List<Response> responses = new ArrayList<>();
        private Map<String, Object> properties = new HashMap<>();

        public String getClazz() {
            return clazz;
        }

        public void setClazz(String clazz) {
            this.clazz = clazz;
        }

        public boolean isShowFragment() {
            return showFragment;
        }

        public void setShowFragment(boolean showFragment) {
            this.showFragment = showFragment;
        }

        public List<Response> getResponses() {
            return responses;
        }

        public void setResponses(List<Response> responses) {
            this.responses = responses;
        }

        public Map<String, Object> getProperties() {
            return properties;
        }

        public void setProperties(Map<String, Object> properties) {
            this.properties = properties;
        }
    }

    public static class Response implements Serializable {
        private static final long serialVersionUID = -7235927869916964418L;

        private int value;
        private boolean backable = true;
        private Node node;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public boolean isBackable() {
            return backable;
        }

        public void setBackable(boolean backable) {
            this.backable = backable;
        }

        public Node getNode() {
            return node;
        }

        public void setNode(Node node) {
            this.node = node;
        }
    }

    public static class ResourceProperty implements Serializable {
        private static final long serialVersionUID = -2940723118228513207L;
        private String resourceName;
        private String definition;
        private String package_;

        public int findResource(Context context) {
            int res = context.getResources().getIdentifier(resourceName, definition, package_);

            if (res == 0) {
                Log.w("WT", String.format("Resource not found, trying with context package: %s", toString()));
            }

            res = context.getResources().getIdentifier(resourceName, definition, context.getPackageName());

            if (res == 0) {
                Log.w(
                        "WT",
                        String.format(
                                "Resource not found when changing path to %s: %s ",
                                context.getPackageName(),
                                toString()));
            }

            return res;
        }

        public String getDefinition() {
            return definition;
        }

        public void setDefinition(String definition) {
            this.definition = definition;
        }

        public String getPackage_() {
            return package_;
        }

        public void setPackage_(String package_) {
            this.package_ = package_;
        }

        public String getResourceName() {
            return resourceName;
        }

        public void setResourceName(String resourceName) {
            this.resourceName = resourceName;
        }

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }

    public static Node parse(final Context context, final int resourceId) /*throws XmlPullParserException, IOException {*/
        throws IOException, SAXException, ParserConfigurationException {
        InputStream in = context.getResources().openRawResource(resourceId);

        try {
            return parse(context, in);
        } finally {
            in.close();
        }
    }

    private static class ParserWrapper {
        private List<Node> nodes = new ArrayList<>();
    }

    private static class XMLHandler extends DefaultHandler {
        private Stack<Object> stack = new Stack<>();
        private ParserWrapper result = null;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes);

            if (localName.equals("parser")) {
                result = new ParserWrapper();
                stack.push(result);
            } else if (localName.equals("node")) {
                final Node node = new Node();
                node.clazz = getString(attributes, "class", true, null);
                node.showFragment = getBool(attributes, "showFragment", false, true);

                final Object o = stack.peek();
                if (o instanceof ParserWrapper) {
                    ((ParserWrapper)o).nodes.add(node);
                } else if (o instanceof Response) {
                    ((Response)o).setNode(node);
                }
                stack.push(node);
            } else if (localName.equals("property")) {
                final KeyValuePair res = new KeyValuePair();

                res.name = getString(attributes, "name", true, null).trim();

                final String type = getString(attributes, "type", true, null).trim();

                if (type.equals("resource")) {
                    final ResourceProperty prop = new ResourceProperty();
                    prop.definition = getString(attributes, "definition", true, null).trim();
                    prop.package_ = getString(attributes, "package", true, null).trim();
                    prop.resourceName = getString(attributes, "resource-name", true, null).trim();
                    res.value = prop;
                } else if (type.equals("list")) {
                    res.value = new HashMap<>();
                } else if (type.equals("object")) {
                    final String clazz = getString(attributes, "class", true, null);
                    try {
                        res.value = Class.forName(clazz).newInstance();
                    } catch (Exception t) {
                        throw new SAXException(
                                String.format("Error instantiating class %s with %s", clazz, t.getMessage()),
                                t);
                    }
                } else if (type.equals("string")) {
                    res.value = getString(attributes, "value", true, null);
                }

                final Object o = stack.peek();

                if (o instanceof Node) {
                    ((Node)o).properties.put(res.name, res.value);
                } else if (o instanceof Map) {
                    ((Map)o).put(res.name, res.value);
                }

                stack.push(res.value);
            } else if (localName.equals("response")) {
                final Response response = new Response();
                response.value = getInt(attributes, "value", true, 0);
                response.backable = getBool(attributes, "backable", false, true);

                final Object o = stack.peek();

                if (o instanceof Node) {
                    ((Node)o).responses.add(response);
                }

                stack.push(response);
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            super.endElement(uri, localName, qName);

            stack.pop();
        }
    }

    public static Node parse(final Context context, final InputStream in)
            throws IOException, SAXException, ParserConfigurationException {
        SAXParserFactory saxPF = SAXParserFactory.newInstance();
        SAXParser saxP = saxPF.newSAXParser();
        XMLReader xmlR = saxP.getXMLReader();

        /**
         * Create the Handler to handle each of the XML tags.
         **/
        XMLHandler myXMLHandler = new XMLHandler();
        xmlR.setContentHandler(myXMLHandler);
        xmlR.parse(new InputSource(in));

        return myXMLHandler.result.nodes.get(0);
    }

    private static String getString(
            final Attributes attributes,
            final String name,
            final boolean mandatory,
            final String default_) throws SAXException {
        final String res = attributes.getValue(name);

        if (mandatory && res == null) {
            throw new SAXException(String.format("Attribute %s is mandatory", name));
        }

        if (res == null) {
            return default_;
        } else {
            return res;
        }
    }

    private static int getInt(
            final Attributes attributes,
            final String name,
            final boolean mandatory,
            final int default_) throws SAXException {
        final String s = getString(attributes, name, mandatory, "" + default_);

        try {
            final int res = Integer.parseInt(s);

            return res;
        } catch (NumberFormatException e) {
            throw new SAXException(String.format("Value for %s of %s is not a valid number", name, s));
        }
    }

    private static boolean getBool(
            final Attributes attributes,
            final String name,
            final boolean mandatory,
            final boolean default_) throws SAXException {
        final String s = getString(attributes, name, mandatory, default_ ? "true" : "false");

        return s.toLowerCase().trim().equals("true");
    }
}
