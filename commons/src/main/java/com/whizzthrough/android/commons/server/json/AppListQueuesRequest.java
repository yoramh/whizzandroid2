package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 13/10/15.
 */
public class AppListQueuesRequest extends BaseRequest {
    private static final long serialVersionUID = 5783062204760443228L;

    private int startAt;
    private int count;
/** test **/
    public int getStartAt() {
        return startAt;
    }

    public void setStartAt(int startAt) {
        this.startAt = startAt;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
