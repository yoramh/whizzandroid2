package com.whizzthrough.android.commons.utils.http;

import com.whizzthrough.android.commons.server.json.BaseResponse;

public class JsonResponse extends HttpResponse {
	private BaseResponse response;

	public BaseResponse getResponse() {
		return this.response;
	}

	public void setResponse(BaseResponse response) {
		this.response = response;
	}
}
