package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 15/10/15.
 */
public abstract class OnLogin extends AbstractWhizzThroughEvents implements IOnLogin {
    public OnLogin(final Context context) {
        super(context);
    }
}
