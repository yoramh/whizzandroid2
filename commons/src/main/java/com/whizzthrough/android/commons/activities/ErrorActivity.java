package com.whizzthrough.android.commons.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import com.whizzthrough.android.commons.utils.AndroidUtils;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 15/02/16.
 */
public class ErrorActivity extends AppCompatActivity {
    public static final String ERROR_MESSAGE = ErrorActivity.class.getName() + ".ERROR_MESSAGE";
    public static final String ERROR_LINK = ErrorActivity.class.getName() + ".ERROR_LINK";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.error_layout);

        final String message = getIntent().getStringExtra(ERROR_MESSAGE);

        if (message != null) {
            final TextView tv = (TextView)findViewById(R.id.cm_error_layout_message);
            final TextView tvLnk = (TextView)findViewById(R.id.cm_error_layout_link);
            final String lnk = getIntent().getStringExtra(ERROR_LINK);

            tv.setText(message);

            if (lnk == null) {
                tvLnk.setVisibility(View.GONE);
            } else {
                //tvLnk.setText(lnk);
                AndroidUtils.undelineTextView(this, tvLnk, getString(R.string.wtcres_click_here));
                tvLnk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent browserIntent = new Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse(lnk));
                        ErrorActivity.this.startActivity(browserIntent);
                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
    }
}
