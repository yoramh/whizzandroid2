package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 27/05/16.
 */
public class SseBackOfficeServeQueueResponse  extends BaseResponse {
    private static final long serialVersionUID = 4591057996186540824L;

    private int numberPeopleWaiting;
    private long nextPersonWaitingTime;
    private UserGrabTicketResponse currentTicket;
    private String queueName;
    private AppAddUserOnQueueResponse[] watingTickets;

    public int getNumberPeopleWaiting() {
        return numberPeopleWaiting;
    }

    public void setNumberPeopleWaiting(int numberPeopleWaiting) {
        this.numberPeopleWaiting = numberPeopleWaiting;
    }

    public long getNextPersonWaitingTime() {
        return nextPersonWaitingTime;
    }

    public void setNextPersonWaitingTime(long nextPersonWaitingTime) {
        this.nextPersonWaitingTime = nextPersonWaitingTime;
    }

    public UserGrabTicketResponse getCurrentTicket() {
        return currentTicket;
    }

    public void setCurrentTicket(UserGrabTicketResponse currentTicket) {
        this.currentTicket = currentTicket;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public AppAddUserOnQueueResponse[] getWatingTickets() {
        return watingTickets;
    }

    public void setWatingTickets(AppAddUserOnQueueResponse[] watingTickets) {
        this.watingTickets = watingTickets;
    }
}
