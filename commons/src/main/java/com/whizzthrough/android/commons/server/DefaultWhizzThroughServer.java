package com.whizzthrough.android.commons.server;

import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.Nullable;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.whizzthrough.android.commons.receivers.ErrorBroadcastReceiver;
import com.whizzthrough.android.commons.server.api.*;
import com.whizzthrough.android.commons.server.async.AsyncJsonJob;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.exceptions.WhizzAuthenticationException;
import com.whizzthrough.android.commons.server.exceptions.WhizzAuthorizationException;
import com.whizzthrough.android.commons.server.exceptions.WhizzException;
import com.whizzthrough.android.commons.server.exceptions.WhizzJsonException;
import com.whizzthrough.android.commons.server.json.*;
import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.LoginUtils;
import com.whizzthrough.android.commons.utils.http.HttpUtils;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import shopping.safelane.android.commons.utils.SystemUtils;

public class DefaultWhizzThroughServer implements IWhizzThroughServer, ISynchroWhizzThroughServer {
    private static void post(
            final String url,
            final String cookie,
            final Object request,
            final Class<?> responseClass,
            JsonResponse httpResponse) throws WhizzException {
        try {
            final HttpUtils utils = new HttpUtils(url);

            httpResponse = (httpResponse == null ? new JsonResponse() : httpResponse);

            if (cookie != null) {
                utils.getHeaders().put("Cookie", cookie);
            }

            utils.postJson(request, responseClass, httpResponse);

            if (httpResponse.getResponseCode() == 401) {
                throw new WhizzAuthenticationException();
            } else if (httpResponse.getResponseCode() == 403) {
                throw new WhizzAuthorizationException();
            } else if (httpResponse.getResponseCode() != 200) {
                if (httpResponse.getResponse() instanceof ErrorResponse) {
                    throw new WhizzJsonException(((ErrorResponse)httpResponse.getResponse()).getErrors());
                } else {
                    throw new WhizzException("Error: " + httpResponse.getResponseCode());
                }
            }
        } catch (WhizzException e) {
            throw e;
        } catch (Throwable t) {
            throw new WhizzException(t.getMessage(), t);
        }
    }

    private final ExecutorService executorService = Executors.newFixedThreadPool(2);

    private static void post(
            final String url,
            final Object request,
            final Class<?> responseClass) throws WhizzException {
        post(url, null, request, responseClass, null);
    }

    private static void get(
            final String url,
            final String cookie,
            final Class<?> responseClass,
            JsonResponse httpResponse) throws WhizzException {
        try {
            final HttpUtils utils = new HttpUtils(url);

            httpResponse = (httpResponse == null ? new JsonResponse() : httpResponse);

            if (cookie != null) {
                utils.getHeaders().put("Cookie", cookie);
            }

            utils.getJson(responseClass, httpResponse);

            if (httpResponse.getResponseCode() == 401) {
                throw new WhizzAuthenticationException();
            } else if (httpResponse.getResponseCode() == 403) {
                throw new WhizzAuthorizationException();
            } else if (httpResponse.getResponseCode() != 200) {
                if (httpResponse.getResponse() instanceof ErrorResponse) {
                    throw new WhizzJsonException(((ErrorResponse)httpResponse.getResponse()).getErrors());
                } else {
                    throw new WhizzException("Error: " + httpResponse.getResponseCode());
                }
            }
        } catch (WhizzException e) {
            throw e;
        } catch (Throwable t) {
            throw new WhizzException(t.getMessage(), t);
        }
    }

    @Override
    public void login(
            final Context context,
            final String email,
            final String password,
            final IOnLogin events,
            final ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent,
            final Object eventsData) {
        final PubLoginRequest request = new PubLoginRequest();
        request.setEmail(email);
        request.setPassword(password);
        new AsyncJsonJob(context).executeOnExecutor(executorService, new JsonJob(request, Config.getLoginUrl(), events, afterErrorEvent, eventsData, JsonJob.RequestType.RT_POST));
    }

    @Override
    public String signin(final PubLoginRequest request) throws WhizzException {
        final JsonResponse httpResponse = new JsonResponse();
        post(Config.getLoginUrl(), null, request, BaseResponse.class, httpResponse);
        List<String> cookies = httpResponse.getHeaders().get("Set-Cookie");

        final StringBuilder res = new StringBuilder();

        for (int i = 0; i < cookies.size(); i++) {
            if (i > 0) {
                res.append("; ");
            }

            res.append(cookies.get(i));
        }

        post(Config.getSaveWtPassUrl(), null, request, BaseResponse.class, httpResponse);

        cookies = httpResponse.getHeaders().get("Set-Cookie");

        for (int i = 0; i < cookies.size(); i++) {
            if (res.length() > 0) {
                res.append("; ");
            }

            res.append(cookies.get(i));
        }

        return res.toString();
    }

    @Override
    public void grabTicket(
            final Context context,
            final UserGrabTicketRequest request,
            final IOnGrabTicket events,
            final Object eventsData) {
        new AsyncJsonJob(context).executeOnExecutor(executorService, new JsonJob(request, Config.getGrabTicketUrl(), events, eventsData));
    }

    @Override
    public void listQueues(
            final Context context,
            final String cookie,
            final int startAt,
            final int count,
            final IOnListQueues events,
            final Object eventsData) {
        final AppListQueuesRequest request = new AppListQueuesRequest();
        request.setStartAt(startAt);
        request.setCount(count);
        new AsyncJsonJob(context, cookie).executeOnExecutor(executorService, new JsonJob(request, Config.getListQueuesUrl(), events, eventsData));
    }

    @Override
    public AppListQueuesResponse listQueues(String cookie, AppListQueuesRequest request) throws WhizzException {
        final JsonResponse httpResponse = new JsonResponse();
        post(Config.getListQueuesUrl(), cookie, request, AppListQueuesResponse.class, httpResponse);

        final AppListQueuesResponse res = (AppListQueuesResponse)httpResponse.getResponse();
        return res;
    }

    @Override
    public UpdatedQueueInfoResponse getQueueStatus(String cookie, long queueId) throws WhizzException {
        final JsonResponse httpResponse = new JsonResponse();
        get(Config.getQueueStatusUrl() + "/" + queueId, cookie, UpdatedQueueInfoResponse.class, httpResponse);

        final UpdatedQueueInfoResponse res = (UpdatedQueueInfoResponse)httpResponse.getResponse();
        return res;
    }

    @Override
    public void register(Context context, PubRegisterAccountRequest request, IOnRegister events, ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent, Object eventsData) {
        new AsyncJsonJob(context).executeOnExecutor(executorService, new JsonJob(request, Config.getRegisterUrl(), events, afterErrorEvent, eventsData, JsonJob.RequestType.RT_POST));
    }

    @Override
    public void validateRegistration(Context context, PubValidateRegistrationRequest request, IOnValidateRegistration events, ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent, Object eventsData) {
        new AsyncJsonJob(context).executeOnExecutor(executorService, new JsonJob(request, Config.getRegisterValidateUrl(), events, afterErrorEvent, eventsData, JsonJob.RequestType.RT_POST));
    }

    @Override
    public void resendRegistrationSms(Context context, PubResendRegistrationSmsRequest request, IOnResendRegistrationSms events, ErrorBroadcastReceiver.OnAfterErrorEvent afterErrorEvent, Object eventsData) {
        new AsyncJsonJob(context).executeOnExecutor(executorService, new JsonJob(request, Config.getResendRegistrationSms(), events, afterErrorEvent, eventsData, JsonJob.RequestType.RT_POST));
    }

    @Override
    public void getUserState(Context context, String cookie, String url, IOnGetUserState events, Object eventsData) {
        final BaseRequest request = new BaseRequest();
        new AsyncJsonJob(context, cookie).executeOnExecutor(executorService, new JsonJob(request, url, events, eventsData));
    }

    @Override
    @Nullable
    public AppUserStateResponse getUserState(final String cookie) {
        final JsonResponse httpResponse = new JsonResponse();

        try {
            post(Config.getGetUserStateUrl(), cookie, new BaseRequest(), AppUserStateResponse.class, httpResponse);

            return (AppUserStateResponse)httpResponse.getResponse();
        } catch (WhizzException e) {
            return  null;
        }
    }


    @Override
    public void openQueue(String cookie, long queueId) throws WhizzException{
        post(
                Config.setQueueStatusUrl(),
                cookie,
                new AppOpenCloseQueueRequest().queueId(queueId).open(true),
                BaseResponse.class,
                new JsonResponse());
    }

    @Override
    public void closeQueue(String cookie, long queueId) throws WhizzException {
        post(
                Config.setQueueStatusUrl(),
                cookie,
                new AppOpenCloseQueueRequest().queueId(queueId).open(false),
                BaseResponse.class,
                new JsonResponse());
    }

    @Override
    public void deleteTicket(
            final Context context,
            final String id,
            final String code,
            final String url,
            final IOnDeleteTicket events,
            final Object eventsData) {
        final UserDeleteTicketRequest request = new UserDeleteTicketRequest();
        request.setId(id);
        request.setCode(code);
        new AsyncJsonJob(context).executeOnExecutor(executorService, new JsonJob(request, url, events, eventsData));
    }

    @Override
    public void collectAccountInformation2(
            final Context context,
            final String url,
            final PubCollectAccountInformationRequest request,
            final IOnCollectAccountInformation events,
            final Object eventsData) {
        new AsyncJsonJob(context).executeOnExecutor(executorService, new JsonJob(request, url, events, eventsData));
    }

    @Override
    public void increaseCurrentTicket(
            final Context context,
            final String cookie,
            final String url,
            final AppIncreaseCurrentTicketRequest2 request,
            final IOnIncreaseCurrentTicket events,
            final Object eventsData) {

        new AsyncJsonJob(context, cookie).executeOnExecutor(executorService, new JsonJob(request, url, events, eventsData));
    }

    @Override
    public void listUsersOnQueue(
            final Context context,
            final AppListUsersOnQueueRequest request,
            final IOnListUsersOnQueue events,
            final Object eventsData) throws WhizzException {
        new AsyncJsonJob(context, Config.getCookie()).executeOnExecutor(executorService, new JsonJob(request, Config.getListUserOnQueueUrl(), events, eventsData));
    }

    @Override
    public AppListUsersOnQueueResponse listUserOnQueue(
            final String cookie,
            final AppListUsersOnQueueRequest request) throws WhizzException {
        final JsonResponse httpResponse = new JsonResponse();
        post(Config.getListUserOnQueueUrl(), cookie, request, AppListUsersOnQueueResponse.class, httpResponse);

        final AppListUsersOnQueueResponse res = (AppListUsersOnQueueResponse)httpResponse.getResponse();
        return res;
    }

    @Override
    public void setTicketDescription(
            final Context context,
            final AppSetTicketDescriptionRequest request,
            final IOnSetTicketDescription events,
            final Object eventsData) {
        new AsyncJsonJob(context, Config.getCookie()).executeOnExecutor(executorService, new JsonJob(request, Config.getSetTicketDescriptionUrl(), events, eventsData));
    }

    @Override
    public void sendMessage(
            final Context context,
            final AppSendMessageRequest request,
            final IOnSendMessage events,
            final Object eventsData) {
        new AsyncJsonJob(context, Config.getCookie()).executeOnExecutor(executorService, new JsonJob(request, Config.getSendMessageUrl(), events, eventsData));
    }

    @Override
    public void sendFeedback(final String alias, final int rating) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpUtils utils = new HttpUtils();
                    URL url = new URL(Config.getFeedbackUrl() + "/" + alias + "/" + rating);
                    utils.setUrl(url);
                    utils.get(new ByteArrayOutputStream());
                } catch (Throwable t) {
                    Log.e("WT", "Sending feedback", t);
                }
            }
        }).start();
    }

    @Override
    public void getQueueInfoForTerminal(
            final Context context,
            final AppGetQueueInfoForTerminalRequest request,
            final IOnGetQueueInfoForTerminal events,
            final Object eventsData) {
        new AsyncJsonJob(context, Config.getCookie()).executeOnExecutor(executorService,
                new JsonJob(request, Config.getQueueInfoForTerminalUrl(), events, eventsData));
    }
}
