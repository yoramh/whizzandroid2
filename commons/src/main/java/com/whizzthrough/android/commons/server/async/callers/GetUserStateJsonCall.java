package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnGetUserState;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.AppUserStateResponse;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

/**
 * Created by yoram on 28/11/15.
 */
public class GetUserStateJsonCall extends AbstractJsonCall {

    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnGetUserState;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final AppUserStateResponse res = (AppUserStateResponse) response.getResponse();
        final IOnGetUserState events = (IOnGetUserState) job.getEvents();
        events.onResponse(res, job.getEventData());
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return AppUserStateResponse.class;
    }

    @Override
    public String getAction() {
        return null;
    }
}
