package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 25/10/15.
 */
public class UserGetAllMyTicketsRequest extends BaseRequest {
    private static final long serialVersionUID = -763347833967277495L;

    private String id;

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }
}
