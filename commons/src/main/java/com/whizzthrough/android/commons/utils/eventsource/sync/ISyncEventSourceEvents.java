package com.whizzthrough.android.commons.utils.eventsource.sync;

import com.whizzthrough.android.commons.utils.eventsource.CloseReason;
import com.whizzthrough.android.commons.utils.eventsource.InputStreamResult;

import java.net.HttpURLConnection;

/**
 * Created by yoram on 17/10/15.
 */
public interface ISyncEventSourceEvents {
    void onMessage(String id, String event, String data);

    void onError(HttpURLConnection conn, Throwable t);

    void onConnectionError(HttpURLConnection conn, InputStreamResult res);

    void onDisconnected();

    void onClose(WhyWasIClosed reason);

    void onConnecting();

    void onConnected();
}
