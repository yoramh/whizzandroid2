package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 28/11/15.
 */
public class AppUserStateResponse extends BaseResponse {
    private static final long serialVersionUID = -5203231025999098762L;

    private boolean customer;
    private boolean staff;
    private boolean user;

    public boolean isCustomer() {
        return customer;
    }

    public void setCustomer(boolean customer) {
        this.customer = customer;
    }

    public boolean isStaff() {
        return staff;
    }

    public void setStaff(boolean staff) {
        this.staff = staff;
    }

    public boolean isUser() {
        return user;
    }

    public void setUser(boolean user) {
        this.user = user;
    }
}
