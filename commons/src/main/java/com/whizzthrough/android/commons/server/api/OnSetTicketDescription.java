package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 29/02/16.
 */
public abstract class OnSetTicketDescription extends AbstractWhizzThroughEvents implements IOnSetTicketDescription {
    public OnSetTicketDescription(final Context context) {
        super(context);
    }
}
