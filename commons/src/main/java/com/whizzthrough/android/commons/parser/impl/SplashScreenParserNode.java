package com.whizzthrough.android.commons.parser.impl;

import android.content.Context;
import android.util.Log;
import com.whizzthrough.android.commons.fragments.AbstractFragment;
import com.whizzthrough.android.commons.fragments.SplashScreenFragment;
import com.whizzthrough.android.commons.fragments.api.ICommand;
import com.whizzthrough.android.commons.parser.api.IFragmentParser;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by yoram on 22/02/16.
 */
public class SplashScreenParserNode extends AbstractParserNode implements IFragmentParser {
    private static final long serialVersionUID = -2868661314816352798L;

    @Override
    public AbstractFragment getFragment(final Context context, final Map<String, Object> properties) {
        final SplashScreenFragment res = new SplashScreenFragment();

        final NodeParser.ResourceProperty logoIdProp = (NodeParser.ResourceProperty)properties.get("logoid");

        final int logoId = logoIdProp.findResource(context);

        res.setLogoDrawableId(logoId);

        final Map list = (Map)properties.get("commands");
        res.setCommands(ObjectListToICommandArray(list.values()));

        return res;
    }

    private ICommand[] ObjectListToICommandArray(Collection<Object> col) {
        final ICommand[] res = new ICommand[col.size()];
        final Iterator<Object> ite = col.iterator();

        for (int i = 0; i < res.length; i++) {
            res[i] = (ICommand)ite.next();
        }

        return res;
    }
}
