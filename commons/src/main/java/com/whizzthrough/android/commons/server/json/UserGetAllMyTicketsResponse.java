package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 25/10/15.
 */
public class UserGetAllMyTicketsResponse extends BaseResponse {
    private static final long serialVersionUID = 4767470387700843762L;

    private UserGrabTicketResponse[] tickets;

    public UserGrabTicketResponse[] getTickets() {
        return tickets;
    }

    public void setTickets(UserGrabTicketResponse[] tickets) {
        this.tickets = tickets;
    }
}
