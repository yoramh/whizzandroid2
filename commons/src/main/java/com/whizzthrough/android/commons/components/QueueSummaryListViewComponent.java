package com.whizzthrough.android.commons.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;
import com.whizzthrough.android.commons.server.json.QueueDefinition;

/**
 * Created by yoram on 24/02/16.
 */
public class QueueSummaryListViewComponent extends ListView {
    private QueueDefinition[] definitions;

    public QueueSummaryListViewComponent(Context context) {
        super(context);

        init();
    }

    public QueueSummaryListViewComponent(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public QueueSummaryListViewComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    private void init() {
        if (isInEditMode()) {
            // Large list so we can see how it fits the screen
            final QueueDefinition[] dummy = new QueueDefinition[30];

            for (int i = 0; i < dummy.length; i++) {
                dummy[i] = new QueueDefinition("Queue " + (i + 1), "Description for queue " + (i + 1));
            }
            setAdapter(new QueueSummaryItemAdapter(
                    getContext(),
                    dummy));
        } else if (definitions == null) {
            setAdapter(new QueueSummaryItemAdapter(getContext(), new QueueDefinition("??", "???")));
        } else {
            setAdapter(new QueueSummaryItemAdapter(getContext(), definitions));
        }
    }

    public void setDefinitions(QueueDefinition[] definitions) {
        this.definitions = definitions;
        init();
        invalidate();
    }
}
