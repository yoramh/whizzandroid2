package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 21/10/15.
 */
public class UserGetCurrentQueueStatusRequest extends BaseRequest {
    private String[] aliases;
    private boolean includeOld = false;

    public String[] getAliases() {
        return aliases;
    }

    public void setAliases(String[] aliases) {
        this.aliases = aliases;
    }

    public boolean isIncludeOld() {
        return includeOld;
    }

    public void setIncludeOld(boolean includeOld) {
        this.includeOld = includeOld;
    }
}
