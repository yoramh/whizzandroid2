package com.whizzthrough.android.commons.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.whizzthrough.android.commons.server.json.QueueDefinition;

import java.io.ByteArrayInputStream;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 24/02/16.
 */
public class QueueSummaryItemAdapter extends BaseAdapter {
    private final QueueDefinition[] queueDefinitions;
    private final Context context;

    public QueueSummaryItemAdapter(final Context context, final QueueDefinition... queueDefinitions) {
        super();

        this.queueDefinitions = queueDefinitions;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.queueDefinitions.length;
    }

    @Override
    public Object getItem(int position) {
        return this.queueDefinitions[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final QueueDefinition def = this.queueDefinitions[position];

        if (convertView == null) {
            v = inflater.inflate(R.layout.comp_queue_summary_item, parent, false);
        } else {
            v = convertView;
        }


        final String qName = getString(def.getName(), "NO NAME");
        final String qDescription = getString(def.getDescription(), "NO DESCRIPTION");
        final TextView name = (TextView)v.findViewById(R.id.comp_queue_summary_item_name);
        final Spanned txt = Html.fromHtml("<b>" + qName + "</b><br/>" + qDescription + "");
        name.setText(txt);

        if (def.getBase64DefaultIcon() != null) {
            final Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(Base64.decode(def.getBase64DefaultIcon(), Base64.NO_WRAP)));
            final Drawable bmd = new BitmapDrawable(parent.getResources(), bm);
            //bmd.setBounds(0, 0, bmd.getIntrinsicWidth() * 2, bmd.getIntrinsicHeight() *2);
            name.setCompoundDrawablesWithIntrinsicBounds(bmd, null, null, null);

        }

        return v;
    }

    @NonNull
    private String getString(final String value, final String default_) {
        return value == null ? default_ : value;
    }
}
