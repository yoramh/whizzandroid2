package com.whizzthrough.android.commons.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import com.whizzthrough.android.commons.components.QueueSummaryListViewComponent;
import com.whizzthrough.android.commons.fragments.api.CommandThread;
import com.whizzthrough.android.commons.fragments.impl.GetAccountQueuesCommand;
import com.whizzthrough.android.commons.server.json.AppListQueuesResponse;
import com.whizzthrough.android.commons.server.json.QueueDefinition;
import com.whizzthrough.android.commons.utils.Config;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 24/02/16.
 */
public class ChooseQueueFragment extends AbstractFragment {
    private static final long serialVersionUID = -1559148042746344772L;

    private int logoDrawableId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.frag_choose_queue, container, false);

        if (savedInstanceState != null) {
            this.logoDrawableId = savedInstanceState.getInt("logoDrawableId");
        }

        TextView txt = (TextView)rootView.findViewById(R.id.wtc_frag_choose_queue_logoText);
        final Drawable logo = rootView.getResources().getDrawable(this.logoDrawableId);
        txt.setCompoundDrawablesWithIntrinsicBounds(null, logo, null, null);

        final CommandThread thread = CommandThread.runSynchronous(new GetAccountQueuesCommand());

        if (thread.success()) {
            final QueueSummaryListViewComponent list = (QueueSummaryListViewComponent)rootView.findViewById(
                    R.id.wtc_frag_choose_queue_list);
            list.setDefinitions(((AppListQueuesResponse)thread.response()).getQueues().toArray(new QueueDefinition[0]));
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Config.setQueueDefinition((QueueDefinition)list.getAdapter().getItem(position));
                    Config.save();

                    getController().onNext(0, getControllerData());
                }
            });
        } else {
            Snackbar.make(rootView, thread.exception().getMessage(), Snackbar.LENGTH_SHORT).show();
        }



        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("logoDrawableId", this.logoDrawableId);
    }

    public int getLogoDrawableId() {
        return this.logoDrawableId;
    }

    public void setLogoDrawableId(final int logoDrawableId) {
        this.logoDrawableId = logoDrawableId;
    }
}
