package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 25/10/15.
 */
public abstract class OnGetAllMyTickets extends AbstractWhizzThroughEvents implements IOnGetAllMyTickets {
    public OnGetAllMyTickets(final Context context) {
        super(context);
    }
}