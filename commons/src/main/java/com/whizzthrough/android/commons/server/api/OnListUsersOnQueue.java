package com.whizzthrough.android.commons.server.api;

import android.content.Context;

/**
 * Created by yoram on 02/03/16.
 */
public abstract class OnListUsersOnQueue extends AbstractWhizzThroughEvents implements IOnListUsersOnQueue {
    public OnListUsersOnQueue(final Context context) {
        super(context);
    }
}
