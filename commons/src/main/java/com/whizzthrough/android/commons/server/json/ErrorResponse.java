package com.whizzthrough.android.commons.server.json;

/**
 * Created by yoram on 14/10/15.
 */
public class ErrorResponse extends BaseResponse {
    private static final long serialVersionUID = 6769177276989522463L;

    private JsonErrorObject[] errors;

    public JsonErrorObject[] getErrors() {
        return errors;
    }

    public void setErrors(JsonErrorObject[] errors) {
        this.errors = errors;
    }
}
