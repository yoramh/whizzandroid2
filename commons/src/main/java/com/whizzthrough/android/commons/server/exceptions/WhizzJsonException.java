package com.whizzthrough.android.commons.server.exceptions;

import com.whizzthrough.android.commons.server.json.JsonErrorObject;

/**
 * Created by yoram on 23/02/16.
 */
public class WhizzJsonException extends WhizzException {
    private static final long serialVersionUID = 4152219120623796788L;

    private JsonErrorObject[] errors;

    public WhizzJsonException() {
        super();
    }

    public WhizzJsonException(final JsonErrorObject... errors) {
        super(errors[0].getDefaultMessage());

        this.errors = errors;
    }

    public JsonErrorObject[] getErrors() {
        return errors;
    }

    public void setErrors(JsonErrorObject[] errors) {
        this.errors = errors;
    }
}
