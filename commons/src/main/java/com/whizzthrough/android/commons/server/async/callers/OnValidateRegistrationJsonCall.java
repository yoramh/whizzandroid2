package com.whizzthrough.android.commons.server.async.callers;

import com.whizzthrough.android.commons.server.api.IOnRegister;
import com.whizzthrough.android.commons.server.api.IOnValidateRegistration;
import com.whizzthrough.android.commons.server.api.IOnWhizzThroughServerEvents;
import com.whizzthrough.android.commons.server.async.JsonJob;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.server.json.PubRegisterAccountResponse;
import com.whizzthrough.android.commons.utils.http.JsonResponse;

import shopping.safelane.android.commons.R;

public class OnValidateRegistrationJsonCall extends AbstractJsonCall {
    @Override
    public boolean supports(IOnWhizzThroughServerEvents events) {
        return events instanceof IOnValidateRegistration;
    }

    @Override
    public Class<? extends BaseResponse> getResponseClass() {
        return BaseResponse.class;
    }

    @Override
    public void afterRun(JsonResponse response, JsonJob job) throws Exception {
        final IOnValidateRegistration events = (IOnValidateRegistration) job.getEvents();
        events.onResponse(job.getEventData());
    }

    @Override
    public String getAction() {
        return getContext().getResources().getString(R.string.wtcres_progress_action_validate_registration);
    }
}
