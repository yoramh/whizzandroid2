package com.whizzthrough.android.commons.server.json;

public enum ERegistrationChallenge {
    SMS,
    EMAIL
}
