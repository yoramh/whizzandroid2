package com.whizzthrough.android.commons.parser.impl;

import android.content.Context;

import androidx.fragment.app.Fragment;

import com.whizzthrough.android.commons.fragments.AbstractFragment;
import com.whizzthrough.android.commons.parser.api.IFragmentParser;
import com.whizzthrough.android.commons.parser.api.IParser;

import java.io.Serializable;

/**
 * Created by yoram on 22/02/16.
 *
 * TODO - Why should be serializable?
 */
public class NodeRunner implements Serializable {
    private static final long serialVersionUID = -4629010642322208234L;

    public interface IResultController {
        void onNext(int result, Object controllerData);
    }

    public interface IActivityController {
        void setCurrentNode(NodeParser.Node node);
        void showFragment(Fragment fragment, NodeParser.Node node);
        void popBackableStack();
        void endProcess();
    }

    public static Class<?> getClazz(String clazz) {
        try {
            return Class.forName(clazz);
        } catch (Throwable t) {
            throw new IllegalArgumentException(
                    String.format("Error loading class %s with %s", clazz, t.getMessage()),
                    t);
        }
    }

    public static Object newInstance(Class<?> clazz) {
        try {
            return clazz.newInstance();
        } catch (Throwable t) {
            throw new IllegalArgumentException(
                    String.format("Error instantiating class %s with %s", clazz, t.getMessage()),
                    t);
        }
    }

    public static IParser getParser(String clazz) {
        final Object o = newInstance(getClazz(clazz));

        if (o instanceof IParser) {
            return (IParser)o;
        } else {
            throw new IllegalArgumentException(
                    String.format("Object of class %s does not support IParser", o.getClass().getName()));
        }
    }

    public static NodeParser.Response findResponse(
            final NodeParser.Node node,
            final int value) {
        NodeParser.Response res = null;

        for (final NodeParser.Response o: node.getResponses()) {
            if (o.getValue() == value) {
                res = o;

                break;
            }
        }

        return res;
    }

    public static void run(
            final NodeParser.Node node,
            final Context context,
            final IActivityController activityController) {
        final IResultController resultController = new IResultController() {
            @Override
            public void onNext(final int result, final Object controllerData) {
                final NodeParser.Node node = (NodeParser.Node)controllerData;

                NodeParser.Response response = findResponse(node, result);

                if (response != null) {
                    if (!response.isBackable()) {
                        activityController.popBackableStack();
                    }

                    run(response.getNode(), context, activityController);
                } else {
                    activityController.endProcess();
                }
            }
        };

        try {
            IParser parser = getParser(node.getClazz());

            if (node.isShowFragment()) {
                final IFragmentParser fragmentParser = (IFragmentParser)parser;
                final AbstractFragment fragment = fragmentParser.getFragment(context, node.getProperties());
                fragment.setController(resultController);
                fragment.setControllerData(node);
                activityController.setCurrentNode(node);
                activityController.showFragment(fragment, node);
            }
        } catch (Throwable t) {
            throw new IllegalArgumentException("Error", t);
        }
    }
}
