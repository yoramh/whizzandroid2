package com.whizzthrough.android.commons.utils.http;

import java.util.List;
import java.util.Map;

public class HttpResponse {
	private int responseCode;

	private Map<String, List<String>> headers;

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public Map<String, List<String>> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, List<String>> headers) {
		this.headers = headers;
	}
}
