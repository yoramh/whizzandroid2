package com.whizzthrough.android.commons.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whizzthrough.android.commons.fragments.api.CommandThread;
import com.whizzthrough.android.commons.fragments.impl.SigninCommand;
import com.whizzthrough.android.commons.utils.AndroidUtils;
import com.whizzthrough.android.commons.utils.Config;
import shopping.safelane.android.commons.utils.SystemUtils;

import shopping.safelane.android.commons.R;

/**
 * Created by yoram on 23/02/16.
 */
public class CredentialsFragment extends AbstractFragment {
    private static final long serialVersionUID = 8113658648530814185L;

    private int logoDrawableId;
    private String registerOrSignin;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.frag_credentials, container, false);

        if (savedInstanceState != null) {
            this.logoDrawableId = savedInstanceState.getInt("logoDrawableId");
            this.registerOrSignin = savedInstanceState.getString("registerOrSignin");
        }

        final TextView emailTextView = (TextView)rootView.findViewById(R.id.wtc_frag_credential_ed_email);
        final TextView passwordTextView = (TextView)rootView.findViewById(R.id.wtc_frag_credential_ed_password);

        TextView txt = (TextView)rootView.findViewById(R.id.wtc_frag_credentials_logoText);
        final Drawable logo;
        if (savedInstanceState == null) {
            logo = rootView.getResources().getDrawable(this.logoDrawableId);
        } else {
            logo = rootView.getResources().getDrawable(savedInstanceState.getInt("logoDrawableId"));
        }
        txt.setCompoundDrawablesWithIntrinsicBounds(null, logo, null, null);

        txt = (TextView)rootView.findViewById(R.id.wtc_frag_credential_next_button);

        if (isRegister()) {
            AndroidUtils.undelineTextView(rootView.getContext(), txt, R.string.wtcres_frag_credentials_register);
        } else {
            AndroidUtils.undelineTextView(rootView.getContext(), txt, R.string.wtcres_frag_credentials_sign_in);
        }
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.setUsername(emailTextView.getText().toString());
                Config.setPassword(passwordTextView.getText().toString());

                Config.save();

                if (CredentialsFragment.this.registerOrSignin.equals("register")) {
                    /*final CommandThread thread = new CommandThread(
                            new RegisterCommand(
                                    emailTextView.getText().toString(),
                                    passwordTextView.getText().toString()));
                    thread.start();

                    while (!thread.finished()) {
                        SystemUtils.sleep(500);
                    }

                    if (!thread.success()) {
                        Snackbar.make(rootView, thread.exception().getMessage(), Snackbar.LENGTH_LONG).show();
                        return;
                    }*/
                }

                //
                //getController().onNext(0, getControllerData());

                final CommandThread thread = new CommandThread(
                        new SigninCommand(
                                emailTextView.getText().toString(),
                                passwordTextView.getText().toString()));
                thread.start();

                while (!thread.finished()) {
                    SystemUtils.sleep(500);
                }

                if (!thread.success()) {
                    Snackbar.make(rootView, thread.exception().getMessage(), Snackbar.LENGTH_LONG).show();
                    return;
                }

                Config.setCookie((String)thread.response());
                Config.save();

                getController().onNext(0, getControllerData());
            }
        });

        txt = (TextView)rootView.findViewById(R.id.wtc_frag_credentials_terms_and_conditions);
        AndroidUtils.undelineTextView(rootView.getContext(), txt, txt.getText().toString());
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(Config.getPrefixUrl() + "/licenses/en/eula.txt"));
                CredentialsFragment.this.startActivity(browserIntent);
            }
        });

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("logoDrawableId", logoDrawableId);
        outState.putString("registerOrSignin", registerOrSignin);

    }

    public int getLogoDrawableId() {
        return logoDrawableId;
    }

    public void setLogoDrawableId(int logoDrawableId) {
        this.logoDrawableId = logoDrawableId;
    }

    public String getRegisterOrSignin() {
        return registerOrSignin;
    }

    public void setRegisterOrSignin(String registerOrSignin) {
        this.registerOrSignin = registerOrSignin;
    }

    private boolean isRegister() {
        return this.registerOrSignin.equals("register");
    }
}
