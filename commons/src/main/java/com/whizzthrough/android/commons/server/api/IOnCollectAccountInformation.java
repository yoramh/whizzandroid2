package com.whizzthrough.android.commons.server.api;

import com.whizzthrough.android.commons.server.json.BaseResponse;

/**
 * Created by yoram on 15/02/16.
 */
public interface IOnCollectAccountInformation extends IOnWhizzThroughServerEvents {
    void onResponse(BaseResponse response, Object eventsData);
}