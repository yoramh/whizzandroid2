package com.whizzthrough.android.apps.userapp;

import java.io.Serializable;

import com.whizzthrough.android.commons.server.json.UserGrabTicketResponse;

public class TicketWrapper implements Serializable {
    private static final long serialVersionUID = 4617010490904053640L;

    private UserGrabTicketResponse ticket;

    public TicketWrapper() {
        super();
    }

    public TicketWrapper(UserGrabTicketResponse ticket) {
        super();
        this.ticket = ticket;
    }
    
    public UserGrabTicketResponse getTicket() {
        return this.ticket;
    }

    public void setTicket(UserGrabTicketResponse ticket) {
        this.ticket = ticket;
    }
}
