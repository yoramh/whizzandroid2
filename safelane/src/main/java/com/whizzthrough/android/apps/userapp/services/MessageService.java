package com.whizzthrough.android.apps.userapp.services;

import androidx.appcompat.app.ActionBar;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.NonNull;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.google.gson.Gson;
import com.whizzthrough.android.apps.userapp.TicketWrapper;
import com.whizzthrough.android.apps.userapp.receivers.AlarmBroadcastReceiver;
import com.whizzthrough.android.apps.userapp.receivers.MessageBroadcastReceiver;
import com.whizzthrough.android.commons.server.json.UserGetAllMyTicketsResponse;
import com.whizzthrough.android.commons.server.json.UserGrabTicketResponse;

import shopping.safelane.android.apps.getticket.activities.main.MainActivity2;
import shopping.safelane.android.commons.services.AbstractSseService;
import com.whizzthrough.android.commons.utils.Config;

import java.util.*;

import shopping.safelane.android.apps.getticket.R;

/**
 * Created by yoram on 10/03/16.
 */
public class MessageService extends AbstractSseService {
    private interface IDoAction {
        void run();
    }

    private static class AddTicketAction implements IDoAction {
        private final TicketWrapper wrapper;

        private AddTicketAction(final TicketWrapper wrapper) {
            super();

            this.wrapper = wrapper;
        }

        @Override
        public void run() {
            TICKET_LIST.add(wrapper);
        }
    }

    private static class DeleteTicketAction implements IDoAction {
        private final String code;

        private DeleteTicketAction(final String code) {
            super();

            this.code = code;
        };

        @Override
        public void run() {
            for (int i = 0; i < TICKET_LIST.size(); i++) {
                final TicketWrapper tw = TICKET_LIST.get(i);

                if (tw.getTicket().getCode().equals(code)) {
                    TICKET_LIST.remove(i);

                    break;
                }
            }
        }
    }

    private static class RefreshTicketAction implements IDoAction {
        private final UserGrabTicketResponse o;

        private RefreshTicketAction(final UserGrabTicketResponse o) {
            super();

            this.o = o;
        };

        @Override
        public void run() {
            for (int i = 0; i < TICKET_LIST.size(); i++) {
                final TicketWrapper tw = TICKET_LIST.get(i);

                if (tw.getTicket().getCode().equals(o.getCode())) {
                    tw.setTicket(o);

                    break;
                }
            }
        }
    }

    private static class SetTicketListAction implements IDoAction {
        private final UserGetAllMyTicketsResponse o;

        private SetTicketListAction(final UserGetAllMyTicketsResponse o) {
            super();

            this.o = o;
        }

        @Override
        public void run() {
            TICKET_LIST.clear();

            for (final UserGrabTicketResponse ticket: this.o.getTickets()) {
                TICKET_LIST.add(new TicketWrapper(ticket));
            }

        }
    }

    private static class GetTicketListAction implements IDoAction {
        private List<TicketWrapper> list;

        @Override
        public void run() {
            list = new ArrayList<>();
            list.addAll(TICKET_LIST);
        }

        public List<TicketWrapper> getList() {
            return this.list;
        }
    }

    private static final List<TicketWrapper> TICKET_LIST = new ArrayList<>();

    private static synchronized void doAction(final IDoAction action) {
        action.run();
    }

    public static UserGrabTicketResponse[] sortTickets(final UserGrabTicketResponse[] originalArray) {
        final List<UserGrabTicketResponse> res = Arrays.asList(originalArray);

        Collections.sort(res, new Comparator<UserGrabTicketResponse>() {
            @Override
            public int compare(UserGrabTicketResponse lhs, UserGrabTicketResponse rhs) {
                return lhs.getTicketNumber() - rhs.getTicketNumber();
            }
        });

        return res.toArray(new UserGrabTicketResponse[0]);
    }

    @NonNull
    @Override
    protected String getUrl(@NonNull Intent intent) {
        return Config.hasUniqueCode() ? Config.getMyTicketsSseUrl(Config.getUniqueIdentifier(false)) : null;
    }

    @Override
    public void onMessage(String id, String event, String data) {
        Log.d("WT", "Server Push - id: " + id + "; event: " + event + "; data: " + data);
        if (event.equals("refresh-all-tickets")) {
            final UserGetAllMyTicketsResponse userGetAllMyTicketsResponse = new Gson().fromJson(
                    data,
                    UserGetAllMyTicketsResponse.class);

            userGetAllMyTicketsResponse.setTickets(sortTickets(userGetAllMyTicketsResponse.getTickets()));

            doAction(new SetTicketListAction(userGetAllMyTicketsResponse));

            final Intent refreshIntent = new Intent(MessageBroadcastReceiver.ACTION_REFRESH);
            sendBroadcast(refreshIntent);
        } else if (event.equals("create-ticket")) {
            final UserGrabTicketResponse res = new Gson().fromJson(
                    data,
                    UserGrabTicketResponse.class);

            doAction(new AddTicketAction(new TicketWrapper(res)));

            final Intent refreshIntent = new Intent(MessageBroadcastReceiver.ACTION_REFRESH);
            sendBroadcast(refreshIntent);
        } else if (event.equals("delete-ticket")) {
            final UserGrabTicketResponse res = new Gson().fromJson(
                    data,
                    UserGrabTicketResponse.class);

            doAction(new DeleteTicketAction(res.getCode()));

            final Intent refreshIntent = new Intent(MessageBroadcastReceiver.ACTION_REFRESH);
            sendBroadcast(refreshIntent);
        } else if (event.equals("refresh-ticket")) {
            final UserGrabTicketResponse res = new Gson().fromJson(
                    data,
                    UserGrabTicketResponse.class);

            doAction(new RefreshTicketAction(res));

            final Intent refreshIntent = new Intent(MessageBroadcastReceiver.ACTION_REFRESH);
            sendBroadcast(refreshIntent);
        } else if (event.equals("call-ticket")) {
            final UserGrabTicketResponse res = new Gson().fromJson(
                    data,
                    UserGrabTicketResponse.class);

            doAction(new RefreshTicketAction(res));

            Intent intent = new Intent(this, MainActivity2.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);

            // show final dialog
            intent = new Intent(MessageBroadcastReceiver.ACTION_CALLED);
            intent.putExtra(MessageBroadcastReceiver.MESSAGE, res);
            sendBroadcast(intent);

            // send alarm
            AlarmBroadcastReceiver o = new AlarmBroadcastReceiver();
            intent = new Intent(AlarmBroadcastReceiver.FILTER_ALARM_MESSAGE);
            intent.putExtra(
                    AlarmBroadcastReceiver.BUNDLE_MESSAGE,
                    getResources().getString(R.string.wtcares_ticket_called_message).replace("{0}", "" + res.getTicketNumber()));
            //sendBroadcast(intent);
            o.onReceive(this, intent);
            /*AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            Intent myIntent = new Intent(AlarmBroadcastReceiver.FILTER_ALARM_MESSAGE);
            PendingIntent  pendingIntent = PendingIntent.getBroadcast(this, 0, myIntent, 0);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntent);              //alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 1800000, pendingIntent);*/

            Config.setAlarmedCalled(res.getCode(), true);
            Config.save();
        } else if (event.equals("message-ticket")) {
            Notification n = createNotification(true, data);
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            // mId allows you to update the notification later on.
            mNotificationManager.notify(0, n);
        }
    }

    Notification createNotification(boolean makeHeadsUpNotification, String message) {
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle("GetTicket")
                .setContentText(message);

        if (Build.VERSION.SDK_INT> Build.VERSION_CODES.JELLY_BEAN) {
            notificationBuilder = notificationBuilder.setPriority(Notification.PRIORITY_DEFAULT);
        }

        if (Build.VERSION.SDK_INT> Build.VERSION_CODES.KITKAT)
        {
            notificationBuilder = notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        }

        if (makeHeadsUpNotification) {
            Intent push = new Intent();
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0,
                    push, PendingIntent.FLAG_CANCEL_CURRENT);
            notificationBuilder = notificationBuilder.setFullScreenIntent(fullScreenPendingIntent, true);
        }

        return notificationBuilder.build();
    }

    public static List<TicketWrapper> getTicketList() {
        final GetTicketListAction action = new GetTicketListAction();
        doAction(action);

        return action.getList();
    }

    @Override
    public void onConnected() {
        new Handler(Looper.getMainLooper()).post(() -> {
            ((MainActivity2)Config.getCurrentContext()).getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME |
                    ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_USE_LOGO);
            ((MainActivity2)Config.getCurrentContext()).getSupportActionBar().setIcon(R.drawable.dot_on);
        });
    }


    @Override
    public void onDisconnected() {
        new Handler(Looper.getMainLooper()).post(() -> {
            ((MainActivity2)Config.getCurrentContext()).getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME |
                    ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_USE_LOGO);
            ((MainActivity2)Config.getCurrentContext()).getSupportActionBar().setIcon(R.drawable.dot_off);
        });
    }
}
