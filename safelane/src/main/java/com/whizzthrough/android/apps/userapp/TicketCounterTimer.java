package com.whizzthrough.android.apps.userapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;
import com.whizzthrough.android.apps.userapp.receivers.AlarmBroadcastReceiver;
import com.whizzthrough.android.commons.server.json.UserGrabTicketResponse;
import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.Constants;

import java.util.concurrent.TimeUnit;

import shopping.safelane.android.apps.getticket.R;

/**
 * Created by yoram on 13/03/16.
 */
public class TicketCounterTimer extends CountDownTimer {
    private final UserGrabTicketResponse ticket;
    private final TextView label;
    private final Activity activity;

    public TicketCounterTimer(
            final long millisInFuture,
            final long countDownInterval,
            final UserGrabTicketResponse ticket,
            final TextView label,
            final Activity activity) {
        super(millisInFuture, countDownInterval);

        this.ticket = ticket;
        this.label = label;
        this.activity = activity;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        ticket.setExpectedTimeRemaining(millisUntilFinished);
        if (millisUntilFinished <= ticket.getMinimumTimeRemaining()) {
            label.setTextColor(Color.BLACK);
            label.setText(dateTimeToString(millisUntilFinished));
            cancel();
            return;
        }

        final long timeCallingSoon =
                Config.getAlarmTimeForTicket(this.ticket.getCode()) == -1 ?
                ticket.getSoonAlarmTime() :
                Config.getAlarmTimeForTicket(this.ticket.getCode());

        final boolean ticketSoon = !Config.hasAlarmed(ticket.getCode()) && millisUntilFinished <= timeCallingSoon;

        if (ticketSoon) {
            label.setText(R.string.wtacres_calling_soon);
            label.setTextColor(Color.RED);
            cancel();
        } else {
            label.setTextColor(Color.BLACK);
            label.setText(dateTimeToString(millisUntilFinished));
        }
    }

    @Override
    public void onFinish() {

    }

    private String doubleDigit(long i) {
        String s = Long.toString(i);
        if (s.length() == 1) {
            return "0" + s;
        } else {
            return s;
        }
    }

    private String dateTimeToString(final long time) {
        final StringBuilder sb = new StringBuilder();
        final long hours = time / TimeUnit.HOURS.toMillis(1);
        if (hours > 0) {
            sb.append(doubleDigit(hours) + ":");
        }
        long remainingMs = time % TimeUnit.HOURS.toMillis(1);

        final long min = remainingMs / TimeUnit.MINUTES.toMillis(1);
        sb.append(doubleDigit(min) + ":");
        remainingMs = remainingMs % TimeUnit.MINUTES.toMillis(1);

        final long sec = remainingMs / TimeUnit.SECONDS.toMillis(1);
        sb.append(doubleDigit(sec));

        return sb.toString();
    }
}
