package com.whizzthrough.android.apps.userapp.receivers;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.View;
import android.widget.GridView;

import androidx.fragment.app.Fragment;

import com.whizzthrough.android.apps.userapp.TicketGrid;
import com.whizzthrough.android.apps.userapp.dialogs.CalledTicketDialog;
import com.whizzthrough.android.commons.receivers.AbstractBroadcastReceiver;
import com.whizzthrough.android.commons.server.json.UserGrabTicketResponse;

import shopping.safelane.android.apps.getticket.R;
import shopping.safelane.android.apps.getticket.activities.main.MainActivity2;

/**
 * Created by yoram on 11/03/16.
 */
public class MessageBroadcastReceiver extends AbstractBroadcastReceiver {
    public static final String ACTION_CALLED = MessageBroadcastReceiver.class.getName() + ".ACTION.CALLED";
    public static final String ACTION_REFRESH = MessageBroadcastReceiver.class.getName() + ".ACTION.REFRESH";
    public static final String ACTION_MESSAGE = MessageBroadcastReceiver.class.getName() + ".ACTION.MESSAGE";
    public static final String MESSAGE = MessageBroadcastReceiver.class.getName() + ".MESSAGE";

    private final MainActivity2 activity;

    public MessageBroadcastReceiver(final MainActivity2 activity) {
        super();

        this.activity = activity;
    }

    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter res = new IntentFilter();
        res.addAction(ACTION_REFRESH);
        res.addAction(ACTION_CALLED);
        res.addAction(ACTION_MESSAGE);
        return res;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_REFRESH)) {
            GridView grid = null;
            if (activity != null) {
                for (final Fragment fragment: activity.getSupportFragmentManager().getFragments()) {
                    View v = fragment.getView().findViewById(R.id.ticketsView);

                    if (v instanceof GridView && ((GridView)v).getAdapter() instanceof TicketGrid) {
                        grid = (GridView)v;

                        break;
                    }
                }

            }

            if (grid != null) {
                ((TicketGrid)grid.getAdapter()).notifyDataSetChanged();
                grid.invalidateViews();
            }
        } else if (intent.getAction().equals(ACTION_CALLED)) {
            final UserGrabTicketResponse res = (UserGrabTicketResponse) intent.getSerializableExtra(MESSAGE);
            CalledTicketDialog.showModal(activity, res.getCode());
        } else if (intent.getAction().equals(ACTION_MESSAGE)) {
            final String msg = intent.getStringExtra(MESSAGE);
            Log.i("WT", msg);
        }
    }
}
