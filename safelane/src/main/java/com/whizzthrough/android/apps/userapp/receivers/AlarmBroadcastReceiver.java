package com.whizzthrough.android.apps.userapp.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.util.Log;
import com.whizzthrough.android.commons.receivers.AbstractBroadcastReceiver;

import shopping.safelane.android.apps.getticket.R;

/**
 * Created by yoram on 29/11/15.
 */
public class AlarmBroadcastReceiver extends AbstractBroadcastReceiver {
    public static final String FILTER_ALARM_MESSAGE = AlarmBroadcastReceiver.class.getName() + ".FILTER.ALARM";

    public static final String BUNDLE_MESSAGE = AlarmBroadcastReceiver.class.getName() + ".BUNDLE.MESSAGE";

    @Override
    public void onReceive(Context context, Intent intent) {
        Notification.Builder builder = new Notification.Builder(context);
        builder.setSound(Uri.parse("android.resource://com.whizzthrough.android.getticket/" + R.raw.you_know_you_like_it));
        builder.setTicker("test alarm");
        builder.setSmallIcon(R.drawable.logo);
        builder.setContentInfo(intent.getStringExtra(BUNDLE_MESSAGE));
        NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        try {
            nm.notify(1, builder.getNotification());
        } catch (Throwable t) {
            Log.e("wt", t.getMessage(), t);
        }
    }

    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter res = new IntentFilter();
        res.addAction(FILTER_ALARM_MESSAGE);
        return res;
    }
}
