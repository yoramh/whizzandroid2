package com.whizzthrough.android.apps.userapp;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.widget.*;
import com.whizzthrough.android.apps.userapp.services.MessageService;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.OnDeleteTicket;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.server.json.UserGrabTicketResponse;
import com.whizzthrough.android.commons.utils.Config;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whizzthrough.android.commons.utils.FormUtils;
import com.whizzthrough.android.commons.utils.TimeUtils;

import shopping.safelane.android.apps.getticket.R;
import shopping.safelane.android.apps.getticket.activities.main.MainActivity2;

public class TicketGrid extends BaseAdapter implements Serializable {
    private static final long serialVersionUID = -3922887532004668738L;

    private static class TicketInfo {
        private CountDownTimer timer;
        private UserGrabTicketResponse ticket;
    }

    private Context mContext;

    public TicketGrid(final Context mContext) {
        super();
            
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return MessageService.getTicketList().size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private MainActivity2 getParentActivity(final View parent) {
        if (parent == null) {
            return null;
        }

        if (!(parent.getParent() instanceof View)) {
            return null;
        }

        return (MainActivity2)((View)parent.getParent()).getContext();
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final View grid;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final List<TicketWrapper> ticketList = MessageService.getTicketList();

        final TicketWrapper ticketWrapper = ticketList.get(position);
        final UserGrabTicketResponse ticket = ticketWrapper.getTicket();

        if (convertView == null) {
            grid = inflater.inflate(R.layout.ticket, parent, false);
        } else {
            grid = convertView;
        }

        if (convertView == null) {
            final MainActivity2 c = getParentActivity(parent);
            // if it's not recycled, initialize some attributes
            View vg = grid.findViewById(R.id.set_alarm_button);
            vg.setOnClickListener(v -> {
                final UserGrabTicketResponse t = (UserGrabTicketResponse)v.getTag();

                final long timeCallingSoon =
                        Config.getAlarmTimeForTicket(t.getCode()) == -1 ?
                                t.getSoonAlarmTime() :
                                Config.getAlarmTimeForTicket(t.getCode());

                final Context ctx = c;
                RelativeLayout linearLayout = new RelativeLayout(ctx);
                final NumberPicker aNumberPicker = new NumberPicker(ctx);
                aNumberPicker.setMaxValue(50);
                aNumberPicker.setMinValue(1);
                aNumberPicker.setValue((int)(timeCallingSoon / TimeUtils.MINUTE));

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
                final RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

                linearLayout.setLayoutParams(params);
                linearLayout.addView(aNumberPicker,numPicerParams);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
                alertDialogBuilder.setTitle(ctx.getResources().getString(R.string.wtacres_alarm_title));
                alertDialogBuilder.setView(linearLayout);

                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton(ctx.getResources().getString(R.string.wtacres_alarm_ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        Config.setAlarmTimeForTicket(
                                                t.getCode(),
                                                aNumberPicker.getValue() * TimeUtils.MINUTE);
                                        Config.save();
                                    }
                                })
                        .setNegativeButton(ctx.getResources().getString(R.string.wtacres_alarm_cancel),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            });

            vg = grid.findViewById(R.id.image_bin);
            vg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String ticketCode = (String)v.getTag();
                    FormUtils.confirmYesNo(
                            c,
                            c.getResources().getString(R.string.wtacres_delete_ticket_warning_title),
                            c.getResources().getString(R.string.wtacres_delete_ticket_warning_question),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == Dialog.BUTTON_POSITIVE) {
                                        WhizzThroughServerFactory.getDefaultServer().deleteTicket(
                                                c,
                                                Config.getUniqueIdentifier(true),
                                                ticketCode,
                                                Config.getDeleteTicketUrl(),
                                                new OnDeleteTicket(c) {
                                                    @Override
                                                    public void onResponse(BaseResponse response, Object eventsData) {
                                                    }
                                                },
                                                null);
                                    }

                                    dialog.dismiss();
                                }
                            });
                }
            });
        }

        View vg = grid.findViewById(R.id.image_bin);
        vg.setTag(ticketWrapper.getTicket().getCode());

        vg = grid.findViewById(R.id.set_alarm_button);
        vg.setTag(ticket);

        TextView ticketInfo = (TextView) grid.findViewById(R.id.ticket_info);
        TextView myticketnumber = (TextView) grid.findViewById(R.id.my_ticket_number);
        final TextView estimatedTime = (TextView) grid.findViewById(R.id.estimated_time);
        final TextView currentTicketNumber = (TextView) grid.findViewById(R.id.current_ticket_number);

        Spanned text = Html.fromHtml("<b>" + ticket.getCompany() + " - " + ticket.getQueueName() + "</b><br/>" + ticket.getDescription() + "");
        ticketInfo.setText(text);

        myticketnumber.setText("N° " + ticket.getTicketNumber());
        currentTicketNumber.setText("N° " + ticket.getCurrentTicket());

        if (ticket.getLogoBase64() != null) {
            Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(Base64.decode(ticket.getLogoBase64(), Base64.NO_WRAP)));
            Drawable bmd = new BitmapDrawable(parent.getResources(), bm);
            bmd.setBounds(0, 0, bmd.getIntrinsicWidth() * 2, bmd.getIntrinsicHeight() * 2);
            ticketInfo.setCompoundDrawables(bmd, null, null, null);
        }

        // estimated time
        TicketCounterTimer timer = new TicketCounterTimer(
                ticket.getExpectedTimeRemaining(),
                1000,
                ticket,
                estimatedTime,
                getParentActivity(parent));

        if (estimatedTime.getTag() != null) {
            ((TicketCounterTimer)estimatedTime.getTag()).cancel();
        }
        estimatedTime.setTag(timer);
        timer.start();

        return grid;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }
}
