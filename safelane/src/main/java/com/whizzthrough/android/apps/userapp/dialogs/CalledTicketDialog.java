package com.whizzthrough.android.apps.userapp.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.ISynchroWhizzThroughServer;
import com.whizzthrough.android.commons.server.api.OnDeleteTicket;
import com.whizzthrough.android.commons.server.json.BaseResponse;
import com.whizzthrough.android.commons.utils.Config;

import shopping.safelane.android.apps.getticket.R;

/**
 * Created by yoram on 11/03/16.
 */
public class CalledTicketDialog extends Dialog{
    public CalledTicketDialog(Context context, int theme) {
        super(context, theme);
    }

    public CalledTicketDialog(Context context) {
        super(context);
    }

    public static void showModal(final Context context, final String ticketCode) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.thanks, null);
        builder.setView(layout);

        final Dialog dialog = builder.create();
        dialog.show();
        dialog.setCancelable(false);

        final RatingBar rb = (RatingBar)layout.findViewById(R.id.thanks_layout_rating);
        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ((ISynchroWhizzThroughServer)WhizzThroughServerFactory.getDefaultServer()).sendFeedback(ticketCode, (int)rating);

                WhizzThroughServerFactory.getDefaultServer().deleteTicket(
                        context,
                        Config.getUniqueIdentifier(true),
                        ticketCode,
                        Config.getDeleteTicketUrl(),
                        new OnDeleteTicket(context) {
                            @Override
                            public void onResponse(BaseResponse response, Object eventsData) {
                                System.out.println("ticket deleted");
                            }
                        },
                        null);

                dialog.dismiss();
            }
        });
    }
}
