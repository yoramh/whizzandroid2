package com.whizzthrough.android.apps.userapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import shopping.safelane.android.apps.getticket.R;

public class EnterCodeDialog extends Dialog {
    public interface OnClickListener {
        void onSelect(Dialog dialog, String code);
        void onCancel(Dialog dialog);
    }

    public static void show(
            final Context context,
            final Activity activity,
            final OnClickListener listener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.entercode, null);
        builder.setView(layout);

        final Dialog dialog = builder.create();
        dialog.show();
        dialog.setCancelable(false);

        Button button = (Button)layout.findViewById(R.id.enter_code_cancel);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCancel(dialog);
            }
        });

        button = (Button)layout.findViewById(R.id.enter_code_grab);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText ed = (EditText)layout.findViewById(R.id.edit_code);
                listener.onSelect(dialog, ed.getText().toString());
            }
        });
    }

    public EnterCodeDialog(Context context, int theme) {
        super(context, theme);
    }

    public EnterCodeDialog(Context context) {
        super(context);
    }
}
