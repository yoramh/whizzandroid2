package com.whizzthrough.android.apps.userapp.services;

import android.app.IntentService;
import android.content.Intent;
import com.whizzthrough.android.apps.userapp.TicketWrapper;
import com.whizzthrough.android.apps.userapp.receivers.AlarmBroadcastReceiver;
import com.whizzthrough.android.commons.server.json.UserGrabTicketResponse;
import com.whizzthrough.android.commons.utils.Config;
import shopping.safelane.android.commons.utils.SystemUtils;

import java.util.List;

import shopping.safelane.android.apps.getticket.R;

/**
 * Created by yoram on 13/03/16.
 */
public class AlarmService extends IntentService {
    private boolean finished = false;

    public AlarmService() {
        super(AlarmService.class.getName());
    }

    public AlarmService(String name) {
        super(name);
    }

    private void sendAlarm(final int messageId, final UserGrabTicketResponse ticket) {
        final Intent i = new Intent(AlarmBroadcastReceiver.FILTER_ALARM_MESSAGE);

        i.putExtra(
                AlarmBroadcastReceiver.BUNDLE_MESSAGE,
                getResources().getString(messageId).replace("{0}", "" + ticket.getTicketNumber()));

        sendBroadcast(i);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        while (!finished) {
            final List<TicketWrapper> list = MessageService.getTicketList();

            for (final TicketWrapper ticket: list) {
                final boolean ticketCalled =
                        ticket.getTicket().getCalledOn() != 0 &&
                                !Config.hasAlarmedCalled(ticket.getTicket().getCode());

                if (ticketCalled) {
                    sendAlarm(R.string.wtcares_ticket_called_message, ticket.getTicket());

                    Config.setAlarmedCalled(ticket.getTicket().getCode(), true);
                    Config.save();
                } else if (ticket.getTicket().getCalledOn() == 0) {
                    final long timeCallingSoon =
                            Config.getAlarmTimeForTicket(ticket.getTicket().getCode()) == -1 ?
                                    ticket.getTicket().getSoonAlarmTime() :
                                    Config.getAlarmTimeForTicket(ticket.getTicket().getCode());

                    final boolean ticketSoon =
                            !Config.hasAlarmed(ticket.getTicket().getCode()) &&
                                    ticket.getTicket().getExpectedTimeRemaining() <= timeCallingSoon;

                    if (ticketSoon) {
                        sendAlarm(R.string.wtcares_ticket_alarm_message, ticket.getTicket());

                        Config.setAlarmed(ticket.getTicket().getCode(), true);
                        Config.save();
                    }
                }

                if (finished) {
                    break;
                }
            }

            if (finished) {
                break;
            }

            SystemUtils.sleep(1000);
        }
    }

    @Override
    public void onDestroy() {
        finished = true;

        super.onDestroy();
    }
}
