package shopping.safelane.android.apps.getticket.activities.main.ui.home;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.whizzthrough.android.apps.userapp.EnterCodeDialog;
import com.whizzthrough.android.apps.userapp.TicketGrid;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.OnGrabTicket;
import com.whizzthrough.android.commons.server.json.ErrorResponse;
import com.whizzthrough.android.commons.server.json.UserGrabTicketRequest;
import com.whizzthrough.android.commons.server.json.UserGrabTicketResponse;
import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.Constants;
import com.whizzthrough.android.commons.utils.http.PermissionsUtils;

import shopping.safelane.android.apps.getticket.GetTicketApplication;
import shopping.safelane.android.apps.getticket.R;
import shopping.safelane.android.apps.getticket.activities.main.MainActivity2;
import shopping.safelane.android.apps.getticket.utils.Preferences;
import shopping.safelane.android.commons.utils.SystemUtils;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private GridView gridview;
    private TicketGrid adp = null;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.frag_activity_main, container, false);
        setHasOptionsMenu(true);

        Preferences.getPublicName();

        gridview = root.findViewById(R.id.ticketsView);

        refreshApp();

        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_scan_code) {
            try {
                if (!PermissionsUtils.requestPermission(
                        getActivity(),
                        new String[] {Manifest.permission.CAMERA, Manifest.permission.GET_ACCOUNTS},
                        Constants.PermissionResult.PERM_REQ_CAMERA)) {
                    onRequestPermissionsResult(Constants.PermissionResult.PERM_REQ_CAMERA, new String[]{Manifest.permission.CAMERA}, new int[]{PackageManager.PERMISSION_GRANTED});
                }
            } catch (Exception e) {
                e.printStackTrace();
                Snackbar.make(
                        getActivity().findViewById(android.R.id.content),
                        "Error launching scanner: " + e.getMessage(),
                        Snackbar.LENGTH_SHORT)
                        .show();
            }
        } else if (item.getItemId() == R.id.action_enter_code) {
            EnterCodeDialog.show(
                    getContext(),
                    getActivity(),
                    new EnterCodeDialog.OnClickListener() {
                        @Override
                        public void onSelect(Dialog dialog, String code) {
                            WhizzThroughServerFactory.getDefaultServer().grabTicket(
                                    getActivity(),
                                    new UserGrabTicketRequest(
                                            Config.getUniqueIdentifier(true),
                                            code.toUpperCase().trim(),
                                            "",
                                            SystemUtils.getLocaleLanguage(getContext())),
                                    new OnGrabTicket(getActivity()) {
                                        @Override
                                        public void onResponse(final UserGrabTicketResponse response, Object eventsData) {
                                        }

                                        @Override
                                        public void onError(ErrorResponse response, Object eventsData) {
                                            if (response.getErrors().length > 0) {
                                                if (response.getErrors()[0].getMessageId() == Constants.ServerErrors.USER_ALREADY_ON_QUEUE) {
                                                    final Intent errorIntent = new Intent(GetTicketApplication.ErrorConstants.BROADCAST);
                                                    errorIntent.putExtra(
                                                            GetTicketApplication.ErrorConstants.EXTRA_MESSAGE,
                                                            getText(R.string.gt_server_err_user_already_on_queue));
                                                    getContext().sendBroadcast(errorIntent);
                                                }
                                            }
                                        }
                                    },
                                    null);

                            dialog.dismiss();
                        }

                        @Override
                        public void onCancel(Dialog dialog) {
                            dialog.dismiss();
                        }
                    });
        } else {
            return super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void refreshApp() {
        if (gridview != null) {
            adp = new TicketGrid(getContext());
            gridview.setAdapter(adp);
            gridview.invalidateViews();
        }
    }

    public TicketGrid getAdp() {
        return adp;
    }
}