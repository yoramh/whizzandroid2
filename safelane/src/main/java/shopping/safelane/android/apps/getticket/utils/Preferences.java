package shopping.safelane.android.apps.getticket.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

import androidx.annotation.NonNull;

import com.whizzthrough.android.commons.utils.Config;

import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

public class Preferences {
    private Preferences() {}

    public static SharedPreferences getSharedPrefs() {
        return Config.getCurrentContext()
                .getSharedPreferences("Safelane", Context.MODE_PRIVATE);
    }

    /**
     * @return The public name is the name displayed on the Queue Manager console for the customer
     */
    public static String getPublicName() {
        return getSharedPrefs().getString("public.name", null);
    }

    public static void setPublicName(@NonNull String publicName) {
        final SharedPreferences.Editor editor = getSharedPrefs().edit();
        editor.putString("public.name", publicName);
        editor.apply();
    }

    /**
     * @return the unique identifier of the client
     */
    public static String getUserId() {
        return getSharedPrefs().getString("user.id", null);
    }

    public static void setUserId(@NonNull String userId) {
        final SharedPreferences.Editor editor = getSharedPrefs().edit();
        editor.putString("user.id", userId);
        editor.apply();
    }

    /**
     * @return the unique identifier of the client
     */
    public static String getPassword() {
        return getSharedPrefs().getString("user.password", null);
    }

    public static void setPassword(@NonNull String userId) {
        final SharedPreferences.Editor editor = getSharedPrefs().edit();
        editor.putString("user.password", userId);
        editor.apply();
    }
}
