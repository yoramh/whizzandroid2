package shopping.safelane.android.apps.getticket;

import android.app.Application;
import android.content.Intent;
import android.content.IntentFilter;

import com.whizzthrough.android.apps.userapp.receivers.AlarmBroadcastReceiver;
import com.whizzthrough.android.apps.userapp.receivers.MessageBroadcastReceiver;
import com.whizzthrough.android.apps.userapp.services.AlarmService;
import com.whizzthrough.android.apps.userapp.services.MessageService;
import com.whizzthrough.android.commons.receivers.ErrorBroadcastReceiver;
import com.whizzthrough.android.commons.receivers.ShowProgressBroadcastReceiver;
import com.whizzthrough.android.commons.utils.Config;

/**
 * Created by yoram on 10/01/16.
 */
public class GetTicketApplication extends Application {
    public interface ErrorConstants {
        String BROADCAST = ErrorConstants.class.getName() + ".BROADCAST";
        String EXTRA_MESSAGE = ErrorConstants.class.getName() + ".EXTRA_MESSAGE";
    }

    private Intent messageServiceIntent;
    private Intent alarmServiceIntent;

    private ErrorBroadcastReceiver errorBroadcastReceiver;
    private MessageBroadcastReceiver messageBroadcastReceiver;
    private ShowProgressBroadcastReceiver showProgressBroadcastReceiver;
    private AlarmBroadcastReceiver alarmBroadcastReceiver;

    @Override
    public void onCreate() {
        super.onCreate();

        Config.setCurrentContext(getApplicationContext());

        if (errorBroadcastReceiver == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(com.whizzthrough.android.commons.receivers.ErrorBroadcastReceiver.FILTER_ERROR_MESSAGE);

            errorBroadcastReceiver = new ErrorBroadcastReceiver();
            registerReceiver(errorBroadcastReceiver, intentFilter);
        }
        if (showProgressBroadcastReceiver == null) {
            showProgressBroadcastReceiver = new ShowProgressBroadcastReceiver();
            final IntentFilter filter = showProgressBroadcastReceiver.getIntentFilter();
            registerReceiver(showProgressBroadcastReceiver, filter);
        }

        if (alarmBroadcastReceiver == null) {
            alarmBroadcastReceiver = new AlarmBroadcastReceiver();
            IntentFilter filter = alarmBroadcastReceiver.getIntentFilter();
            registerReceiver(alarmBroadcastReceiver, filter);
        }

        messageServiceIntent = new Intent(this, MessageService.class);
        startService(messageServiceIntent);

        alarmServiceIntent = new Intent(this, AlarmService.class);
        startService(alarmServiceIntent);
    }

    @Override
    public void onTerminate() {
        stopService(alarmServiceIntent);
        alarmServiceIntent = null;

        stopService(messageServiceIntent);
        messageServiceIntent = null;

        super.onTerminate();
    }
}
