package shopping.safelane.android.apps.getticket.activities.main;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.whizzthrough.android.apps.userapp.ScannerActivity;
import com.whizzthrough.android.apps.userapp.receivers.MessageBroadcastReceiver;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.OnGrabTicket;
import com.whizzthrough.android.commons.server.api.OnLogin;
import com.whizzthrough.android.commons.server.json.ErrorResponse;
import com.whizzthrough.android.commons.server.json.UserGrabTicketRequest;
import com.whizzthrough.android.commons.server.json.UserGrabTicketResponse;
import com.whizzthrough.android.commons.utils.Config;
import com.whizzthrough.android.commons.utils.Constants;
import com.whizzthrough.android.commons.utils.http.PermissionsUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.io.ByteArrayInputStream;
import java.util.Properties;

import shopping.safelane.android.apps.getticket.GetTicketApplication;
import shopping.safelane.android.apps.getticket.R;
import shopping.safelane.android.apps.getticket.activities.main.ui.dashboard.DashboardFragment;
import shopping.safelane.android.apps.getticket.utils.Preferences;
import shopping.safelane.android.commons.activities.startup.OnFinishEvent;
import shopping.safelane.android.commons.activities.startup.WelcomeFragment;
import shopping.safelane.android.commons.activities.startup.dto.RegistrationHolder;

public class MainActivity2 extends AppCompatActivity implements OnFinishEvent {
    private boolean duringSetup;

    private NavController navController;
    private RegistrationHolder holder;
    private MessageBroadcastReceiver messageBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Config.setCurrentContext(this);

        /*
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }*/

        setContentView(R.layout.activity_main2);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(item -> {
            System.out.println(item);

            return true;
        });
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();

        //NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavHostFragment navHostFragment1 =
                (NavHostFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.nav_host_fragment);
        navController = navHostFragment1.getNavController();

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        findViewById(R.id.nav_view).setVisibility(View.GONE);

        duringSetup = true;
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(() -> {
            final FragmentManager f1 = getSupportFragmentManager();
            int i = f1.getBackStackEntryCount();

            if (i == 0 && duringSetup) {
                //findViewById(R.id.nav_view).setVisibility(View.VISIBLE);
                duringSetup = false;
                //System.exit(0);
            }

            System.out.println("aaa");
        });

        final String publicName = Preferences.getPublicName();
         String userId = Preferences.getUserId();
        final String password = Preferences.getPassword();

        if (userId == null || password == null) {
            holder = new RegistrationHolder()
                    .registrationNameLabel(getString(R.string.slg_registration_type_your_name));

            Bundle bundle = new Bundle();
            bundle.putSerializable(RegistrationHolder.BUNDLE_KEY, holder);

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            Fragment fragment = new WelcomeFragment();
            fragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.nav_host_fragment, fragment, fragment.getClass().getSimpleName());
            fragmentTransaction.addToBackStack(fragment.toString());
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            fragmentTransaction.commit();
        } else {
            start(false);
        }
    }

    private void start(boolean fromRegistration) {
        if (fromRegistration) {
            FragmentManager manager = getSupportFragmentManager();

            for (int i = 0; i < manager.getBackStackEntryCount(); i++) {
                manager.popBackStack();
            }

            Preferences.setPublicName(holder.getName());
            Preferences.setUserId(holder.getRegisterAccountResponse().getUserId());
            Preferences.setPassword(holder.getPassword());
        }

        final String publicName = Preferences.getPublicName();
        final String userId = Preferences.getUserId();
        final String password = Preferences.getPassword();

        WhizzThroughServerFactory.getDefaultServer().login(
                MainActivity2.this,
                userId,
                password,
                new OnLogin(this) {
                    @Override
                    public void onResponse(String cookie, Object eventsData) {
                        Config.setCookie(cookie);
                        Config.save();

                        if (messageBroadcastReceiver == null) {
                            messageBroadcastReceiver = new MessageBroadcastReceiver(MainActivity2.this);
                            IntentFilter filter = messageBroadcastReceiver.getIntentFilter();
                            registerReceiver(messageBroadcastReceiver, filter);
                        }

                    }
                },
                null,
                null);

        //findViewById(R.id.nav_view).setVisibility(View.VISIBLE);
    }

    @Override
    public void start() {
        start(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == DashboardFragment.REQUEST_SCANNER_RESULT) {
            if (resultCode == RESULT_OK) {
                try {
                    final Properties props = new Properties();
                    String content = data.getStringExtra("content");
                    props.load(new ByteArrayInputStream(content.getBytes()));

                    WhizzThroughServerFactory.getDefaultServer().grabTicket(
                            this.getBaseContext(),
                            new UserGrabTicketRequest(
                                    Config.getUniqueIdentifier(true),
                                    props.getProperty("qcode"),
                                    props.getProperty("token"),
                                    "en_EN"),
                            new OnGrabTicket(MainActivity2.this) {
                                @Override
                                public void onResponse(UserGrabTicketResponse response, Object eventsData) {

                                }

                                @Override
                                public void onError(ErrorResponse response, Object eventsData) {
                                    if (response.getErrors().length > 0) {
                                        if (response.getErrors()[0].getMessageId() == Constants.ServerErrors.USER_ALREADY_ON_QUEUE) {
                                            final Intent errorIntent = new Intent(GetTicketApplication.ErrorConstants.BROADCAST);
                                            errorIntent.putExtra(
                                                    GetTicketApplication.ErrorConstants.EXTRA_MESSAGE,
                                                    getText(R.string.gt_server_err_user_already_on_queue));
                                            sendBroadcast(errorIntent);
                                        }
                                    }
                                }
                            },
                            null);
                } catch (Throwable e) {
                    Snackbar.make(findViewById(android.R.id.content),
                            "error: " + e.getMessage(), Snackbar.LENGTH_LONG)
                            .show();
                    e.printStackTrace();
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        for (int i = 0;i < permissions.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                if (permissions[i].equals(Manifest.permission.CAMERA)) {
                    Intent intent = new Intent(this, ScannerActivity.class);

                    startActivityForResult(intent, DashboardFragment.REQUEST_SCANNER_RESULT);
                }
            }
        }
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/
}