package com.whizzthrough.android.apps.queuemanager.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.whizzthrough.android.apps.queuemanager.dialogs.events.OnPositiveEnterDescriptionDialog;
import com.whizzthrough.android.apps.queuemanager.dialogs.events.OnPositiveSendSmsDialog;
import com.whizzthrough.android.commons.server.json.AppAddUserOnQueueResponse;

import shopping.safelane.android.apps.queuemanager.R;

/**
 * Created by yoram on 13/03/16.
 */
public class SmsOrDescriptionDialog extends Dialog {
    public SmsOrDescriptionDialog(Context context, int theme) {
        super(context, theme);
    }

    public SmsOrDescriptionDialog(Context context) {
        super(context);
    }

    public static void showModal(
            final AppAddUserOnQueueResponse ticket,
            final ListView list,
            final View view) {
        if (ticket.getEmail().startsWith("printed://")) {
            EnterDescriptionDialog.show(
                    ticket.getTicket(),
                    ticket.getCalledOn() == 0,
                    ticket.getDescription(),
                    view,
                    R.string.wtqm_frag_edit_ticket_description_description,
                    shopping.safelane.android.commons.R.string.wtcres_button_set,
                    new OnPositiveEnterDescriptionDialog(ticket, view, list));

            return;
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        final LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.dlg_sms_or_description, null);
        builder.setView(layout);

        final Dialog dialog = builder.create();
        dialog.show();
        dialog.setCancelable(true);

        Button button = (Button)layout.findViewById(R.id.dlg_sms_or_description_sms_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EnterDescriptionDialog.show(
                        ticket.getTicket(),
                        ticket.getCalledOn() != 0,
                        "",
                        view,
                        R.string.wtqm_frag_edit_ticket_description_send_message,
                        shopping.safelane.android.commons.R.string.wtcres_button_send,
                        new OnPositiveSendSmsDialog(ticket, view));

                dialog.dismiss();
            }
        });

        button = (Button)layout.findViewById(R.id.dlg_sms_or_description_description_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EnterDescriptionDialog.show(
                        ticket.getTicket(),
                        ticket.getCalledOn() == 0,
                        ticket.getDescription(),
                        view,
                        R.string.wtqm_frag_edit_ticket_description_description,
                        shopping.safelane.android.commons.R.string.wtcres_button_set,
                        new OnPositiveEnterDescriptionDialog(ticket, view, list));

                dialog.dismiss();
            }
        });
    }
}
