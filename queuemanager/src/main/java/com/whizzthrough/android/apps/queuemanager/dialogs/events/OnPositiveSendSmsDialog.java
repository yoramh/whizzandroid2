package com.whizzthrough.android.apps.queuemanager.dialogs.events;

import android.app.Dialog;
import android.view.View;
import com.whizzthrough.android.apps.queuemanager.dialogs.EnterDescriptionDialog;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.OnSendMessage;
import com.whizzthrough.android.commons.server.json.AppAddUserOnQueueResponse;
import com.whizzthrough.android.commons.server.json.AppSendMessageRequest;

/**
 * Created by yoram on 13/03/16.
 */
public class OnPositiveSendSmsDialog implements EnterDescriptionDialog.OnClickListener {
    private final AppAddUserOnQueueResponse ticket;
    private final View view;

    public OnPositiveSendSmsDialog(
            final AppAddUserOnQueueResponse ticket,
            final View view) {
        super();

        this.ticket = ticket;
        this.view = view;
    }

    @Override
    public void onSelect(
            final Dialog dialog,
            final String description) {
        AppSendMessageRequest request = new AppSendMessageRequest();
        request.setCode(ticket.getCode());
        request.setMessage(description);

        WhizzThroughServerFactory.getDefaultServer().sendMessage(
                view.getContext(),
                request,
                new OnSendMessage(view.getContext()) {
                    @Override
                    public void onResponse(Object eventsData) {

                    }
                },
                null);
        dialog.dismiss();
    }

    @Override
    public void onCancel(Dialog dialog) {
        dialog.dismiss();
    }
}
