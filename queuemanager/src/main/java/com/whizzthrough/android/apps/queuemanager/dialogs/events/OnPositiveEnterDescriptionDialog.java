package com.whizzthrough.android.apps.queuemanager.dialogs.events;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import com.whizzthrough.android.apps.queuemanager.dialogs.EnterDescriptionDialog;
import com.whizzthrough.android.commons.server.WhizzThroughServerFactory;
import com.whizzthrough.android.commons.server.api.OnSetTicketDescription;
import com.whizzthrough.android.commons.server.json.AppAddUserOnQueueResponse;
import com.whizzthrough.android.commons.server.json.AppSetTicketDescriptionRequest;

/**
 * Created by yoram on 13/03/16.
 */
public class OnPositiveEnterDescriptionDialog implements EnterDescriptionDialog.OnClickListener {
    private final AppAddUserOnQueueResponse ticket;
    private final View view;
    private final ListView list;


    public OnPositiveEnterDescriptionDialog(
            final AppAddUserOnQueueResponse ticket,
            final View view,
            final ListView list) {
        super();

        this.ticket = ticket;
        this.view = view;
        this.list = list;
    }

    @Override
    public void onSelect(
            final Dialog dialog,
            final String description) {
        AppSetTicketDescriptionRequest request = new AppSetTicketDescriptionRequest();
        request.setCode(ticket.getCode());
        request.setDescription(description);

        WhizzThroughServerFactory.getDefaultServer().setTicketDescription(
                view.getContext(),
                request,
                new OnSetTicketDescription(view.getContext()) {
                    @Override
                    public void onResponse(AppAddUserOnQueueResponse response, Object eventsData) {
                        ticket.setDescription(description);
                        ((Activity) view.getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((BaseAdapter)list.getAdapter()).notifyDataSetChanged();
                                list.invalidate();
                            }
                        });
                    }
                },
                null
        );
        dialog.dismiss();
    }

    @Override
    public void onCancel(Dialog dialog) {
        dialog.dismiss();
    }
}
