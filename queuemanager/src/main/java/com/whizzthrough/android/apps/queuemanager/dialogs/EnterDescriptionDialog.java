package com.whizzthrough.android.apps.queuemanager.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import shopping.safelane.android.apps.queuemanager.R;

public class EnterDescriptionDialog extends Dialog {
    public interface OnClickListener {
        void onSelect(Dialog dialog, String description);
        void onCancel(Dialog dialog);
    }

    public static void show(
            final int ticketNumber,
            final boolean live,
            final String description,
            final View view,
            final int titleStringResource,
            final int setButtonStringResource,
            final OnClickListener listener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        final LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.frag_edit_ticket_description, null);
        builder.setView(layout);

        final Dialog dialog = builder.create();
        dialog.show();
        dialog.setCancelable(false);

        TextView txt = (TextView)layout.findViewById(R.id.frag_edit_ticket_number);
        txt.setText("" + ticketNumber);
        txt.setBackground(live ? view.getContext().getResources().getDrawable(R.drawable.circle) : view.getContext().getResources().getDrawable(R.drawable.circle_called));

        txt = (TextView)layout.findViewById(R.id.frag_edit_ticket_description_label);
        txt.setText(view.getResources().getString(titleStringResource));

        EditText etxt = (EditText)layout.findViewById(R.id.frag_edit_ticket_description_text);
        etxt.setText(description);

        Button button = (Button)layout.findViewById(R.id.frag_edit_ticket_button_cancel);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCancel(dialog);
            }
        });

        button = (Button)layout.findViewById(R.id.frag_edit_ticket_button_set);
        button.setText(view.getResources().getString(setButtonStringResource));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText ed = (EditText)layout.findViewById(R.id.frag_edit_ticket_description_text);
                listener.onSelect(dialog, ed.getText().toString());
            }
        });
    }

    public EnterDescriptionDialog(Context context, int theme) {
        super(context, theme);
    }

    public EnterDescriptionDialog(Context context) {
        super(context);
    }
}
