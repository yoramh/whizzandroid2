package shopping.safelane.android.apps.queuemanager;

import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import android.util.Log;
import android.view.View;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.whizzthrough.android.commons.parser.impl.NodeParser;
import com.whizzthrough.android.commons.parser.impl.NodeRunner;
import com.whizzthrough.android.commons.receivers.ErrorBroadcastReceiver;
import com.whizzthrough.android.commons.receivers.ShowProgressBroadcastReceiver;
import com.whizzthrough.android.commons.utils.Config;
import shopping.safelane.android.commons.utils.SystemUtils;

import java.io.Serializable;
import java.util.Stack;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, NodeRunner.IActivityController {
    private static class Holder implements Serializable {
        private static final long serialVersionUID = 5299194042562114451L;

        private NodeParser.Node node;
        private Fragment fragment;
    }

    private NodeParser.Node currentNode;
    private Stack<Holder> backNode = new Stack<>();
    private ShowProgressBroadcastReceiver progressReceiver;
    private ErrorBroadcastReceiver errorReceiver;

    private boolean started = false;
    private TextView navEmail;

    private void startApp(Bundle state) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (navEmail == null) {
            navEmail = (TextView)navigationView.getHeaderView(0).findViewById(R.id.qmgr_nav_header_nav_email);
        }
        if (navEmail  != null) {
            navEmail.setText(Config.getUsername());
        }


        if (!this.started) {
            changeToServeQueue();
            this.started = true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressReceiver = new ShowProgressBroadcastReceiver();
        IntentFilter filter = progressReceiver.getIntentFilter();
        registerReceiver(progressReceiver, filter);

        errorReceiver = new ErrorBroadcastReceiver();
        filter = errorReceiver.getIntentFilter();
        registerReceiver(errorReceiver, filter);

        Config.setCurrentContext(this);

        this.currentNode = null;
        if (savedInstanceState == null) {
            try {
                this.currentNode = NodeParser.parse(this, R.raw.bootstrap);
            } catch (Throwable t) {
                Log.e("WT", t.getMessage(), t);
            }
        } else {
            this.currentNode = (NodeParser.Node)savedInstanceState.getSerializable("currentNode");
            this.backNode = (Stack<Holder>)savedInstanceState.getSerializable("backNode");
            this.started = savedInstanceState.getBoolean("started", false);
        }

        if (started) {
            startApp(savedInstanceState);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    NodeRunner.run(MainActivity.this.currentNode, MainActivity.this, MainActivity.this);
                }
            });
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable("currentNode", this.currentNode);
        try {
            outState.putSerializable("backNode", this.backNode);
        } catch (Throwable t) {
            Log.e("WT", t.getMessage(), t);
        }
        outState.putBoolean("started", started);
    }

    @Override
    protected void onDestroy() {
        if (progressReceiver != null) {
            unregisterReceiver(progressReceiver);
            progressReceiver = null;
        }

        if (errorReceiver != null) {
            unregisterReceiver(errorReceiver);
            errorReceiver = null;
        }

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (this.backNode != null && this.backNode.size() > 1) {
            this.backNode.pop();
            Holder h = this.backNode.pop();
            showFragment(h.fragment, h.node);
            this.currentNode = h.node;
        } else if (getFragmentManager().getBackStackEntryCount() > 0){
            super.onBackPressed();
        } else {
            SystemUtils.quit(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.qmgr_nav_controller) {
            changeToServeQueue();
        } else if (id == R.id.qmgr_nav_live_tickets) {
            changeToTicketInbox(true, false);
        } else if (id == R.id.qmgr_nav_dead_tickets) {
            changeToTicketInbox(false, true);
        } else if (id == R.id.qmgr_nav_all_tickets) {
            changeToTicketInbox(true, true);
        } else if (id == R.id.nav_share) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void showFragment(final Fragment fragment, final NodeParser.Node node) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                //transaction.add(fragment, fragment.getClass().getName())
                        // Add this transaction to the back stack
                //        .addToBackStack(null)
                 //       .commit();
                //.add(detailFragment, "detail")
                
                transaction.replace(R.id.content_main_frame, fragment);
                transaction.addToBackStack(null);
                transaction.commit();

                Holder h = new Holder();
                h.fragment = fragment;
                h.node = node;
                MainActivity.this.backNode.push(h);
            }
        });
    }

    @Override
    public void popBackableStack() {
        final FragmentManager fragmentManager = getSupportFragmentManager();

        this.backNode.clear();
        /*
        while (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        }*/
    }

    @Override
    public void setCurrentNode(NodeParser.Node node) {
        this.currentNode = node;
    }

    @Override
    public void endProcess() {
        this.backNode = null;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startApp(null);
            }
        });
    }

    private void changeToServeQueue() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        ServeQueueFragment frag = (ServeQueueFragment) fragmentManager.findFragmentByTag("serveQueueFragment");

        if (frag == null) {
            final boolean isTablet = getResources().getBoolean(R.bool.isTablet);
            frag = new ServeQueueFragment();
        }
        transaction.replace(R.id.content_main_frame, frag, "serveQueueFragment");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void changeToTicketInbox(boolean live, boolean called) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        //ticketsInboxFragment.refresh();
    }
}
