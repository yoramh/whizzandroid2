package shopping.safelane.android.apps.queuemanager;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;

import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.whizzthrough.android.commons.components.QueueCurrentTicketComponent;
import com.whizzthrough.android.commons.fragments.api.CommandThread;
import com.whizzthrough.android.commons.fragments.impl.ListUserOnQueueCommand;
import com.whizzthrough.android.commons.server.json.AppListUsersOnQueueResponse;
import com.whizzthrough.android.commons.server.json.JsonErrorObject;
import com.whizzthrough.android.commons.server.json.UpdatedQueueInfoResponse;
import com.whizzthrough.android.commons.utils.Config;

import java.util.UUID;

import shopping.safelane.android.commons.components.tickets.TicketSummaryRecycleViewComponent;
import shopping.safelane.android.commons.fragments.impl.GetQueueStatusCommand;
import shopping.safelane.android.commons.fragments.impl.SetQueueOpenCloseCommand;
import shopping.safelane.android.commons.services.server.SafelaneServerService;

/**
 * Created by yoram on 25/02/16.
 */
public class ServeQueueFragment extends Fragment {
    private View lastView;
    private SafelaneServerService mService;
    boolean mBound = false;

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {
        private final String uid = UUID.randomUUID().toString();
        private String url;

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            SafelaneServerService.LocalBinder binder = (SafelaneServerService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;

            url = Config.getQueueStatusUrl() + "/" + Config.getQueueDefinition().getId();
            try {
                mService.getSafelaneServerBinder().addServerSentEvent(uid, url);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            try {
                mService.getSafelaneServerBinder().removeServerSentEvent(uid, url);
            } catch (Throwable t) {
                t.printStackTrace();
            }
            mBound = false;
        }
    };

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.frag_serve_queue, container, false);
        this.lastView = rootView;

        final TicketSummaryRecycleViewComponent list = rootView.findViewById(R.id.frag_serve_queue_ticket_list);

        final ImageView shutter = rootView.findViewById(R.id.frag_serve_shutter);
        shutter.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));

        final CommandThread thread = CommandThread.runSynchronous(new GetQueueStatusCommand(Config.getQueueDefinition().getId()));
        final boolean open;
        if (thread.success()) {
            open = ((UpdatedQueueInfoResponse)thread.response()).isOpen();
        } else {
            open = false;
        }

        final TextView openCloseText = rootView.findViewById(R.id.frag_serve_queue_open_close_text);
        openCloseText.setText(open ? R.string.slres_queue_open : R.string.slres_queue_close);

        final Switch openClose = rootView.findViewById(R.id.frag_serve_queue_open_close_switch);
        openClose.setChecked(open);
        openClose.setOnCheckedChangeListener((buttonView, isChecked) -> {
            CommandThread.runSynchronous(
                    new SetQueueOpenCloseCommand(Config.getQueueDefinition().getId(), isChecked),
                    getContext(),
                    t -> {
                        if (!t.success()) {
                            // error;
                            openClose.setChecked(!isChecked);
                        } else {
                            if (isChecked) {
                                refreshList();
                                list.setVisibility(View.VISIBLE);
                                shutter.setVisibility(View.GONE);
                            } else {
                                list.setVisibility(View.GONE);
                                shutter.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        });

        final TextView lastTicketCalled = rootView.findViewById(R.id.frag_serve_queue_last_ticket_text);
        lastTicketCalled.setText(R.string.slres_last_ticket_called);

        final QueueCurrentTicketComponent txt = rootView.findViewById(R.id.frag_serve_queue_current_number);
        txt.setText(Integer.toString(Config.getQueueDefinition().getCurrentTicket()));

        if (open) {
            list.setVisibility(View.VISIBLE);
            refreshList();

            shutter.setVisibility(View.GONE);
        } else {
            list.setVisibility(View.GONE);
            shutter.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Intent intent = new Intent(this.getContext(), SafelaneServerService.class);
        this.getContext().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        getContext().unbindService(mConnection);
    }

    public View getLastView() {
        return this.lastView;
    }

    public void refreshList() {
        final ListUserOnQueueCommand userCommand = new ListUserOnQueueCommand();
        userCommand.setLiveTicket(true);
        userCommand.setOldTicket(true);

        final CommandThread thread = CommandThread.runSynchronous(userCommand);

        if (thread.success()) {
            final TicketSummaryRecycleViewComponent list = lastView.findViewById(R.id.frag_serve_queue_ticket_list);
            final AppListUsersOnQueueResponse response = (AppListUsersOnQueueResponse)thread.response();
            list.tickets(response.getRows());
        } else if (thread.exception() != null) {
            Snackbar.make(getView(), thread.exception().getMessage(), Snackbar.LENGTH_SHORT).show();
        } else if (thread.response() != null && thread.response() instanceof JsonErrorObject[])  {
            Snackbar.make(getView(), ((JsonErrorObject[])thread.response())[0].getDefaultMessage(), Snackbar.LENGTH_SHORT).show();
        }
    }

}
